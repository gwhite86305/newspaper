## Newspaper

The Worthy Brief is a newspaper that tracks todays most popular articles.

## Code Example

Show what the library does as concisely as possible, developers should be able to figure out **how** your project solves their problem by looking at the code example. Make sure the API you are showing off is obvious, and that your code is short and concise.

## Motivation

An example of how to design, code and test a REST API using Express and Node.js
The steps involved in building this app were are follows

  * Design the API using Swagger and post it as a public API see below: https://app.swaggerhub.com/apis/GregWhite/Worthy/0.0.4-oas3 

  * Next, create a JSON schema that cretes JSON objects to test the API

  * Use Swagger CodeGen to generate  the API

  * Incorporate code into Express/Node js application 

## Code features

  * Node modules - Redis - used to cache endpoint data
  *              - req-fast - used to perform lighting fast reads
  * [Using River5 - for collecting news via a very fast RSS aggregator](https://github.com/scripting/river5)

## Code flow

  data structure **articleData**: A JSON object that will contain all formatted news requests

  **/lists  - folder that contains a set of all URL's for each feed**

  Every 5 minutes, we read from all the feeds and route new items to the /rivers folder 

  The code works as follows:

 API Endpoints:

  /worthynews/{articleId}- responsible for taking an articleId and recording the hit at the time it is called

  /worthynews - This endpoint will look at the last worthy news articles accumulated for that past 5 minutes (less only if there are fewer minutes in the articleData object
  

## Installation

**npm install worthy**

Directory structure
   /api - Contains the Open API 3.0 specification
     /swagger  - Contains the specification YAML file 
        /my-api  - The complete REST API implementation
  
	/test - Mocha unit tests
	
	/lists  - folder that contains a set of all URL's for each feed

	/rivers - contains our popular news items pre-screened  

## Testing 

[ Using Postman for sending JSON requests to each endpoint](https://www.getpostman.com/) 

JSON payload objects are contained in directory /postman

Using Karma and Mocha for unit testing

** type: mocha test** - Will run unit tests for each API's  

## Contributors

Greg White  gwhite86305@gmail.com

## License


