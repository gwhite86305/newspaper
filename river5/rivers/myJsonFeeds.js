onGetRiverStream ({
    "updatedFeeds": {
        "updatedFeed": [
            {
                "feedTitle": "Dave Winer",
                "feedUrl": "http://radio3.io/users/davewiner/rss.xml",
                "websiteUrl": "http://scripting.com/",
                "feedDescription": "I started up blogging, podcasting, RSS 2.0, and software for all of that. I love outliners, JavaScript. I love to make new media. Read my blog! :-)",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:56 GMT",
                "item": [
                    {
                        "title": "",
                        "link": "http://nymag.com/daily/intelligencer/2018/01/paul-ryan-silent-partner-in-trumps-war-on-the-rule-of-law.html",
                        "body": "Paul Ryan, Silent Partner in Trump’s War on the Rule of Law. #mustread",
                        "pubDate": "Thu, 25 Jan 2018 18:27:51 GMT",
                        "permaLink": "http://nymag.com/daily/intelligencer/2018/01/paul-ryan-silent-partner-in-trumps-war-on-the-rule-of-law.html",
                        "outline": {
                            "text": "Paul Ryan, Silent Partner in Trump’s War on the Rule of Law. #mustread",
                            "created": "Thu, 25 Jan 2018 18:27:51 GMT",
                            "type": "tweet",
                            "tweetid": "956594483931570177",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000150"
                    },
                    {
                        "title": "",
                        "link": "https://qz.com/1187566/spaceship-media-will-pair-5000-women-on-facebook-to-repair-the-rift-in-us-politics/",
                        "body": "Spaceship Media will pair 5,000 women on Facebook to repair the rift in US politics.",
                        "pubDate": "Thu, 25 Jan 2018 19:43:23 GMT",
                        "permaLink": "https://qz.com/1187566/spaceship-media-will-pair-5000-women-on-facebook-to-repair-the-rift-in-us-politics/",
                        "outline": {
                            "text": "Spaceship Media will pair 5,000 women on Facebook to repair the rift in US politics.",
                            "created": "Thu, 25 Jan 2018 19:43:23 GMT",
                            "type": "tweet",
                            "tweetid": "956613492181790721",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000149"
                    },
                    {
                        "title": "",
                        "link": "https://www.washingtonpost.com/local/dc-politics/the-white-house-wanted-a-van-gogh-the-guggenheim-offered-a-used-solid-gold-toilet/2018/01/25/38d574fc-0154-11e8-bb03-722769454f82_story.html?utm_term=.216c6e492153",
                        "body": "The Trumps asked to borrow a Van Gogh but the Guggenheim offered a solid gold toilet instead.",
                        "pubDate": "Thu, 25 Jan 2018 19:45:52 GMT",
                        "permaLink": "https://www.washingtonpost.com/local/dc-politics/the-white-house-wanted-a-van-gogh-the-guggenheim-offered-a-used-solid-gold-toilet/2018/01/25/38d574fc-0154-11e8-bb03-722769454f82_story.html?utm_term=.216c6e492153",
                        "outline": {
                            "text": "The Trumps asked to borrow a Van Gogh but the Guggenheim offered a solid gold toilet instead.",
                            "created": "Thu, 25 Jan 2018 19:45:52 GMT",
                            "type": "tweet",
                            "tweetid": "956614120530481152",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000148"
                    },
                    {
                        "title": "",
                        "link": "http://podcatch.com/archive/2018/01/25/61195.html",
                        "body": "A remarkable historic episode of the Daily podcast on the end of the trial of Dr Lawrence Nassar. Each of the survivors spoke directly to Nassar, publicly, before he was sentenced.",
                        "pubDate": "Thu, 25 Jan 2018 21:39:15 GMT",
                        "permaLink": "http://podcatch.com/archive/2018/01/25/61195.html",
                        "outline": {
                            "text": "A remarkable historic episode of the Daily podcast on the end of the trial of Dr Lawrence Nassar. Each of the survivors spoke directly to Nassar, publicly, before he was sentenced.",
                            "created": "Thu, 25 Jan 2018 21:39:15 GMT",
                            "type": "tweet",
                            "tweetid": "956642652900003841",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000147"
                    },
                    {
                        "title": "",
                        "link": "https://en.wikiquote.org/wiki/Finley_Peter_Dunne",
                        "body": "\"The job of the newspaper is to comfort the afflicted and afflict the comfortable.\"",
                        "pubDate": "Thu, 25 Jan 2018 22:13:43 GMT",
                        "permaLink": "https://en.wikiquote.org/wiki/Finley_Peter_Dunne",
                        "outline": {
                            "text": "\"The job of the newspaper is to comfort the afflicted and afflict the comfortable.\"",
                            "created": "Thu, 25 Jan 2018 22:13:43 GMT",
                            "type": "tweet",
                            "tweetid": "956651325416144896",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000146"
                    },
                    {
                        "title": "",
                        "link": "https://www.reuters.com/article/us-netherlands-russia-cybercrime/dutch-intelligence-agency-spied-on-russian-hacking-group-media-idUSKBN1FE34W",
                        "body": "Dutch intelligence agency spied on Russian hacking group.",
                        "pubDate": "Thu, 25 Jan 2018 22:42:39 GMT",
                        "permaLink": "https://www.reuters.com/article/us-netherlands-russia-cybercrime/dutch-intelligence-agency-spied-on-russian-hacking-group-media-idUSKBN1FE34W",
                        "outline": {
                            "text": "Dutch intelligence agency spied on Russian hacking group.",
                            "created": "Thu, 25 Jan 2018 22:42:39 GMT",
                            "type": "tweet",
                            "tweetid": "956658607919116295",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000145"
                    },
                    {
                        "title": "",
                        "link": "http://www.quantum.danconover.com/the-republic-is-doomed-heres-why/",
                        "body": "Dan Conover: The Republic is doomed. Here’s why.",
                        "pubDate": "Thu, 25 Jan 2018 22:55:10 GMT",
                        "permaLink": "http://www.quantum.danconover.com/the-republic-is-doomed-heres-why/",
                        "outline": {
                            "text": "Dan Conover: The Republic is doomed. Here’s why.",
                            "created": "Thu, 25 Jan 2018 22:55:10 GMT",
                            "type": "tweet",
                            "tweetid": "956661758667698177",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000144"
                    },
                    {
                        "title": "",
                        "link": "http://inessential.com/2018/01/25/evergreen_diary_9_on_indieweb",
                        "body": "Brent has a change of heart re IndieWeb.",
                        "pubDate": "Thu, 25 Jan 2018 23:17:26 GMT",
                        "permaLink": "http://inessential.com/2018/01/25/evergreen_diary_9_on_indieweb",
                        "outline": {
                            "text": "Brent has a change of heart re IndieWeb.",
                            "created": "Thu, 25 Jan 2018 23:17:26 GMT",
                            "type": "tweet",
                            "tweetid": "956667359267344388",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000143"
                    },
                    {
                        "title": "",
                        "link": "https://finance.yahoo.com/news/george-soros-says-facebook-googles-days-numbered-232153289.html",
                        "body": "George Soros slams Facebook and Google.",
                        "pubDate": "Thu, 25 Jan 2018 23:55:02 GMT",
                        "permaLink": "https://finance.yahoo.com/news/george-soros-says-facebook-googles-days-numbered-232153289.html",
                        "outline": {
                            "text": "George Soros slams Facebook and Google.",
                            "created": "Thu, 25 Jan 2018 23:55:02 GMT",
                            "type": "tweet",
                            "tweetid": "956676826025349120",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000142"
                    },
                    {
                        "title": "",
                        "link": "https://www.nytimes.com/2018/01/24/opinion/larry-nassar-gymnastics-sentencing.html?partner=rss&emc=rss",
                        "body": "Adults who suspect a predator is on the prowl and do nothing are simply monsters in another form.",
                        "pubDate": "Fri, 26 Jan 2018 01:02:52 GMT",
                        "permaLink": "https://www.nytimes.com/2018/01/24/opinion/larry-nassar-gymnastics-sentencing.html?partner=rss&emc=rss",
                        "outline": {
                            "text": "Adults who suspect a predator is on the prowl and do nothing are simply monsters in another form.",
                            "created": "Fri, 26 Jan 2018 01:02:52 GMT",
                            "type": "tweet",
                            "tweetid": "956693895072632833",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000141"
                    },
                    {
                        "title": "",
                        "link": "http://www.elle.com/culture/a15855944/womens-march-2018-media-attention-coverage/",
                        "body": "Between 1.6 and 2.5 million people participated in women's marches around the world this weekend.",
                        "pubDate": "Fri, 26 Jan 2018 01:08:12 GMT",
                        "permaLink": "http://www.elle.com/culture/a15855944/womens-march-2018-media-attention-coverage/",
                        "outline": {
                            "text": "Between 1.6 and 2.5 million people participated in women's marches around the world this weekend.",
                            "created": "Fri, 26 Jan 2018 01:08:12 GMT",
                            "type": "tweet",
                            "tweetid": "956695235870027776",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000140"
                    },
                    {
                        "title": "",
                        "link": "https://www.nytimes.com/2018/01/25/us/politics/trump-mueller-special-counsel-russia.html#click=https://t.co/aTryuohwm7",
                        "body": "Trump Ordered Mueller Fired, but Backed Off When White House Counsel Threatened to Quit.",
                        "pubDate": "Fri, 26 Jan 2018 01:32:05 GMT",
                        "permaLink": "https://www.nytimes.com/2018/01/25/us/politics/trump-mueller-special-counsel-russia.html#click=https://t.co/aTryuohwm7",
                        "outline": {
                            "text": "Trump Ordered Mueller Fired, but Backed Off When White House Counsel Threatened to Quit.",
                            "created": "Fri, 26 Jan 2018 01:32:05 GMT",
                            "type": "tweet",
                            "tweetid": "956701246286696448",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000139"
                    },
                    {
                        "title": "",
                        "link": "http://www.espn.com/nba/story/_/id/22210182/who-won-2018-nba-all-star-draft-lebron-james-vs-stephen-curry?device=featurephone",
                        "body": "\"Have Joel Embiid switch teams at halftime. He can talk smack to one group for 24 minutes, then link arms with them, implore them to Trust The Process, and lather up the other half of the league for the final two quarters.\"",
                        "pubDate": "Fri, 26 Jan 2018 02:40:23 GMT",
                        "permaLink": "http://www.espn.com/nba/story/_/id/22210182/who-won-2018-nba-all-star-draft-lebron-james-vs-stephen-curry?device=featurephone",
                        "outline": {
                            "text": "\"Have Joel Embiid switch teams at halftime. He can talk smack to one group for 24 minutes, then link arms with them, implore them to Trust The Process, and lather up the other half of the league for the final two quarters.\"",
                            "created": "Fri, 26 Jan 2018 02:40:23 GMT",
                            "type": "tweet",
                            "tweetid": "956718437551755264",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000138"
                    },
                    {
                        "title": "",
                        "link": "https://newrepublic.com/article/146765/judging-larry-nassar",
                        "body": "Why Rosemarie Aquilina’s handling of a serial sex abusers case was so problematic.",
                        "pubDate": "Fri, 26 Jan 2018 02:59:05 GMT",
                        "permaLink": "https://newrepublic.com/article/146765/judging-larry-nassar",
                        "outline": {
                            "text": "Why Rosemarie Aquilina’s handling of a serial sex abusers case was so problematic.",
                            "created": "Fri, 26 Jan 2018 02:59:05 GMT",
                            "type": "tweet",
                            "tweetid": "956723143195987969",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000137"
                    },
                    {
                        "title": "",
                        "link": "https://jsnes.fir.sh/",
                        "body": "A JavaScript NES emulator.",
                        "pubDate": "Fri, 26 Jan 2018 08:56:39 GMT",
                        "permaLink": "https://jsnes.fir.sh/",
                        "outline": {
                            "text": "A JavaScript NES emulator.",
                            "created": "Fri, 26 Jan 2018 08:56:39 GMT",
                            "type": "tweet",
                            "tweetid": "956813128712499201",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000136"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/liveblog/users/davewiner/2015/12/21/0681.html",
                        "body": "Until recently the meaning in our lives was survival. If we created a new generation of humans to follow us, that was enough meaning.",
                        "pubDate": "Fri, 26 Jan 2018 08:58:05 GMT",
                        "permaLink": "http://scripting.com/liveblog/users/davewiner/2015/12/21/0681.html",
                        "outline": {
                            "text": "Until recently the meaning in our lives was survival. If we created a new generation of humans to follow us, that was enough meaning.",
                            "created": "Fri, 26 Jan 2018 08:58:05 GMT",
                            "type": "tweet",
                            "tweetid": "956813486218149888",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000135"
                    },
                    {
                        "title": "",
                        "link": "https://www.bloomberg.com/news/articles/2018-01-26/u-s-is-about-to-get-real-cold-again-blame-it-on-global-warming?utm_content=tv&utm_campaign=socialflow-organic&utm_source=twitter&utm_medium=social&cmpid%20percent3D=socialflow-twitter-tv",
                        "body": "The U.S. Is About to Get Real Cold Again. Blame It on Global Warming.",
                        "pubDate": "Fri, 26 Jan 2018 14:38:16 GMT",
                        "permaLink": "https://www.bloomberg.com/news/articles/2018-01-26/u-s-is-about-to-get-real-cold-again-blame-it-on-global-warming?utm_content=tv&utm_campaign=socialflow-organic&utm_source=twitter&utm_medium=social&cmpid percent3D=socialflow-twitter-tv",
                        "outline": {
                            "text": "The U.S. Is About to Get Real Cold Again. Blame It on Global Warming.",
                            "created": "Fri, 26 Jan 2018 14:38:16 GMT",
                            "type": "tweet",
                            "tweetid": "956899093980352513",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000134"
                    },
                    {
                        "title": "",
                        "link": "https://www.theverge.com/2018/1/26/16932350/ice-immigration-customs-license-plate-recognition-contract-vigilant-solutions",
                        "body": "ICE has struck a deal to track license plates across the US.",
                        "pubDate": "Fri, 26 Jan 2018 14:48:02 GMT",
                        "permaLink": "https://www.theverge.com/2018/1/26/16932350/ice-immigration-customs-license-plate-recognition-contract-vigilant-solutions",
                        "outline": {
                            "text": "ICE has struck a deal to track license plates across the US.",
                            "created": "Fri, 26 Jan 2018 14:48:02 GMT",
                            "type": "tweet",
                            "tweetid": "956901553973219328",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000133"
                    },
                    {
                        "title": "",
                        "link": "https://www.newyorker.com/magazine/2018/01/15/when-deportation-is-a-death-sentence",
                        "body": "When Deportation Is a Death Sentence.",
                        "pubDate": "Fri, 26 Jan 2018 17:29:08 GMT",
                        "permaLink": "https://www.newyorker.com/magazine/2018/01/15/when-deportation-is-a-death-sentence",
                        "outline": {
                            "text": "When Deportation Is a Death Sentence.",
                            "created": "Fri, 26 Jan 2018 17:29:08 GMT",
                            "type": "tweet",
                            "tweetid": "956942097122242560",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000132"
                    },
                    {
                        "title": "",
                        "link": "https://www.politico.com/story/2018/01/26/trump-immigration-proposal-chuck-schumer-371139",
                        "body": "Schumer may have made a tactical mistake. Now the Repubs can say that Dems blocked citizenship for Dreamers. Might have been better to make a counter-proposal with no funding for the wall and without the other stuff.",
                        "pubDate": "Fri, 26 Jan 2018 18:06:24 GMT",
                        "permaLink": "https://www.politico.com/story/2018/01/26/trump-immigration-proposal-chuck-schumer-371139",
                        "outline": {
                            "text": "Schumer may have made a tactical mistake. Now the Repubs can say that Dems blocked citizenship for Dreamers. Might have been better to make a counter-proposal with no funding for the wall and without the other stuff.",
                            "created": "Fri, 26 Jan 2018 18:06:24 GMT",
                            "type": "tweet",
                            "tweetid": "956951473434386432",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000131"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/26/181453.html",
                        "body": "Does Facebook really do open source?",
                        "pubDate": "Fri, 26 Jan 2018 18:30:11 GMT",
                        "permaLink": "http://scripting.com/2018/01/26/181453.html",
                        "outline": {
                            "text": "Does Facebook really do open source?",
                            "created": "Fri, 26 Jan 2018 18:30:11 GMT",
                            "type": "tweet",
                            "tweetid": "956957460820123649",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000130"
                    },
                    {
                        "title": "",
                        "link": "http://blogs.harvard.edu/philg/2018/01/26/are-academics-who-teach-software-development-methodologies-teaching-the-lessons-of-failure/?utm_source=dlvr.it&utm_medium=twitter",
                        "body": "Are academics who teach software development methodologies teaching the lessons of failure?",
                        "pubDate": "Fri, 26 Jan 2018 18:52:33 GMT",
                        "permaLink": "http://blogs.harvard.edu/philg/2018/01/26/are-academics-who-teach-software-development-methodologies-teaching-the-lessons-of-failure/?utm_source=dlvr.it&utm_medium=twitter",
                        "outline": {
                            "text": "Are academics who teach software development methodologies teaching the lessons of failure?",
                            "created": "Fri, 26 Jan 2018 18:52:33 GMT",
                            "type": "tweet",
                            "tweetid": "956963090511421440",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000129"
                    },
                    {
                        "title": "",
                        "link": "https://nypost.com/2018/01/26/joakim-noah-leaves-knicks-in-dark-over-return-date/amp/?__twitter_impression=true",
                        "body": "Knicks fear Noah not back till next week, double down on trade talks.",
                        "pubDate": "Fri, 26 Jan 2018 19:26:57 GMT",
                        "permaLink": "https://nypost.com/2018/01/26/joakim-noah-leaves-knicks-in-dark-over-return-date/amp/?__twitter_impression=true",
                        "outline": {
                            "text": "Knicks fear Noah not back till next week, double down on trade talks.",
                            "created": "Fri, 26 Jan 2018 19:26:57 GMT",
                            "type": "tweet",
                            "tweetid": "956971745352867841",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000128"
                    },
                    {
                        "title": "",
                        "link": "https://posts.google.com/bulletin/share",
                        "body": "Bulletin is a free, lightweight app for telling a story by capturing photos, videoclips and text right from your phone, published straight to the web (without having to create a blog or build a website).",
                        "pubDate": "Fri, 26 Jan 2018 20:12:53 GMT",
                        "permaLink": "https://posts.google.com/bulletin/share",
                        "outline": {
                            "text": "Bulletin is a free, lightweight app for telling a story by capturing photos, videoclips and text right from your phone, published straight to the web (without having to create a blog or build a website).",
                            "created": "Fri, 26 Jan 2018 20:12:53 GMT",
                            "type": "tweet",
                            "tweetid": "956983311271854080",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000127"
                    },
                    {
                        "title": "",
                        "link": "https://www.thedailybeast.com/key-republican-dials-back-effort-to-protect-robert-mueller-from-donald-trump",
                        "body": "One of the first Senate Republicans to call for legislation protecting special counsel Robert Mueller has stopped actively pushing that effort, sources on Capitol Hill say.",
                        "pubDate": "Fri, 26 Jan 2018 20:16:55 GMT",
                        "permaLink": "https://www.thedailybeast.com/key-republican-dials-back-effort-to-protect-robert-mueller-from-donald-trump",
                        "outline": {
                            "text": "One of the first Senate Republicans to call for legislation protecting special counsel Robert Mueller has stopped actively pushing that effort, sources on Capitol Hill say.",
                            "created": "Fri, 26 Jan 2018 20:16:55 GMT",
                            "type": "tweet",
                            "tweetid": "956984318995386375",
                            "tweetusername": "davewiner"
                        },
                        "id": "0000126"
                    }
                ]
            }
        ]
    },
    "metadata": {
        "name": "myJsonFeeds.json",
        "docs": "http://scripting.com/stories/2010/12/06/innovationRiverOfNewsInJso.html",
        "secs": 0,
        "ctBuilds": 1,
        "ctDuplicatesSkipped": 0,
        "whenGMT": "Fri, 26 Jan 2018 20:55:00 GMT",
        "whenLocal": "2018-1-26 13:55:00",
        "aggregator": "River5 v0.6.2"
    }
})