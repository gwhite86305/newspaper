onGetRiverStream ({
    "updatedFeeds": {
        "updatedFeed": [
            {
                "feedTitle": "TorrentFreak",
                "feedUrl": "http://feeds.feedburner.com/Torrentfreak",
                "websiteUrl": "https://torrentfreak.com/",
                "feedDescription": "Breaking File-sharing, Copyright and Privacy News",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:55:00 GMT",
                "item": [
                    {
                        "title": "Police Shut Down Pirate Streaming TV Provider, Three Men Arrested",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/Qo09eH-8ov8/",
                        "body": "As prices for official multi-channel cable and satellite packages continue to increase, unauthorized streaming TV providers are providing an interesting alternative for those who demand the greatest variety of channels at a cut-down price.\nOf course, none of this is legal and as ...",
                        "pubDate": "Sat, 20 Jan 2018 11:19:49 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/police-shut-down-pirate-streaming-tv-provider-three-men-arrested-180120/#respond",
                        "id": "0000541"
                    },
                    {
                        "title": "China Seriously Doubts Objectivity of US ‘Pirate Site’ List",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/7i-5bhfBXng/",
                        "body": "Late last week, the Office of the US Trade Representative (USTR) released an updated version of its “Out-of-Cycle Review of Notorious Markets,” identifying some of the worst IP-offenders worldwide.\nThe overview is largely based on input from major copyright holders and related ...",
                        "pubDate": "Sat, 20 Jan 2018 20:10:49 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/china-seriously-doubts-objectivity-of-us-pirate-site-list-180120/#respond",
                        "id": "0000540"
                    },
                    {
                        "title": "Copyright Trolls Obtained Details of 200,000 Finnish Internet Users",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/UTNkbjgOUQY/",
                        "body": "Fifteen years ago, the RIAA was contacting alleged file-sharers in the United States, demanding cash payments to make supposed lawsuits go away. In the years that followed, dozens of companies followed in their footsteps &#8211; not as a deterrent &#8211; but as a way to turn ...",
                        "pubDate": "Sun, 21 Jan 2018 11:29:51 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/copyright-trolls-obtained-details-of-200000-finnish-internet-users-180118/#respond",
                        "id": "0000539"
                    },
                    {
                        "title": "Kim Dotcom Sues Government for ‘Billions’ Over Erroneous Arrest",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/LS21a9B5Sf8/",
                        "body": "Six years ago, New Zealand police carried out a spectacular military-style raid against individuals accused only of copyright infringement.\nActing on allegations from the United States government and its Hollywood partners, New Zealand’s elite counter-terrorist force raided the ...",
                        "pubDate": "Sun, 21 Jan 2018 19:25:26 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/kim-dotcom-sues-government-for-billions-over-erroneous-arrest-180121/#respond",
                        "id": "0000538"
                    },
                    {
                        "title": "Top 10 Most Pirated Movies of The Week on BitTorrent – 01/22/18",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/ySiTJtKQLW4/",
                        "body": "This week we have three newcomers in our chart.\nThe Shape of Water is the most downloaded movie again.\nThe data for our weekly download chart is estimated by TorrentFreak, and is for informational and educational reference only. All the movies in the list are ...",
                        "pubDate": "Mon, 22 Jan 2018 07:58:17 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/top-10-pirated-movies-week-bittorrent-012218/#respond",
                        "id": "0000537"
                    },
                    {
                        "title": "Thor Ragnarok Furiously Pirated After iTunes Pre-Order Blunder",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/WADFMCXnmjU/",
                        "body": "When perfect copies of movies leak out onto the Internet in advance of their official release dates, there&#8217;s usually an element of skullduggery at play.\nThis can sometimes involve people intercepting, stealing, or borrowing DVD screener discs, for example. However, other ...",
                        "pubDate": "Mon, 22 Jan 2018 08:20:04 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/thor-ragnarok-furiously-pirated-after-itunes-pre-order-blunder-180122/#respond",
                        "id": "0000536"
                    },
                    {
                        "title": "Planned Piracy Upload Filters are ‘Censorship Machines,’ MEPs Warn",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/s8D4lfV4L2s/",
                        "body": "Through a series of new proposals, the European Commission is working hard to modernize EU copyright law. Among other things, it will require online services to do more to fight piracy.\nThese proposals have not been without controversy. Article 13 of the proposed Copyright ...",
                        "pubDate": "Mon, 22 Jan 2018 16:01:22 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/planned-piracy-upload-filters-are-censorship-machines-meps-warn-180122/#respond",
                        "id": "0000535"
                    },
                    {
                        "title": "Hollywood Says Only Site-Blocking Left to Beat Piracy in New Zealand",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/wcpCzQ8yops/",
                        "body": "The Motion Picture Distributors’ Association (MPDA) is a non-profit organisation which represents major international film studios in New Zealand.\nWith companies including Fox, Sony, Paramount, Roadshow, Disney, and Universal on the books, the MPDA sings from the same sheet as ...",
                        "pubDate": "Tue, 23 Jan 2018 09:33:31 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/hollywood-says-only-site-blocking-left-to-beat-piracy-in-new-zealand-180123/#respond",
                        "id": "0000534"
                    },
                    {
                        "title": "Denuvo Has Been Sold to Global Anti-Piracy Outfit Irdeto",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/3_-UCHt1zUs/",
                        "body": "It&#8217;s fair to say that of all video games anti-piracy technologies, Denuvo is perhaps the most hated of recent times. That hatred unsurprisingly stems from both its success and complexity.\nThose with knowledge of the system say it&#8217;s fiendishly difficult to defeat but ...",
                        "pubDate": "Tue, 23 Jan 2018 12:10:28 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/denuvo-has-been-sold-to-global-anti-piracy-outfit-irdeto-180123/#respond",
                        "id": "0000533"
                    },
                    {
                        "title": "NAFTA Negotiations Heat Up Copyright “Safe Harbor” Clash",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/purRrRbNjDg/",
                        "body": "The North American Free Trade Agreement (NAFTA) between the United States, Canada, and Mexico was negotiated more than 25 years ago.\nOver the past quarter-century trade has changed drastically, especially online, so the United States is now planning to modernize the ...",
                        "pubDate": "Tue, 23 Jan 2018 20:31:50 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/nafta-negotiations-heat-up-copyright-safe-harbor-clash-180123/#respond",
                        "id": "0000532"
                    },
                    {
                        "title": "The Pirate Bay Suffers Downtime, Tor Domain Is Up (Update)",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/BpkkzCjjTxU/",
                        "body": "The main Pirate Bay domain has been offline for nearly a day now.\nFor most people, the site currently displays a Cloudflare error message across the entire site, with the CDN provider referring to a &#8220;bad gateway.&#8221;\nNo further details are available to us and there is ...",
                        "pubDate": "Wed, 24 Jan 2018 08:15:09 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/the-pirate-bay-suffers-downtime-tor-domain-is-up-180124/#respond",
                        "id": "0000531"
                    },
                    {
                        "title": "The EU is Working On Its Own Piracy Watch-List",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/dm_x9PvaRow/",
                        "body": "Earlier this month,  the Office of the US Trade Representative (USTR) released an updated version of its “Out-of-Cycle Review of Notorious Markets,” ostensibly identifying some of the worst IP-offenders worldwide.\nThe annual list overview helps to guide the U.S. Government’s ...",
                        "pubDate": "Wed, 24 Jan 2018 10:27:37 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/the-eu-is-working-on-its-own-piracy-watch-list-180124/#respond",
                        "id": "0000530"
                    },
                    {
                        "title": "New Kodi Addon Tool Might Carry Interesting Copyright Liability Implications",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/wPVC9iK8EVk/",
                        "body": "Kodi is the now ubiquitous media player taking the world by storm. In itself it&#8217;s a great piece of software but augmented with third-party software it can become a piracy powerhouse.\nThis software, known collectively as &#8216;add-ons&#8217;, enables Kodi to do things it ...",
                        "pubDate": "Wed, 24 Jan 2018 19:14:50 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/new-kodi-addon-tool-might-carry-interesting-copyright-liability-implications-180124/#respond",
                        "id": "0000529"
                    },
                    {
                        "title": "Movie Industry Hides Anti-Piracy Messages in ‘Pirate’ Subtitles",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/WFjJzdLwmoQ/",
                        "body": "Anti-piracy campaigns come in all shapes and sizes, from oppressive and scary to the optimistically educational. It is rare for any to be labeled &#8216;brilliant&#8217; but a campaign just revealed in Belgium hits really close to the mark.\nAccording to an announcement by the ...",
                        "pubDate": "Thu, 25 Jan 2018 08:09:18 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/movie-industry-hides-anti-piracy-messages-in-pirate-subtitles-180125/#respond",
                        "id": "0000528"
                    },
                    {
                        "title": "Grumpy Cat Wins $710,000 From Copyright Infringing Coffee Maker",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/IBVjmFPPtz4/",
                        "body": "There are dozens of celebrity cats on the Internet, but Grumpy Cat probably tops them all.\nThe cat’s owners have made millions thanks to their pet’s unique facial expression, which turned her into an overnight Internet star.\nPart of this revenue comes from successful merchandise ...",
                        "pubDate": "Thu, 25 Jan 2018 13:02:54 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/grumpy-cat-wins-710000-copyright-infringing-coffee-maker-180125/#respond",
                        "id": "0000527"
                    },
                    {
                        "title": "Pirate Bay Founder’s Domain Service “Mocks” NY Times Legal Threats",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/TwMrp8MtuvM/",
                        "body": "Back in the day, The Pirate Bay was famous for its amusing responses to legal threats. Instead of complying with takedown notices, it sent witty responses to embarrass the senders.\nToday the notorious torrent site gives copyright holders the silent treatment, but the good-old ...",
                        "pubDate": "Thu, 25 Jan 2018 21:05:47 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/pirate-bay-founders-domain-service-mocks-ny-times-legal-threats-180125/#respond",
                        "id": "0000526"
                    },
                    {
                        "title": "Court Orders Hosting Provider to Stop Pirate Premier League Streams",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/EjNqgMqm2Ho/",
                        "body": "In many parts of the world football, or soccer as some would call it, is the number one spectator sport. \nThe English Premier League, widely regarded as one the top competitions, draws hundreds of millions of viewers per year. Many of these pay for access to the matches, but ...",
                        "pubDate": "Fri, 26 Jan 2018 08:33:48 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/court-orders-hosting-provider-to-stop-pirate-premier-league-streams-180126/#respond",
                        "id": "0000525"
                    },
                    {
                        "title": "Torrent Links Disappear From Torrentz2,  For Adblock Users",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/bZupjpgyhnk/",
                        "body": "With millions of visits per day, Torrentz2 is without a doubt the most popular torrent meta-search engine on the Internet.\nThe site took this spot from the original Torrentz site, which surprisingly closed its doors during the summer of 2016.\nUp until a month ago everything was ...",
                        "pubDate": "Fri, 26 Jan 2018 16:03:30 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/torrent-links-disappear-from-torrentz2-adblock-180126/#respond",
                        "id": "0000524"
                    },
                    {
                        "title": "Playboy Brands Boing Boing a “Clickbait” Site With No Fair Use Defense",
                        "link": "http://feedproxy.google.com/~r/Torrentfreak/~3/LBiqzNVbYu0/",
                        "body": "Late 2017, Boing Boing co-editor Xena Jardin posted an article in which he linked to an archive containing every Playboy centerfold image to date.\n“Kind of amazing to see how our standards of hotness, and the art of commercial erotic photography, have changed over time,” Jardin ...",
                        "pubDate": "Fri, 26 Jan 2018 18:15:10 GMT",
                        "permaLink": "",
                        "comments": "https://torrentfreak.com/playboy-brands-boing-boing-a-clickbait-site-with-no-fair-use-defense-180126/#respond",
                        "id": "0000523"
                    }
                ]
            },
            {
                "feedTitle": "Techmeme",
                "feedUrl": "http://www.techmeme.com/index.xml",
                "websiteUrl": "http://www.techmeme.com/",
                "feedDescription": "Tech Web, page A1",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:55:00 GMT",
                "item": [
                    {
                        "title": "Sources: Airbnb will show $100M in cash-flow profitability for the full year in 2017, as it adds Amex ex-CEO Kenneth Chenault to its board and prepares for IPO (Kara Swisher/Recode)",
                        "link": "http://www.techmeme.com/180125/p33#a180125p33",
                        "body": "Kara Swisher / Recode:\nSources: Airbnb will show $100M in cash-flow profitability for the full year in 2017, as it adds Amex ex-CEO Kenneth Chenault to its board and prepares for IPO&nbsp; &mdash;&nbsp; It's a small $100 million in cash-flow profitability, but it sets up the ...",
                        "pubDate": "Fri, 26 Jan 2018 02:30:03 GMT",
                        "permaLink": "http://www.techmeme.com/180125/p33#a180125p33",
                        "id": "0000522"
                    },
                    {
                        "title": "Sources: Dell's board is meeting later this month to discuss strategic options, including an IPO and buying the rest of VMWare it does not already own (Bloomberg)",
                        "link": "http://www.techmeme.com/180125/p34#a180125p34",
                        "body": "Bloomberg:\nSources: Dell's board is meeting later this month to discuss strategic options, including an IPO and buying the rest of VMWare it does not already own&nbsp; &mdash;&nbsp; Company's board plans to discuss options later this month&nbsp; &mdash;&nbsp; After going private ...",
                        "pubDate": "Fri, 26 Jan 2018 02:44:27 GMT",
                        "permaLink": "http://www.techmeme.com/180125/p34#a180125p34",
                        "id": "0000521"
                    },
                    {
                        "title": "Dutch media: Dutch intelligence penetrated Russian APT29 Cozy Bear's network, office security camera in 2014, provided info on State Department, DNC hacks to US (Huib Modderkolk/De Volkskrant)",
                        "link": "http://www.techmeme.com/180125/p35#a180125p35",
                        "body": "Huib Modderkolk / De Volkskrant:\nDutch media: Dutch intelligence penetrated Russian APT29 Cozy Bear's network, office security camera in 2014, provided info on State Department, DNC hacks to US&nbsp; &mdash;&nbsp; Hackers from the Dutch intelligence service AIVD have provided ...",
                        "pubDate": "Fri, 26 Jan 2018 03:10:14 GMT",
                        "permaLink": "http://www.techmeme.com/180125/p35#a180125p35",
                        "id": "0000520"
                    },
                    {
                        "title": "JD.com founder Richard Liu says the company is in talks to sell 15% of its logistics unit to Tencent, others, as it prepares to launch in Los Angeles in 2018 (Bloomberg)",
                        "link": "http://www.techmeme.com/180125/p36#a180125p36",
                        "body": "Bloomberg:\nJD.com founder Richard Liu says the company is in talks to sell 15% of its logistics unit to Tencent, others, as it prepares to launch in Los Angeles in 2018&nbsp; &mdash;&nbsp; Tencent and other investors to buy 15% of JD Logistics&nbsp; &mdash;&nbsp; Billionaire CEO ...",
                        "pubDate": "Fri, 26 Jan 2018 03:25:05 GMT",
                        "permaLink": "http://www.techmeme.com/180125/p36#a180125p36",
                        "id": "0000519"
                    },
                    {
                        "title": "Google is testing Bulletin, an app for allowing local users to submit stories about their communities, as a limited pilot in Nashville, TN and Oakland, CA (Will Oremus/Slate)",
                        "link": "http://www.techmeme.com/180126/p1#a180126p1",
                        "body": "Will Oremus / Slate:\nGoogle is testing Bulletin, an app for allowing local users to submit stories about their communities, as a limited pilot in Nashville, TN and Oakland, CA&nbsp; &mdash;&nbsp; Google appears to be testing a new tool for people to report and publish local news ...",
                        "pubDate": "Fri, 26 Jan 2018 05:45:05 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p1#a180126p1",
                        "id": "0000518"
                    },
                    {
                        "title": "Coinbase partners with Trading Technologies, trading software provider, enabling financial institutions to trade on Coinbase's GDAX cryptocurrency exchange (Jeff John Roberts/Fortune)",
                        "link": "http://www.techmeme.com/180126/p2#a180126p2",
                        "body": "Jeff John Roberts / Fortune:\nCoinbase partners with Trading Technologies, trading software provider, enabling financial institutions to trade on Coinbase's GDAX cryptocurrency exchange&nbsp; &mdash;&nbsp; The crypto-currency exchange Coinbase on Thursday announced a new tie-up ...",
                        "pubDate": "Fri, 26 Jan 2018 11:50:04 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p2#a180126p2",
                        "id": "0000517"
                    },
                    {
                        "title": "Diversity advocates inside Google, including queer and transgender staff, say they are being targeted by a small group of coworkers with doxxing and harassment (Nitasha Tiku/Wired)",
                        "link": "http://www.techmeme.com/180126/p3#a180126p3",
                        "body": "Nitasha Tiku / Wired:\nDiversity advocates inside Google, including queer and transgender staff, say they are being targeted by a small group of coworkers with doxxing and harassment&nbsp; &mdash;&nbsp; FIRED GOOGLE ENGINEER James Damore says he was vilified and harassed for ...",
                        "pubDate": "Fri, 26 Jan 2018 15:20:16 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p3#a180126p3",
                        "id": "0000516"
                    },
                    {
                        "title": "China's smartphone market suffered first ever annual decline in 2017 as shipments fell 4% YoY to 459M and 14% QoQ in Q4, but Huawei grew shipments 9% QoQ (Canalys)",
                        "link": "http://www.techmeme.com/180126/p4#a180126p4",
                        "body": "Canalys:\nChina's smartphone market suffered first ever annual decline in 2017 as shipments fell 4% YoY to 459M and 14% QoQ in Q4, but Huawei grew shipments 9% QoQ&nbsp; &mdash;&nbsp; Palo Alto, Shanghai, Singapore and Reading (UK) - Thursday, 25 January 2018&nbsp; &mdash;&nbsp; ...",
                        "pubDate": "Fri, 26 Jan 2018 16:05:05 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p4#a180126p4",
                        "id": "0000515"
                    },
                    {
                        "title": "Japanese cryptocurrency exchange Coincheck says $400M worth of NEM cryptocurrency tokens have been \"illicitly\" transferred out of the exchange (Yuji Nakamura/Bloomberg)",
                        "link": "http://www.techmeme.com/180126/p5#a180126p5",
                        "body": "Yuji Nakamura / Bloomberg:\nJapanese cryptocurrency exchange Coincheck says $400M worth of NEM cryptocurrency tokens have been &ldquo;illicitly&rdquo; transferred out of the exchange&nbsp; &mdash;&nbsp; Coincheck Inc., one of Japan's biggest digital exchanges, said that about ...",
                        "pubDate": "Fri, 26 Jan 2018 16:33:56 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p5#a180126p5",
                        "id": "0000514"
                    },
                    {
                        "title": "Renders of upcoming Samsung Galaxy S9 and S9+ leak, both look very similar to their S8 predecessors (Evan Blass/VentureBeat)",
                        "link": "http://www.techmeme.com/180126/p6#a180126p6",
                        "body": "Evan Blass / VentureBeat:\nRenders of upcoming Samsung Galaxy S9 and S9+ leak, both look very similar to their S8 predecessors&nbsp; &mdash;&nbsp; EXCLUSIVE:&nbsp; &mdash;&nbsp; Having opted against what would have been an uncharacteristic debut at the recent Consumer Electronics ...",
                        "pubDate": "Fri, 26 Jan 2018 17:05:04 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p6#a180126p6",
                        "id": "0000513"
                    },
                    {
                        "title": "Bloomberg says its Twitter video channel, TicToc, now has 750K daily viewers, 1M daily views, and 119K followers, with half of the audience outside the US (Lucinda Southern/Digiday)",
                        "link": "http://www.techmeme.com/180126/p7#a180126p7",
                        "body": "Lucinda Southern / Digiday:\nBloomberg says its Twitter video channel, TicToc, now has 750K daily viewers, 1M daily views, and 119K followers, with half of the audience outside the US&nbsp; &mdash;&nbsp; Bloomberg is seeing early signs of success one month after launching TicToc, ...",
                        "pubDate": "Fri, 26 Jan 2018 17:40:03 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p7#a180126p7",
                        "id": "0000512"
                    },
                    {
                        "title": "Mobius, which is building a Stripe-like payments platform for cryptocurrencies, raises $39M in its ICO, uses Stellar blockchain instead of Ethereum (Brady Dale/CoinDesk)",
                        "link": "http://www.techmeme.com/180126/p8#a180126p8",
                        "body": "Brady Dale / CoinDesk:\nMobius, which is building a Stripe-like payments platform for cryptocurrencies, raises $39M in its ICO, uses Stellar blockchain instead of Ethereum&nbsp; &mdash;&nbsp; &ldquo;We look at ethereum like AOL or Myspace.&rdquo;&nbsp; &mdash;&nbsp; That's how ...",
                        "pubDate": "Fri, 26 Jan 2018 18:30:03 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p8#a180126p8",
                        "id": "0000511"
                    },
                    {
                        "title": "Facebook will allow fans of game streamers to tip creators a minimum of $3 as part of its new gaming creator pilot program (Josh Constine/TechCrunch)",
                        "link": "http://www.techmeme.com/180126/p9#a180126p9",
                        "body": "Josh Constine / TechCrunch:\nFacebook will allow fans of game streamers to tip creators a minimum of $3 as part of its new gaming creator pilot program&nbsp; &mdash;&nbsp; Facebook Live is launching monetization for video gameplay streamers, allowing users to tip creators a ...",
                        "pubDate": "Fri, 26 Jan 2018 19:17:40 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p9#a180126p9",
                        "id": "0000510"
                    },
                    {
                        "title": "Maersk says NonPetya ransomware attack required an almost complete infrastructure overhaul, reinstallation of 4,000 servers, 45,000 PCs, and 2,500 applications (Charlie Osborne/ZDNet)",
                        "link": "http://www.techmeme.com/180126/p10#a180126p10",
                        "body": "Charlie Osborne / ZDNet:\nMaersk says NonPetya ransomware attack required an almost complete infrastructure overhaul, reinstallation of 4,000 servers, 45,000 PCs, and 2,500 applications&nbsp; &mdash;&nbsp; The shipping giant has suffered millions of dollars in damage due to the ...",
                        "pubDate": "Fri, 26 Jan 2018 19:55:04 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p10#a180126p10",
                        "id": "0000509"
                    },
                    {
                        "title": "Twitter now lets advertisers sponsor Moments from select publishers (Sarah Perez/TechCrunch)",
                        "link": "http://www.techmeme.com/180126/p11#a180126p11",
                        "body": "Sarah Perez / TechCrunch:\nTwitter now lets advertisers sponsor Moments from select publishers&nbsp; &mdash;&nbsp; Twitter has added a new advertising product to its lineup.&nbsp; The company announced today it's offering brands the ability to sponsor Moments - the ...",
                        "pubDate": "Fri, 26 Jan 2018 20:05:03 GMT",
                        "permaLink": "http://www.techmeme.com/180126/p11#a180126p11",
                        "id": "0000508"
                    }
                ]
            },
            {
                "feedTitle": "memeorandum",
                "feedUrl": "http://www.memeorandum.com/feed.xml",
                "websiteUrl": "http://www.memeorandum.com/",
                "feedDescription": "Political Web, page A1",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:55:00 GMT",
                "item": [
                    {
                        "title": "Steve Wynn, Republican finance chair and Las Vegas mogul, accused of sexually abusing employees (German Lopez/Vox)",
                        "link": "http://www.memeorandum.com/180126/p82#a180126p82",
                        "body": "German Lopez / Vox:\nSteve Wynn, Republican finance chair and Las Vegas mogul, accused of sexually abusing employees&nbsp; &mdash;&nbsp; Dozens of Wynn's employees alleged what amounts to decades of sexual abuse in a new Wall Street Journal report.&nbsp; &mdash;&nbsp; Republican ...",
                        "pubDate": "Fri, 26 Jan 2018 19:35:00 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p82#a180126p82",
                        "id": "0000507"
                    },
                    {
                        "title": "National Gallery cancels Chuck Close, Thomas Roma shows; artists accused of sexual harassment (Peggy McGlone/Washington Post)",
                        "link": "http://www.memeorandum.com/180126/p83#a180126p83",
                        "body": "Peggy McGlone / Washington Post:\nNational Gallery cancels Chuck Close, Thomas Roma shows; artists accused of sexual harassment&nbsp; &mdash;&nbsp; The National Gallery of Art will not present solo exhibitions by painter Chuck Close and photographer Thomas Roma, both of whom have ...",
                        "pubDate": "Fri, 26 Jan 2018 19:40:03 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p83#a180126p83",
                        "id": "0000506"
                    },
                    {
                        "title": "Exclusive: Frustrated State Department employees hire attorneys, charging 'political retribution' (Elise Labott/CNN)",
                        "link": "http://www.memeorandum.com/180126/p84#a180126p84",
                        "body": "Elise Labott / CNN:\nExclusive: Frustrated State Department employees hire attorneys, charging &lsquo;political retribution&rsquo;&nbsp; &mdash;&nbsp; Ex diplomat: US ability to &lsquo;avert disasters&rsquo; at risk&nbsp; &mdash;&nbsp; STORY HIGHLIGHTS&nbsp; &mdash; Several ...",
                        "pubDate": "Fri, 26 Jan 2018 19:45:02 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p84#a180126p84",
                        "id": "0000505"
                    },
                    {
                        "title": "Trump plans to ask for $716 billion for national defense in 2019 - a major increase (Washington Post)",
                        "link": "http://www.memeorandum.com/180126/p86#a180126p86",
                        "body": "Washington Post:\nTrump plans to ask for $716 billion for national defense in 2019 &mdash; a major increase &hellip; President Trump is expected to ask for $716 billion in defense spending when he unveils his 2019 budget next month, a major increase that signals a shift away from ...",
                        "pubDate": "Fri, 26 Jan 2018 20:05:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p86#a180126p86",
                        "id": "0000504"
                    },
                    {
                        "title": "Draft GOP resolution would flatter Joe Arpaio, then urge him to quit Senate race (Dan Nowicki/Arizona Republic)",
                        "link": "http://www.memeorandum.com/180126/p90#a180126p90",
                        "body": "Dan Nowicki / Arizona Republic:\nDraft GOP resolution would flatter Joe Arpaio, then urge him to quit Senate race&nbsp; &mdash;&nbsp; A draft Arizona Republican Party resolution would heap praise on former Maricopa County Sheriff Joe Arpaio's long career in law enforcement and ...",
                        "pubDate": "Fri, 26 Jan 2018 20:10:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p90#a180126p90",
                        "id": "0000503"
                    },
                    {
                        "title": "Why Is Pay Lagging? Maybe Too Many Mergers in the Heartland (New York Times)",
                        "link": "http://www.memeorandum.com/180126/p89#a180126p89",
                        "body": "New York Times:\nWhy Is Pay Lagging?&nbsp; Maybe Too Many Mergers in the Heartland&nbsp; &mdash;&nbsp; Consolidation is often seen as a consumer problem.&nbsp; But it may also reduce competition for workers, especially outside big cities, holding down wages.&nbsp; &mdash;&nbsp; ...",
                        "pubDate": "Fri, 26 Jan 2018 20:10:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p89#a180126p89",
                        "id": "0000502"
                    },
                    {
                        "title": "Boeing's trade petition against Canadian rival Bombardier rejected (Megan Cassella/Politico)",
                        "link": "http://www.memeorandum.com/180126/p88#a180126p88",
                        "body": "Megan Cassella / Politico:\nBoeing's trade petition against Canadian rival Bombardier rejected&nbsp; &mdash;&nbsp; The U.S. International Trade Commission sided with Montreal-based aircraft manufacturer Bombardier on Friday when it said it found no evidence that U.S.-based Boeing ...",
                        "pubDate": "Fri, 26 Jan 2018 20:10:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p88#a180126p88",
                        "id": "0000501"
                    },
                    {
                        "title": "Key Republican Dials Back Effort to Protect Robert Mueller From Donald Trump (Betsy Woodruff/The Daily Beast)",
                        "link": "http://www.memeorandum.com/180126/p87#a180126p87",
                        "body": "Betsy Woodruff / The Daily Beast:\nKey Republican Dials Back Effort to Protect Robert Mueller From Donald Trump &hellip; One of the first Senate Republicans to call for legislation protecting special counsel Robert Mueller has stopped actively pushing that effort, sources on ...",
                        "pubDate": "Fri, 26 Jan 2018 20:10:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p87#a180126p87",
                        "id": "0000500"
                    },
                    {
                        "title": "Trump's latest interview shows he's not really the president (Matthew Yglesias/Vox)",
                        "link": "http://www.memeorandum.com/180126/p92#a180126p92",
                        "body": "Matthew Yglesias / Vox:\nTrump's latest interview shows he's not really the president&nbsp; &mdash;&nbsp; He's holding the office but not doing the job.&nbsp; &mdash;&nbsp; President Donald Trump's first non-Fox television interview in a long time, conducted with CNBC's Joe ...",
                        "pubDate": "Fri, 26 Jan 2018 20:20:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p92#a180126p92",
                        "id": "0000499"
                    },
                    {
                        "title": "Steve Wynn, RNC finance chairman, faces allegations of sexual misconduct (Ed O'Keefe/Washington Post)",
                        "link": "http://www.memeorandum.com/180126/p91#a180126p91",
                        "body": "Ed O'Keefe / Washington Post:\nSteve Wynn, RNC finance chairman, faces allegations of sexual misconduct&nbsp; &mdash;&nbsp; Casino mogul Steve Wynn has no immediate plans to relinquish his role as finance chairman of the Republican National Committee in the wake of reports ...",
                        "pubDate": "Fri, 26 Jan 2018 20:20:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p91#a180126p91",
                        "id": "0000498"
                    },
                    {
                        "title": "The Price of Taking Down Larry Nassar (Rachael Denhollander/New York Times)",
                        "link": "http://www.memeorandum.com/180126/p93#a180126p93",
                        "body": "Rachael Denhollander / New York Times:\nThe Price of Taking Down Larry Nassar&nbsp; &mdash;&nbsp; On Jan. 16, women and girls from across the country began congregating in a courtroom in Lansing, Mich. Some of us were athletes; some of us were not.&nbsp; Some of us were white; ...",
                        "pubDate": "Fri, 26 Jan 2018 20:30:07 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p93#a180126p93",
                        "id": "0000497"
                    },
                    {
                        "title": "Jeb Bush Reams Marco Rubio on Immigration: 'God Forbid' You Do Anything Controversial (Gideon Resnick/The Daily Beast)",
                        "link": "http://www.memeorandum.com/180126/p95#a180126p95",
                        "body": "Gideon Resnick / The Daily Beast:\nJeb Bush Reams Marco Rubio on Immigration: &lsquo;God Forbid&rsquo; You Do Anything Controversial &hellip; The two former Republican presidential candidates best known for bucking the party on immigration found themselves at odds over the issue ...",
                        "pubDate": "Fri, 26 Jan 2018 20:40:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p95#a180126p95",
                        "id": "0000496"
                    },
                    {
                        "title": "These 6 GOP tweets just became very awkward (Judd Legum/ThinkProgress)",
                        "link": "http://www.memeorandum.com/180126/p94#a180126p94",
                        "body": "Judd Legum / ThinkProgress:\nThese 6 GOP tweets just became very awkward&nbsp; &mdash;&nbsp; Steve Wynn has raised and donated millions for Trump.&nbsp; &mdash;&nbsp; On Friday, the Wall Street Journal published a bombshell report about Steve Wynn, the Las Vegas casino mogul, ...",
                        "pubDate": "Fri, 26 Jan 2018 20:40:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p94#a180126p94",
                        "id": "0000495"
                    },
                    {
                        "title": "A Father, A Husband, An Immigrant: Detained And Facing Deportation (NPR)",
                        "link": "http://www.memeorandum.com/180126/p96#a180126p96",
                        "body": "NPR:\nA Father, A Husband, An Immigrant: Detained And Facing Deportation&nbsp; &mdash;&nbsp; The day was going to be perfect.&nbsp; &mdash;&nbsp; Alex figured he would wake up at 6:30 a.m., help get his little brothers up and off to school and catch the bus by 7.&nbsp; After ...",
                        "pubDate": "Fri, 26 Jan 2018 20:45:01 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p96#a180126p96",
                        "id": "0000494"
                    },
                    {
                        "title": "Mueller's Team Has Interviewed Facebook Staff as Part of Russia Probe (Issie Lapowsky/Wired)",
                        "link": "http://www.memeorandum.com/180126/p97#a180126p97",
                        "body": "Issie Lapowsky / Wired:\nMueller's Team Has Interviewed Facebook Staff as Part of Russia Probe&nbsp; &mdash;&nbsp; THE DEPARTMENT OF Justice's special counsel Robert Mueller and his office have interviewed at least one member of Facebook's team that was associated with President ...",
                        "pubDate": "Fri, 26 Jan 2018 20:50:02 GMT",
                        "permaLink": "http://www.memeorandum.com/180126/p97#a180126p97",
                        "id": "0000493"
                    }
                ]
            },
            {
                "feedTitle": "Mediagazer",
                "feedUrl": "http://mediagazer.com/feed.xml",
                "websiteUrl": "http://mediagazer.com/",
                "feedDescription": "Must-read media news",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:59 GMT",
                "item": [
                    {
                        "title": "Elizabeth Hawley, longtime Reuters correspondent in Nepal and the premier chronicler of Himalayan expeditions, dies at 95 (Kunda Dixit/Nepali Times)",
                        "link": "http://mediagazer.com/180126/p1#a180126p1",
                        "body": "Kunda Dixit / Nepali Times:\nElizabeth Hawley, longtime Reuters correspondent in Nepal and the premier chronicler of Himalayan expeditions, dies at 95&nbsp; &mdash;&nbsp; Elizabeth Hawley was working at the desk for Fortune magazine as a reporter when she first came to Nepal in ...",
                        "pubDate": "Fri, 26 Jan 2018 05:00:05 GMT",
                        "permaLink": "http://mediagazer.com/180126/p1#a180126p1",
                        "id": "0000492"
                    },
                    {
                        "title": "Sources: Chinese live-video streaming startup Kuaishou is close to raising $1B led by Tencent at an ~$18B valuation and plans to IPO in the second half of 2018 (Reuters)",
                        "link": "http://mediagazer.com/180126/p3#a180126p3",
                        "body": "Reuters:\nSources: Chinese live-video streaming startup Kuaishou is close to raising $1B led by Tencent at an ~$18B valuation and plans to IPO in the second half of 2018&nbsp; &mdash;&nbsp; Julie Zhu, Kane Wu&nbsp; &mdash;&nbsp; HONG KONG (Reuters) - Chinese live-video streaming ...",
                        "pubDate": "Fri, 26 Jan 2018 10:20:06 GMT",
                        "permaLink": "http://mediagazer.com/180126/p3#a180126p3",
                        "id": "0000490"
                    },
                    {
                        "title": "Walmart to start selling ebooks, audiobooks, and ereaders powered by Rakuten's digital book arm, Kobo, as part of Walmart-Rakuten alliance (Scott Hilton/Walmart)",
                        "link": "http://mediagazer.com/180126/p4#a180126p4",
                        "body": "Scott Hilton / Walmart:\nWalmart to start selling ebooks, audiobooks, and ereaders powered by Rakuten's digital book arm, Kobo, as part of Walmart-Rakuten alliance&nbsp; &mdash;&nbsp; Today, we announced a strategic alliance with Rakuten, the number one e-commerce company in ...",
                        "pubDate": "Fri, 26 Jan 2018 12:35:06 GMT",
                        "permaLink": "http://mediagazer.com/180126/p4#a180126p4",
                        "id": "0000489"
                    },
                    {
                        "title": "Cable news outlets CNN, Fox, and MSNBC devoted a scant 16+ minutes total to coverage of the Kentucky high school shooting (Media Matters for America)",
                        "link": "http://mediagazer.com/180126/p5#a180126p5",
                        "body": "Media Matters for America:\nCable news outlets CNN, Fox, and MSNBC devoted a scant 16+ minutes total to coverage of the Kentucky high school shooting&nbsp; &mdash;&nbsp; A deadly shooting rampage at a Kentucky high school that left two students dead and 18 others wounded received ...",
                        "pubDate": "Fri, 26 Jan 2018 13:40:06 GMT",
                        "permaLink": "http://mediagazer.com/180126/p5#a180126p5",
                        "id": "0000488"
                    },
                    {
                        "title": "China's Bytedance partners with BuzzFeed to distribute its content on Toutiao, though how Toutiao will select content not subject to censorship is unclear (Reuters)",
                        "link": "http://mediagazer.com/180126/p6#a180126p6",
                        "body": "Reuters:\nChina's Bytedance partners with BuzzFeed to distribute its content on Toutiao, though how Toutiao will select content not subject to censorship is unclear&nbsp; &mdash;&nbsp; BEIJING/SHANGHAI (Reuters) - A popular Chinese news portal has signed a deal with U.S. online ...",
                        "pubDate": "Fri, 26 Jan 2018 14:25:02 GMT",
                        "permaLink": "http://mediagazer.com/180126/p6#a180126p6",
                        "id": "0000487"
                    },
                    {
                        "title": "Six male BBC on-air presenters agree to pay cuts following the resignation of Carrie Gracie as China editor because of pay inequity (BBC)",
                        "link": "http://mediagazer.com/180126/p7#a180126p7",
                        "body": "BBC:\nSix male BBC on-air presenters agree to pay cuts following the resignation of Carrie Gracie as China editor because of pay inequity&nbsp; &mdash;&nbsp; Six of the BBC's leading male presenters have agreed to take pay cuts after revelations over equal salaries.&nbsp; ...",
                        "pubDate": "Fri, 26 Jan 2018 15:55:00 GMT",
                        "permaLink": "http://mediagazer.com/180126/p7#a180126p7",
                        "id": "0000486"
                    },
                    {
                        "title": "Jemele Hill, the ESPN anchor called out by Trump in a tweet after she called him a white supremacist, is leaving SportsCenter to join The Undefeated (Richard Deitsch/Sports Illustrated)",
                        "link": "http://mediagazer.com/180126/p8#a180126p8",
                        "body": "Richard Deitsch / Sports Illustrated:\nJemele Hill, the ESPN anchor called out by Trump in a tweet after she called him a white supremacist, is leaving SportsCenter to join The Undefeated&nbsp; &mdash;&nbsp; ESPN's SportsCenter will need a new co-host as Jemele Hill is expected ...",
                        "pubDate": "Fri, 26 Jan 2018 16:10:00 GMT",
                        "permaLink": "http://mediagazer.com/180126/p8#a180126p8",
                        "id": "0000485"
                    },
                    {
                        "title": "Interview with BuzzFeed CEO Jonah Peretti, on Facebook's News Feed changes and lack of revenue sharing, BuzzFeed's revenue miss in 2017, and more (Mathew Ingram/Columbia Journalism Review)",
                        "link": "http://mediagazer.com/180126/p9#a180126p9",
                        "body": "Mathew Ingram / Columbia Journalism Review:\nInterview with BuzzFeed CEO Jonah Peretti, on Facebook's News Feed changes and lack of revenue sharing, BuzzFeed's revenue miss in 2017, and more&nbsp; &mdash;&nbsp; Photo courtesy BuzzFeed.&nbsp; &mdash;&nbsp; BuzzFeed co-founder and ...",
                        "pubDate": "Fri, 26 Jan 2018 17:05:00 GMT",
                        "permaLink": "http://mediagazer.com/180126/p9#a180126p9",
                        "id": "0000484"
                    },
                    {
                        "title": "How Outside magazine's online edition practices diversity and transparency by establishing quotas for new writers, a 50/50 male/female ratio, and more (Laura Hazard Owen/Nieman Lab)",
                        "link": "http://mediagazer.com/180126/p11#a180126p11",
                        "body": "Laura Hazard Owen / Nieman Lab:\nHow Outside magazine's online edition practices diversity and transparency by establishing quotas for new writers, a 50/50 male/female ratio, and more&nbsp; &mdash;&nbsp; A lot of publications are paying lip service to inclusiveness and ...",
                        "pubDate": "Fri, 26 Jan 2018 19:00:00 GMT",
                        "permaLink": "http://mediagazer.com/180126/p11#a180126p11",
                        "id": "0000482"
                    },
                    {
                        "title": "In June 2017, Hannity and guests called on Robert Mueller to resign, recuse himself, or be fired 39 times, claimed Mueller had a conflict of interest 102 times (Nick Fernandez/Media Matters for America)",
                        "link": "http://mediagazer.com/180126/p13#a180126p13",
                        "body": "Nick Fernandez / Media Matters for America:\nIn June 2017, Hannity and guests called on Robert Mueller to resign, recuse himself, or be fired 39 times, claimed Mueller had a conflict of interest 102 times&nbsp; &mdash;&nbsp; The New York Times reported that in June 2017, ...",
                        "pubDate": "Fri, 26 Jan 2018 20:00:05 GMT",
                        "permaLink": "http://mediagazer.com/180126/p13#a180126p13",
                        "id": "0000480"
                    },
                    {
                        "title": "Emails show Texas Monthly had a deal with Bumble, for app to pay for social media promotion of Texas Monthly cover story featuring Bumble's founder (Alexandria Neason/Columbia Journalism ...)",
                        "link": "http://mediagazer.com/180126/p15#a180126p15",
                        "body": "Alexandria Neason / Columbia Journalism Review:\nEmails show Texas Monthly had a deal with Bumble, for app to pay for social media promotion of Texas Monthly cover story featuring Bumble's founder&nbsp; &mdash;&nbsp; The skyline of Austin, Texas, where Texas Monthly is ...",
                        "pubDate": "Fri, 26 Jan 2018 20:40:10 GMT",
                        "permaLink": "http://mediagazer.com/180126/p15#a180126p15",
                        "id": "0000478"
                    }
                ]
            },
            {
                "feedTitle": "Wirecutter: Reviews for the Real World",
                "feedUrl": "http://feeds.feedburner.com/TheWirecutter",
                "websiteUrl": "https://thewirecutter.com/",
                "feedDescription": "A List of Great Technology",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:59 GMT",
                "item": [
                    {
                        "title": "The Best VR Headsets for PC and PS4",
                        "link": "https://thewirecutter.com/reviews/best-desktop-virtual-reality-headset/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After years of waiting, you can now choose from among three competent VR systems for turning your TV room into a one-person holodeck. VR is still a new technology with a lot of kinks to be ironed out (far from a “most people” purchase), but after spending hours using each ...",
                        "pubDate": "Thu, 18 Jan 2018 21:08:05 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-desktop-virtual-reality-headset/#comments",
                        "id": "0000477"
                    },
                    {
                        "title": "The Best Soundbar",
                        "link": "https://thewirecutter.com/reviews/best-soundbar/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After conducting a thorough survey of Wirecutter readers and combining those insights with the results of our previous testing, we think the Sonos Playbar or Playbase is the right soundbar for most people, depending on the type of TV you own. Both sound fantastic, and are easier ...",
                        "pubDate": "Fri, 19 Jan 2018 16:45:28 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-soundbar/#comments",
                        "id": "0000476"
                    },
                    {
                        "title": "The Best Space Heaters",
                        "link": "https://thewirecutter.com/reviews/best-space-heaters/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After 95 hours of research, including tests of more than three dozen heaters and comparisons of nearly 100 different competitors over the past six years, we still think the Lasko 754200 Ceramic Heater is the best space heater for most people. While most space heaters work fine, ...",
                        "pubDate": "Fri, 19 Jan 2018 20:27:32 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-space-heaters/#comments",
                        "id": "0000475"
                    },
                    {
                        "title": "The Best External Desktop Hard Drive",
                        "link": "https://thewirecutter.com/reviews/the-best-external-desktop-hard-drive/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After 20 hours of new research and testing, we found the best external desktop hard drive for most people is Seagate’s 4 TB Backup Plus Desktop Drive. The Backup Plus has a great balance of speed and price and enough space for your future storage needs.",
                        "pubDate": "Mon, 22 Jan 2018 17:10:30 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/the-best-external-desktop-hard-drive/#comments",
                        "id": "0000474"
                    },
                    {
                        "title": "The Best microSD Cards",
                        "link": "https://thewirecutter.com/reviews/best-microsd-card/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After 9 hours of research and testing, we found that the speedy and affordable 64 GB Samsung Evo Select MB-ME64GA is the best microSD card for most people’s needs. It has the fastest random speeds for the price, making it ideal for storing and running apps on your phone or ...",
                        "pubDate": "Mon, 22 Jan 2018 17:46:24 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-microsd-card/#comments",
                        "id": "0000473"
                    },
                    {
                        "title": "The Best 15-Inch Laptops for Photo and Video Editing",
                        "link": "https://thewirecutter.com/reviews/best-15-inch-laptops-for-photo-and-video-editing/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After spending 30 hours researching laptops, speaking to 11 creative professionals, and testing the top five contenders, we found that the Dell XPS 15 is the best laptop for most creative work, especially photo and video editing. The XPS 15 has the most powerful processor and ...",
                        "pubDate": "Mon, 22 Jan 2018 18:15:58 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-15-inch-laptops-for-photo-and-video-editing/#comments",
                        "id": "0000472"
                    },
                    {
                        "title": "The Best Water Bottles",
                        "link": "https://thewirecutter.com/reviews/best-water-bottle/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "We’ve conducted almost 70 hours of research into water bottles over the past five years and have now tested a total of 79 in all shapes and sizes. But because different people have different priorities, we couldn’t pick just one, so we’ve chosen our favorites across a broad ...",
                        "pubDate": "Mon, 22 Jan 2018 18:41:24 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-water-bottle/#comments",
                        "id": "0000471"
                    },
                    {
                        "title": "The Best Portable SSD",
                        "link": "https://thewirecutter.com/reviews/best-portable-ssd/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After researching 28 external solid-state drives and testing the four most promising contenders in 2017, we found that the best portable SSD is the 500 GB Samsung T5 Portable SSD. Samsung’s solid-state drives work reliably, and the T5 was consistently speedier than the ...",
                        "pubDate": "Mon, 22 Jan 2018 19:30:45 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-portable-ssd/#respond",
                        "id": "0000470"
                    },
                    {
                        "title": "The Best USB 3.0 Flash Drive",
                        "link": "https://thewirecutter.com/reviews/the-best-usb-3-0-thumb-drive/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After more than 14 hours of new research and testing, we found that the 64 GB SanDisk Extreme CZ80 USB 3.0 Flash Drive is still the best flash drive for most people—while you can get it. With both PCs and Macs, the SanDisk Extreme CZ80 offers the best combination of speed, ...",
                        "pubDate": "Mon, 22 Jan 2018 21:44:10 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/the-best-usb-3-0-thumb-drive/#comments",
                        "id": "0000469"
                    },
                    {
                        "title": "The Best Comforter",
                        "link": "https://thewirecutter.com/reviews/best-comforter/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After a combined 180 hours researching more than 100 comforters, and testing dozens, we’d go for the the Snowe Down Comforter. Lots of down comforters are lofty, warm, and well-constructed. The real trick is finding one that’s durable and well-priced. The Snowe is it. It beat ...",
                        "pubDate": "Mon, 22 Jan 2018 22:22:21 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-comforter/#comments",
                        "id": "0000468"
                    },
                    {
                        "title": "The Best All-in-One Computer",
                        "link": "https://thewirecutter.com/reviews/best-all-in-one-computer/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After more than 35 hours of research and testing, we found that the HP Envy 27 All-in-One is the best all-in-one computer for most people, because it combines ample processing power with a gorgeous 27-inch display in a stylish, compact design. Best of all, it costs less than ...",
                        "pubDate": "Mon, 22 Jan 2018 22:27:56 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-all-in-one-computer/#comments",
                        "id": "0000467"
                    },
                    {
                        "title": "The Best Wi-Fi Mesh-Networking Kits for Most People",
                        "link": "https://thewirecutter.com/reviews/best-wi-fi-mesh-networking-kits/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "Mesh-networking kits, which use multiple access points spread around your house to improve the range and performance of your Wi-Fi, instead of using a single router, are great for large homes or old apartments or houses with plaster, brick, or concrete walls. After spending over ...",
                        "pubDate": "Mon, 22 Jan 2018 22:52:54 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-wi-fi-mesh-networking-kits/#comments",
                        "id": "0000466"
                    },
                    {
                        "title": "The Best Home 3D Printer for Beginners",
                        "link": "https://thewirecutter.com/reviews/best-home-3d-printer/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After spending 18 hours researching, 22 hours setting up, and 240 hours printing with eight of the best beginner-focused 3D printers we could find, we think the Tiertime Up Mini 2 is the best choice for most people just starting out with 3D printing at home. It has an unmatched ...",
                        "pubDate": "Tue, 23 Jan 2018 14:16:50 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-home-3d-printer/#comments",
                        "id": "0000465"
                    },
                    {
                        "title": "The Best TV",
                        "link": "https://thewirecutter.com/reviews/best-tv/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After spending 80 hours testing new models for 2017, we picked the 55-inch TCL 55P607 as the best for most people because it’s the best value we have ever seen in a TV. It produces images with more detail, brightness, and color than most TVs that cost hundreds more. Even when ...",
                        "pubDate": "Tue, 23 Jan 2018 18:28:35 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-tv/#comments",
                        "id": "0000464"
                    },
                    {
                        "title": "The Best 4K TV on a Budget",
                        "link": "https://thewirecutter.com/reviews/best-4k-tv/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After spending 80 hours testing new models for 2017, we found that the 55-inch TCL 55P607 is the best 4K TV if you’re on a budget. Not only that, but it’s also our pick for the best TV, period. Even when viewed side by side with TVs that cost two and a half times as much, the ...",
                        "pubDate": "Tue, 23 Jan 2018 18:29:04 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-4k-tv/#comments",
                        "id": "0000463"
                    },
                    {
                        "title": "Why We Love the Raspberry Pi",
                        "link": "https://thewirecutter.com/reviews/raspberry-pi/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "Anytime someone asks me how to turn their weird tech project into reality, my immediate instinct is to recommend the Raspberry Pi. This $35 computer, the size of a deck of cards, is as capable as it is cheap. With just a bit of know-how and curiosity, you can use it to make a ...",
                        "pubDate": "Tue, 23 Jan 2018 19:37:03 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/raspberry-pi/#comments",
                        "id": "0000462"
                    },
                    {
                        "title": "The Best Drones",
                        "link": "https://thewirecutter.com/reviews/best-drones/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After 25 hours of research and several days of real-world flight and photography with nine leading models, we’ve found that the DJI Mavic Pro is the best drone for most aspiring aerial photographers and videographers. It matches or beats similarly priced competitors in image ...",
                        "pubDate": "Tue, 23 Jan 2018 20:05:41 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-drones/#comments",
                        "id": "0000461"
                    },
                    {
                        "title": "The Best Wireless Workout Headphones",
                        "link": "https://thewirecutter.com/reviews/best-wireless-exercise-headphones/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After testing 136 sets of headphones and considering an additional 90, we are yet again convinced that the JLab Epic2 is the best pair of wireless workout headphones for most people, because they sound good, fit comfortably, and stay out of your way during rigorous workouts. Our ...",
                        "pubDate": "Tue, 23 Jan 2018 20:40:50 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-wireless-exercise-headphones/#comments",
                        "id": "0000460"
                    },
                    {
                        "title": "The Best HomeKit-Compatible Smart-Home Devices",
                        "link": "https://thewirecutter.com/reviews/best-homekit-devices/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "Apple’s HomeKit is a user-friendly platform for anyone who owns an iOS device and is interested in a simple, reliable DIY smart-home system. With a couple of HomeKit-compatible devices and a recent iPhone or iPad, it’s realistic to start from scratch and have an integrated ...",
                        "pubDate": "Tue, 23 Jan 2018 20:53:58 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-homekit-devices/#comments",
                        "id": "0000459"
                    },
                    {
                        "title": "The Best Windows Ultrabook",
                        "link": "https://thewirecutter.com/reviews/the-best-windows-ultrabook/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "Ultrabooks are marvels of engineering: powerful laptops with long battery life wrapped up in thin and light packages. Even though almost all ultrabooks are good nowadays, only a few are great. After putting in more than 70 hours of research and testing nine laptops in 2017, we ...",
                        "pubDate": "Tue, 23 Jan 2018 21:39:20 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/the-best-windows-ultrabook/#comments",
                        "id": "0000458"
                    },
                    {
                        "title": "The Best Cheap Projector",
                        "link": "https://thewirecutter.com/reviews/best-500-projector/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After 70 hours testing five projectors, we’ve concluded that the BenQ TH670 is the best cheap projector. It offers the best contrast ratio and the best color quality of everything we tested, as well as 1080p resolution, low input lag for video games, integrated speakers, plenty ...",
                        "pubDate": "Wed, 24 Jan 2018 22:16:28 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-500-projector/#comments",
                        "id": "0000457"
                    },
                    {
                        "title": "The Best Smart Thermostat",
                        "link": "https://thewirecutter.com/reviews/the-best-thermostat/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "We spent three months testing nine popular smart thermostats, and after considering design, features, ease of use, how comfortable they kept our home, and integrations with popular smart-home systems, the Nest Thermostat E is our top pick. The best smart thermostat is one that ...",
                        "pubDate": "Wed, 24 Jan 2018 22:21:23 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/the-best-thermostat/#comments",
                        "id": "0000456"
                    },
                    {
                        "title": "Why Do Tide Pods Stain Laundry? We Might Have the Answer",
                        "link": "https://thewirecutter.com/blog/why-do-tide-pods-stain-laundry-we-might-have-the-answer/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "Since I started working on our guide to the best laundry detergent back in 2014, I’ve noticed a steady stream of people reporting Tide detergents leaving permanent purple stains on their laundry. Recently, Becca Laurie wrote at The Outline about Tide Pods, that candy-like ...",
                        "pubDate": "Thu, 25 Jan 2018 14:56:30 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/blog/why-do-tide-pods-stain-laundry-we-might-have-the-answer/#comments",
                        "id": "0000455"
                    },
                    {
                        "title": "The Best Gear for the Big Game",
                        "link": "https://thewirecutter.com/blog/best-gear-for-the-big-game/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "Whether you’re inviting just a few friends over or planning the party of the year, you can rely on our picks to take the guesswork out of hosting a Big Game viewing party. If you’ve been looking for an excuse to upgrade your TV, or bite the bullet on that extra seating you’ve ...",
                        "pubDate": "Thu, 25 Jan 2018 15:53:06 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/blog/best-gear-for-the-big-game/#comments",
                        "id": "0000454"
                    },
                    {
                        "title": "The Best Laundry Detergent",
                        "link": "https://thewirecutter.com/reviews/the-best-laundry-detergent/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "Laundry detergent has to simultaneously attract and repel dirt, then rinse away without damaging your clothes, your washer, your skin, or the environment. After doing 85 hours of research, examining 782 stain swatches, running 456 pounds of laundry, testing 42 different ...",
                        "pubDate": "Thu, 25 Jan 2018 21:28:39 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/the-best-laundry-detergent/#comments",
                        "id": "0000453"
                    },
                    {
                        "title": "The Best College Dorm Essentials",
                        "link": "https://thewirecutter.com/reviews/college-school-supplies/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "Going to college for the first time can be daunting, but having a dorm room that feels more like a home can make a huge difference. That’s why we’ve spent more than 150 hours over the past three summers evaluating the best dorm bedding, appliances, and other essentials to ease ...",
                        "pubDate": "Thu, 25 Jan 2018 21:31:35 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/college-school-supplies/#comments",
                        "id": "0000452"
                    },
                    {
                        "title": "The Best Smart Lock",
                        "link": "https://thewirecutter.com/reviews/the-best-smart-lock/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "After more than two years of testing, including ongoing use of our previous picks and extensive head-to-head competition among leading smart locks, we continue to recommend the Kwikset Kevo Smart Lock 2nd Gen with the optional Kevo Plus upgrade (a separate $100 in-app purchase) ...",
                        "pubDate": "Thu, 25 Jan 2018 21:45:55 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/the-best-smart-lock/#comments",
                        "id": "0000451"
                    },
                    {
                        "title": "The Best Android Phones",
                        "link": "https://thewirecutter.com/reviews/best-android-phone/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "We spend dozens of hours each year testing the latest Android smartphones in everyday use, and we think the 5-inch Google Pixel 2 is the best Android phone for most people. It has the fastest performance of any Android phone we’ve tested, a best-in-class 12.2-megapixel camera, ...",
                        "pubDate": "Thu, 25 Jan 2018 22:31:09 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-android-phone/#comments",
                        "id": "0000450"
                    },
                    {
                        "title": "The Best VR Headset for Your Phone",
                        "link": "https://thewirecutter.com/reviews/best-vr-headset-for-your-phone/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "&lt;!-- --&gt;\n\t\t\t\n\tWe put in 35 hours of research and testing, and ultimately the Samsung Gear VR (SM-R325) narrowly beat out the Google Daydream View as the best mobile virtual reality headset because it has many more apps and games available. However, both options work with ...",
                        "pubDate": "Thu, 25 Jan 2018 22:36:58 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-vr-headset-for-your-phone/#comments",
                        "id": "0000449"
                    },
                    {
                        "title": "The Best True Wireless Headphones So Far",
                        "link": "https://thewirecutter.com/reviews/best-true-wireless-headphones/?utm_source=rss&utm_medium=feed&utm_campaign=RSS%20Feed",
                        "body": "We tested 21 sets of true wireless in-ear headphones and found that totally wireless earbuds remain a first-generation technology with a few kinks to work out. Every set we tested had flaws in fit, functionality, convenience, compatibility, or a combination of all four. But if ...",
                        "pubDate": "Fri, 26 Jan 2018 00:19:14 GMT",
                        "permaLink": "",
                        "comments": "https://thewirecutter.com/reviews/best-true-wireless-headphones/#comments",
                        "id": "0000448"
                    }
                ]
            },
            {
                "feedTitle": "The Cannabist",
                "feedUrl": "http://www.thecannabist.co/feed/",
                "websiteUrl": "https://www.thecannabist.co/",
                "feedDescription": "Denver, Colorado marijuana news, culture, resources",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:59 GMT",
                "item": [
                    {
                        "title": "N.J. medical marijuana program 'stifled' under Christie administration, says Gov. Murphy",
                        "link": "https://www.thecannabist.co/2018/01/23/new-jersey-medical-marijuana-review-murphy/97360/",
                        "body": "Gov. Phil Murphy is ordering his administration to review New Jersey’s medical marijuana program, which has just 15,000 enrollees.",
                        "pubDate": "Tue, 23 Jan 2018 18:16:53 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/23/new-jersey-medical-marijuana-review-murphy/97360/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/09/AP_17158085834423.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "341",
                                "width": "512"
                            }
                        ],
                        "id": "0000447"
                    },
                    {
                        "title": "Timing of cannabis company's donation to California councilwoman raises questions",
                        "link": "https://www.thecannabist.co/2018/01/24/timing-cannabis-companys-donation-california-councilwoman-raises-questions/97417/",
                        "body": "The day after Councilwoman Monica Garcia recommended a company get a monopoly on transporting cannabis, she received a $4,400 campaign contribution from the company’s CEO.",
                        "pubDate": "Wed, 24 Jan 2018 13:25:08 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/timing-cannabis-companys-donation-california-councilwoman-raises-questions/97417/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/cali-506x400.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "400",
                                "width": "506"
                            }
                        ],
                        "id": "0000446"
                    },
                    {
                        "title": "Medical marijuana patient barred from volunteering to care for hospice patients",
                        "link": "https://www.thecannabist.co/2018/01/24/medical-marijuana-volunteer-hospice/97422/",
                        "body": "In one California hospital, medical marijuana patients can’t volunteer to help comfort patients in their dying days.",
                        "pubDate": "Wed, 24 Jan 2018 14:09:38 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/medical-marijuana-volunteer-hospice/97422/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2014/06/xxxwebZSS26OASISTHERAPY_2-560x371.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "371",
                                "width": "560"
                            }
                        ],
                        "id": "0000445"
                    },
                    {
                        "title": "Could Sessions' marijuana policy shift benefit the cannabis industry in 2018?",
                        "link": "https://www.thecannabist.co/2018/01/24/colorado-marijuana-ncia-sessions/97403/",
                        "body": "The A.G.'s unilateral moves have forced politicians to take sides on marijuana legalization, the leader of the National Cannabis Industry Association told a gathering of the group's Colorado members.",
                        "pubDate": "Wed, 24 Jan 2018 20:23:04 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/colorado-marijuana-ncia-sessions/97403/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/ap17291527649528-1-560x373.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "373",
                                "width": "560"
                            }
                        ],
                        "id": "0000444"
                    },
                    {
                        "title": "Alliance of California marijuana growers file lawsuit to block vast cultivation operations",
                        "link": "https://www.thecannabist.co/2018/01/24/california-marijuana-growers-lawsuit/97462/",
                        "body": "\"Approving large cultivation operations in 2018 will significantly reduce the ability of small and medium cultivators to compete economically in the regulated market,\" the lawsuit said.",
                        "pubDate": "Wed, 24 Jan 2018 21:26:47 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/california-marijuana-growers-lawsuit/97462/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/AP18024088047391-560x392.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "392",
                                "width": "560"
                            }
                        ],
                        "id": "0000443"
                    },
                    {
                        "title": "Cannabis growers Aurora, CanniMed agree to a $1 billion merger",
                        "link": "https://www.thecannabist.co/2018/01/24/aurora-cannabis-cannimed-billion-merger/97491/",
                        "body": "The sweetened C$1.23 billion ($1 billion) cash-and-stock deal would be the largest yet in the country's red-hot cannabis industry.",
                        "pubDate": "Wed, 24 Jan 2018 22:32:37 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/aurora-cannabis-cannimed-billion-merger/97491/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/12/canada-money-2-560x347.png",
                                "type": "image/png",
                                "length": null,
                                "height": "347",
                                "width": "560"
                            }
                        ],
                        "id": "0000442"
                    },
                    {
                        "title": "Alaska marijuana testing facilities to be audited after \"significant deviation\" in potency results",
                        "link": "https://www.thecannabist.co/2018/01/24/alaska-marijuana-audit-potency/97503/",
                        "body": "Erika McConnell, director of the Alcohol and Marijuana Control Office, cited evidence of “significant deviation” in potency-testing results of the same product by different labs.",
                        "pubDate": "Wed, 24 Jan 2018 23:40:27 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/alaska-marijuana-audit-potency/97503/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/09/dpo-slack_-com-c1urlhttp3a2f2fw-2f102fap16298782494891-1-560x373.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "373",
                                "width": "560"
                            }
                        ],
                        "id": "0000441"
                    },
                    {
                        "title": "Colorado sticking with METRC for marijuana seed to sale tracking",
                        "link": "https://www.thecannabist.co/2018/01/24/colorado-marijuana-tracking-system/97394/",
                        "body": "The state has relied on Farnwell's METRC seed to sale tracking tech since 2011",
                        "pubDate": "Wed, 24 Jan 2018 17:31:58 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/colorado-marijuana-tracking-system/97394/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/04/seed-to-sale-1-560x373.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "373",
                                "width": "560"
                            }
                        ],
                        "id": "0000440"
                    },
                    {
                        "title": "U.S. Attorney for Massachusetts: focus is on opioids, not marijuana",
                        "link": "https://www.thecannabist.co/2018/01/24/massachusetts-marijuana-us-attorney-andrew-lelling/97474/",
                        "body": "But U.S. Attorney Andrew Lelling said he believes marijuana is a dangerous drug and refused to rule out prosecuting those working in the industry.",
                        "pubDate": "Wed, 24 Jan 2018 21:55:55 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/massachusetts-marijuana-us-attorney-andrew-lelling/97474/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/US-Attorney-Andrew-Lelling-393x400.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "400",
                                "width": "393"
                            }
                        ],
                        "id": "0000439"
                    },
                    {
                        "title": "Where marijuana legalization stands in New Mexico",
                        "link": "https://www.thecannabist.co/2018/01/24/new-mexico-marijuana-possession-laws/97512/",
                        "body": "New Mexico gubernatorial candidates have proposed reducing penalties for possession of small amounts of marijuana, or legalizing it.",
                        "pubDate": "Wed, 24 Jan 2018 23:43:19 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/new-mexico-marijuana-possession-laws/97512/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/10/AP17272721802423-560x348.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "348",
                                "width": "560"
                            }
                        ],
                        "id": "0000438"
                    },
                    {
                        "title": "Denver hydroponics retail chain expands to New England, improves positioning in legal cannabis states",
                        "link": "https://www.thecannabist.co/2018/01/24/growgeneration-hydroponics-acquisition-cannabis/97501/",
                        "body": "The acquisition of East Coast Hydroponics is expected to add $5 million in annual revenue and continues GrowGeneration’s strategy of positioning the company’s brand in legal cannabis states, officials said.",
                        "pubDate": "Thu, 25 Jan 2018 00:08:37 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/growgeneration-hydroponics-acquisition-cannabis/97501/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/GrowGeneration_shop-560x355.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "355",
                                "width": "560"
                            }
                        ],
                        "id": "0000437"
                    },
                    {
                        "title": "Were raided cannabis dispensaries targeted after police dispute?",
                        "link": "https://www.thecannabist.co/2018/01/25/santa-ana-sky-high-cannabis-raids-2018/97539/",
                        "body": "Sky High Holistic, a previous target of Santa Ana police, was one of two dispensaries closed by city officials Tuesday morning.",
                        "pubDate": "Thu, 25 Jan 2018 17:22:25 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/25/santa-ana-sky-high-cannabis-raids-2018/97539/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/raid-jan23-560x315.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "315",
                                "width": "560"
                            }
                        ],
                        "id": "0000436"
                    },
                    {
                        "title": "Trump's 24-year-old drug policy appointee will step down amid controversy",
                        "link": "https://www.thecannabist.co/2018/01/25/weymouth-trump-24-year-old-drug-policy-quits/97531/",
                        "body": "A 24-year-old former Trump campaign worker who rose rapidly to a senior post in the White House Office of National Drug Control Policy will step down by the end of the month.",
                        "pubDate": "Thu, 25 Jan 2018 14:29:03 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/25/weymouth-trump-24-year-old-drug-policy-quits/97531/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/white-house-560x347.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "347",
                                "width": "560"
                            }
                        ],
                        "id": "0000435"
                    },
                    {
                        "title": "TD Ameritrade CEO notes 'pronounced' increase in cannabis, blockchain trading activity",
                        "link": "https://www.thecannabist.co/2018/01/24/td-ameritrade-cannabis-blockchain/97362/",
                        "body": "“This doesn’t surprise us, given the significant media coverage on this topic,” TD Ameritrade CEO Tim Hockey said.",
                        "pubDate": "Wed, 24 Jan 2018 22:41:37 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/24/td-ameritrade-cannabis-blockchain/97362/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/Tim-Hockey_TD-Ameritrade-CEO-560x373.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "373",
                                "width": "560"
                            }
                        ],
                        "id": "0000434"
                    },
                    {
                        "title": "Nebraska bill would eliminate state's tax on illegal drugs",
                        "link": "https://www.thecannabist.co/2018/01/25/nebraska-bill-eliminate-state-tax-illegal-drugs/97536/",
                        "body": "A state lawmaker says Nebraska's tax on illegal drugs may violate constitutional rights and that virtually no one pays it until they're caught.",
                        "pubDate": "Thu, 25 Jan 2018 16:44:47 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/25/nebraska-bill-eliminate-state-tax-illegal-drugs/97536/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/civil-asset-forfeiture-editorial-1-560x373.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "373",
                                "width": "560"
                            }
                        ],
                        "id": "0000433"
                    },
                    {
                        "title": "Indiana House unanimously calls for study of medical marijuana",
                        "link": "https://www.thecannabist.co/2018/01/25/indiana-medical-marijuana-study/97585/",
                        "body": "The House voted 94-0 on Thursday in favor of a resolution by Berne Rep. Matt Lehman, which calls for a legislative commission to study medical marijuana.",
                        "pubDate": "Thu, 25 Jan 2018 21:55:24 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/25/indiana-medical-marijuana-study/97585/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/12/CBD_oil_supplement_rj_14280-560x388.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "388",
                                "width": "560"
                            }
                        ],
                        "id": "0000432"
                    },
                    {
                        "title": "After sounding alarm about increased crime, Sessions credits Trump with crime rate drop in first half of 2017",
                        "link": "https://www.thecannabist.co/2018/01/25/us-crime-rates-sessions-credits-trump/97546/",
                        "body": "Sessions is making the case Trump administration anti-crime policies are working, even though the trend already was apparent when he repeatedly warned that crime was out of control.",
                        "pubDate": "Thu, 25 Jan 2018 20:46:53 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/25/us-crime-rates-sessions-credits-trump/97546/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2014/01/handcuff-560x373.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "373",
                                "width": "560"
                            }
                        ],
                        "id": "0000431"
                    },
                    {
                        "title": "Oregon triples penalties for selling marijuana to minors",
                        "link": "https://www.thecannabist.co/2018/01/26/oregon-underage-marijuana-sales-penalties/97609/",
                        "body": "Oregon has announced it will temporarily increase penalties for those who unintentionally sell marijuana to minors.",
                        "pubDate": "Fri, 26 Jan 2018 16:27:33 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/26/oregon-underage-marijuana-sales-penalties/97609/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/ThinkstockPhotos-696265642-400x400.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "400",
                                "width": "400"
                            }
                        ],
                        "id": "0000430"
                    },
                    {
                        "title": "Wholesale cannabis prices hit historic lows in January, but don't blame Sessions",
                        "link": "https://www.thecannabist.co/2018/01/25/marijuana-prices-national-wholesale/97244/",
                        "body": "The U.S. Spot Index for cannabis last week stood at a national average of $1,292 per pound, a 3.5% decline compared to the week prior, according to Cannabis Benchmarks.",
                        "pubDate": "Thu, 25 Jan 2018 22:52:11 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/25/marijuana-prices-national-wholesale/97244/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/12/potmoney115-1-560x385.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "385",
                                "width": "560"
                            }
                        ],
                        "id": "0000429"
                    },
                    {
                        "title": "Marijuana-based anti-seizure drug shows strong study results",
                        "link": "https://www.thecannabist.co/2018/01/25/marijuana-anti-seizure-epidolex-drug-study-gw/97547/",
                        "body": "Officials from GW Pharmaceuticals, the company that developed  Epidiolex announced promising results from a randomized study on 171 patients.",
                        "pubDate": "Thu, 25 Jan 2018 18:12:39 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/25/marijuana-anti-seizure-epidolex-drug-study-gw/97547/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/05/cbd-oil-medical-560x347.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "347",
                                "width": "560"
                            }
                        ],
                        "id": "0000428"
                    },
                    {
                        "title": "Mexican tourism secretary floats legalizing marijuana at resorts",
                        "link": "https://www.thecannabist.co/2018/01/26/mexico-resorts-marijuana-tourism-madrid/97615/",
                        "body": "Mexico's tourism secretary has suggested that legalizing marijuana could help reduce drug violence at big tourist resorts.",
                        "pubDate": "Fri, 26 Jan 2018 17:57:41 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/26/mexico-resorts-marijuana-tourism-madrid/97615/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/Enrique_de_la_Madrid_Cordero_2017-560x347.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "347",
                                "width": "560"
                            }
                        ],
                        "id": "0000427"
                    },
                    {
                        "title": "Sen. Elizabeth Warren, Rep. Jared Polis head up bipartisan letter to Trump urging him to restore Cole Memo",
                        "link": "https://www.thecannabist.co/2018/01/26/elizabeth-warren-jared-polis-marijuana-letter-trump/97623/",
                        "body": "A bicameral, bipartisan coalition of Congressional leaders Thursday asked President Trump to direct Attorney General Jeff Sessions to reinstate the Cole Memo.",
                        "pubDate": "Fri, 26 Jan 2018 19:05:13 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/26/elizabeth-warren-jared-polis-marijuana-letter-trump/97623/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/01/363256592ae5469896ba9dced04e59ee-1-560x371.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "371",
                                "width": "560"
                            }
                        ],
                        "id": "0000426"
                    },
                    {
                        "title": "Odds of Virginia decriminalizing or legalizing marijuana this year just plummeted",
                        "link": "https://www.thecannabist.co/2018/01/26/virginia-marijuana-legalization-panel/97605/",
                        "body": "A key panel in the Virginia state House has rejected a proposal to decriminalize possession of small amounts of marijuana.",
                        "pubDate": "Fri, 26 Jan 2018 14:09:56 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/26/virginia-marijuana-legalization-panel/97605/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2017/10/ap17212613922738-1-560x373.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "373",
                                "width": "560"
                            }
                        ],
                        "id": "0000425"
                    },
                    {
                        "title": "Authorities raid marijuana grow, lab in central Colorado",
                        "link": "https://www.thecannabist.co/2018/01/26/woodland-park-colorado-marijuana-raid/97649/",
                        "body": "Authorities say marijuana and hash oil were seized after raiding an illegal grow and processing lab in a rural area in central Colorado.",
                        "pubDate": "Fri, 26 Jan 2018 20:37:01 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/26/woodland-park-colorado-marijuana-raid/97649/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/dtsthzhumaagcw4-1-533x400.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "400",
                                "width": "533"
                            }
                        ],
                        "id": "0000424"
                    },
                    {
                        "title": "Is X Games ready to embrace Aspen's extreme marijuana scene?",
                        "link": "https://www.thecannabist.co/2018/01/26/aspen-x-games-mairjuana-scene/97599/",
                        "body": "In her new column, High Country, Katie Shapiro fires up the cannabis conversation at X Games Aspen, where weed is prevalent at après ski parties, but but cultural acceptance remains illusive.",
                        "pubDate": "Fri, 26 Jan 2018 20:38:02 GMT",
                        "permaLink": "",
                        "comments": "https://www.thecannabist.co/2018/01/26/aspen-x-games-mairjuana-scene/97599/#respond",
                        "enclosure": [
                            {
                                "url": "https://www.thecannabist.co/wp-content/uploads/2018/01/thallskibossrollingpapers-560x280.jpg",
                                "type": "image/jpeg",
                                "length": null,
                                "height": "280",
                                "width": "560"
                            }
                        ],
                        "id": "0000423"
                    }
                ]
            },
            {
                "feedTitle": "Scripting News",
                "feedUrl": "http://scripting.com/rss.xml",
                "websiteUrl": "http://scripting.com/",
                "feedDescription": "Scripting News, the weblog started in 1997 that bootstrapped the blogging revolution.",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:59 GMT",
                "item": [
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/16.html#a190814",
                        "body": "Last week it was a real struggle to find the RSS feed for the Slow Burn podcast, but we did find it, and a new episode just showed up on podcatch.com , so the loop closes. Happy. 💥",
                        "pubDate": "Tue, 16 Jan 2018 19:08:14 GMT",
                        "permaLink": "http://scripting.com/2018/01/16.html#a190814",
                        "outline": {
                            "text": "Last week it was a real struggle to find the RSS feed for the Slow Burn podcast, but we <a href=\"http://scripting.com/2018/01/06/170237.html\">did find it</a>, and a <a href=\"http://podcatch.com/archive/2018/01/16/850605.html\">new episode</a> just showed up on podcatch.com , so the loop closes. Happy. <span class=\"spOldSchoolEmoji\">💥</span>",
                            "created": "Tue, 16 Jan 2018 19:08:14 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/16.html#a190814"
                        },
                        "id": "0000422"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/16.html#a191011",
                        "body": "Just realized, the reason librarians must love the web, and linking, is that you can provide a complete bit of complex information without being overwhelming. It's the same reason I like coding in an outliner. There's no cost for being verbose, just tuck the verbosity under a ...",
                        "pubDate": "Tue, 16 Jan 2018 19:10:11 GMT",
                        "permaLink": "http://scripting.com/2018/01/16.html#a191011",
                        "outline": {
                            "text": "Just realized, the reason <a href=\"https://mcphee.com/products/librarian-action-figure\">librarians</a> must love the web, and linking, is that you can provide a complete bit of complex information without being overwhelming. It's the same reason I like coding in an outliner. There's no cost for being verbose, just tuck the verbosity under a headline and leave it collapsed. Until the day you wonder wtf is going on here. You can hide little crumb trails for later discovery. Links work the same way.",
                            "created": "Tue, 16 Jan 2018 19:10:11 GMT",
                            "type": "outline",
                            "image": "http://scripting.com/images/2018/01/16/librarianActionFigure.png",
                            "permalink": "http://scripting.com/2018/01/16.html#a191011"
                        },
                        "id": "0000421"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/16.html#a192726",
                        "body": "I'm hunting everywhere for my glasses and then realize I'm wearing them.",
                        "pubDate": "Tue, 16 Jan 2018 19:27:26 GMT",
                        "permaLink": "http://scripting.com/2018/01/16.html#a192726",
                        "outline": {
                            "text": "I'm hunting everywhere for my glasses and then realize I'm wearing them.",
                            "created": "Tue, 16 Jan 2018 19:27:26 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/16.html#a192726"
                        },
                        "id": "0000420"
                    },
                    {
                        "title": "How Trump stank up Haiti",
                        "link": "http://scripting.com/2018/01/17/154439.html",
                        "body": "I read this Politico piece about this history of Trump and Haiti.  \nInitially there was some bad data about a possible connection betw Haitians and AIDS that soon turned out to be false. But Trump kept bringing it up, and the stink on Haiti wouldn't go away. \nTrump is still ...",
                        "pubDate": "Wed, 17 Jan 2018 15:44:39 GMT",
                        "permaLink": "http://scripting.com/2018/01/17/154439.html",
                        "outline": {
                            "text": "How Trump stank up Haiti",
                            "created": "Wed, 17 Jan 2018 15:44:39 GMT",
                            "type": "outline",
                            "description": "Plenty of other people take the same kinds of detours just to hurt other people. If you live long enough you meet lots of them.",
                            "subs": [
                                {
                                    "text": "I read this <a href=\"https://www.politico.com/magazine/story/2018/01/15/aids-panic-trump-haitians-216324\">Politico piece</a> about this history of Trump and Haiti.",
                                    "created": "Wed, 17 Jan 2018 15:44:53 GMT",
                                    "permalink": "http://scripting.com/2018/01/17/154439.html#a154453"
                                },
                                {
                                    "text": "Initially there was some bad data about a possible connection betw Haitians and AIDS that soon turned out to be false. But Trump kept bringing it up, and the stink on Haiti wouldn't go away.",
                                    "created": "Wed, 17 Jan 2018 15:45:02 GMT",
                                    "permalink": "http://scripting.com/2018/01/17/154439.html#a154502"
                                },
                                {
                                    "text": "Trump is <i>still</i> putting the <a href=\"https://www.washingtonpost.com/news/the-fix/wp/2018/01/11/trumps-shithole-comment-about-haiti-lends-credence-to-report-that-he-said-its-residents-all-have-aids/?utm_term=.a69771f2031d\">stink</a> on Haiti.",
                                    "created": "Wed, 17 Jan 2018 15:47:08 GMT",
                                    "permalink": "http://scripting.com/2018/01/17/154439.html#a154708"
                                },
                                {
                                    "text": "Reading this reminds me of the stink that people in tech put on RSS. There never was anything wrong with RSS, no data behind any of the things that were said, but people, some who even thought we were friends, said some very ugly Trump-like things about RSS. (Actually even worse.)",
                                    "created": "Wed, 17 Jan 2018 15:45:31 GMT",
                                    "permalink": "http://scripting.com/2018/01/17/154439.html#a154531"
                                },
                                {
                                    "text": "That's the sad thing about Trump, not just that he is such a flawed awful human being who is our president, but that if you live long enough, you've met plenty of other people who take exactly those kinds of shortcuts just to hurt other people.",
                                    "created": "Wed, 17 Jan 2018 15:46:12 GMT",
                                    "permalink": "http://scripting.com/2018/01/17/154439.html#a154612"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/17/154439.html"
                        },
                        "id": "0000419"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/17.html#a160928",
                        "body": "Back when blogging was young, I was the chief blogger in some sense. I got a lot of shit. I was surprised then, not now. The picture people painted wasn't me. Reading their stories, I sounded like the little capitalist dude in the Monopoly game.",
                        "pubDate": "Wed, 17 Jan 2018 16:09:28 GMT",
                        "permaLink": "http://scripting.com/2018/01/17.html#a160928",
                        "outline": {
                            "text": "Back when blogging was young, I was the chief blogger in some sense. I got a lot of shit. I was surprised then, not now. The picture people painted wasn't me. Reading their stories, I sounded like the little capitalist dude in the Monopoly game.",
                            "created": "Wed, 17 Jan 2018 16:09:28 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/17.html#a160928"
                        },
                        "id": "0000418"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/17.html#a144740",
                        "body": "Piero Macchioni, an Italian journalist, on Feeds for Journalists.",
                        "pubDate": "Wed, 17 Jan 2018 14:47:40 GMT",
                        "permaLink": "http://scripting.com/2018/01/17.html#a144740",
                        "outline": {
                            "text": "<a href=\"https://macchioni.in/blog/2018-01-17-feed-rss-for-journalists/\">Piero Macchioni</a>, an Italian journalist, on <a href=\"https://github.com/scripting/feedsForJournalists\">Feeds for Journalists</a>.",
                            "created": "Wed, 17 Jan 2018 14:47:40 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/17.html#a144740"
                        },
                        "id": "0000417"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/17.html#a144523",
                        "body": "New this.how doc on Black Lives Matter.",
                        "pubDate": "Wed, 17 Jan 2018 14:45:23 GMT",
                        "permaLink": "http://scripting.com/2018/01/17.html#a144523",
                        "outline": {
                            "text": "New <a href=\"http://this.how/blackLivesMatter/\">this.how doc</a> on Black Lives Matter.",
                            "created": "Wed, 17 Jan 2018 14:45:23 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/17.html#a144523"
                        },
                        "id": "0000416"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/17.html#a031417",
                        "body": "If you're a journalist and have ideas for a feed or two or want to see the discussion so far, here's the place to look.",
                        "pubDate": "Thu, 18 Jan 2018 03:14:17 GMT",
                        "permaLink": "http://scripting.com/2018/01/17.html#a031417",
                        "outline": {
                            "text": "If you're a journalist and have ideas for a feed or two or want to see the discussion so far, here's the <a href=\"https://github.com/scripting/feedsForJournalists/issues/3\">place</a> to look.",
                            "created": "Thu, 18 Jan 2018 03:14:17 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/17.html#a031417"
                        },
                        "id": "0000415"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/17.html#a184518",
                        "body": "The Feeds for Journalists OPML file is now available. You can use this file to subsribe to all the feeds in any feed reader. It will be updated periodically, so check back. Even better if your reader allows you to subscribe to OPML files, a drum I've been beating for a long ...",
                        "pubDate": "Wed, 17 Jan 2018 18:45:18 GMT",
                        "permaLink": "http://scripting.com/2018/01/17.html#a184518",
                        "outline": {
                            "text": "The Feeds for Journalists <a href=\"https://github.com/scripting/feedsForJournalists/blob/master/list.opml\">OPML file</a> is now available. You can use this file to subsribe to all the feeds in any feed reader. It will be updated periodically, so check back. Even better if your reader allows you to subscribe to OPML files, a drum I've been <a href=\"http://scripting.com/2013/08/15/feedReaderDevs\">beating</a> for a long time. Then you'll get the updates automatically.",
                            "created": "Wed, 17 Jan 2018 18:45:18 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/17.html#a184518"
                        },
                        "id": "0000414"
                    },
                    {
                        "title": "Goodbye Huffington Post",
                        "link": "http://scripting.com/2018/01/18/145004.html",
                        "body": "I tried writing at Huffington Post, many years ago, hoping to get more flow. When I finally got a hot story on HP, here's what they did.\n\nRewrote it.\nRedirected traffic from my page to theirs.\n\nThat's when the great experiment ended. 💥",
                        "pubDate": "Thu, 18 Jan 2018 14:50:04 GMT",
                        "permaLink": "http://scripting.com/2018/01/18/145004.html",
                        "outline": {
                            "text": "Goodbye Huffington Post",
                            "created": "Thu, 18 Jan 2018 14:50:04 GMT",
                            "subs": [
                                {
                                    "text": "I tried writing at Huffington Post, many years ago, hoping to get more flow. When I finally got a hot story on HP, here's what they did.",
                                    "created": "Thu, 18 Jan 2018 14:50:10 GMT",
                                    "flnumberedsubs": "true",
                                    "subs": [
                                        {
                                            "text": "Rewrote it.",
                                            "created": "Thu, 18 Jan 2018 14:53:20 GMT",
                                            "permalink": "http://scripting.com/2018/01/18/145004.html#a145320"
                                        },
                                        {
                                            "text": "Redirected traffic from my page to theirs.",
                                            "created": "Thu, 18 Jan 2018 14:53:20 GMT",
                                            "permalink": "http://scripting.com/2018/01/18/145004.html#a145320"
                                        }
                                    ],
                                    "permalink": "http://scripting.com/2018/01/18/145004.html#a145010"
                                },
                                {
                                    "text": "That's when the great experiment ended. <span class=\"spOldSchoolEmoji\">💥</span>",
                                    "created": "Thu, 18 Jan 2018 14:50:30 GMT",
                                    "permalink": "http://scripting.com/2018/01/18/145004.html#a145030"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/18/145004.html"
                        },
                        "id": "0000413"
                    },
                    {
                        "title": "Little wrecked ecosystems",
                        "link": "http://scripting.com/2018/01/18/152557.html",
                        "body": "With Google Reader shutting down and Facebook pulling out of news, and now HuffPost withdrawing, I feel great. Vindicated. Optimistic once again.\nThere is no magic to platforms. Corporate platforms always end up as puddles. Little wrecked ecosystems that started with great ...",
                        "pubDate": "Thu, 18 Jan 2018 15:25:57 GMT",
                        "permaLink": "http://scripting.com/2018/01/18/152557.html",
                        "outline": {
                            "text": "Little wrecked ecosystems",
                            "created": "Thu, 18 Jan 2018 15:25:57 GMT",
                            "type": "outline",
                            "subs": [
                                {
                                    "text": "<a href=\"https://twitter.com/davewiner/status/954004529292480512\">With</a> Google Reader shutting down and Facebook pulling out of news, and now HuffPost withdrawing, I feel great. Vindicated. Optimistic once again.",
                                    "created": "Thu, 18 Jan 2018 15:26:06 GMT",
                                    "permalink": "http://scripting.com/2018/01/18/152557.html#a152606"
                                },
                                {
                                    "text": "There is no magic to platforms. Corporate platforms always end up as puddles. Little wrecked ecosystems that started with great bluster.",
                                    "created": "Thu, 18 Jan 2018 15:29:31 GMT",
                                    "permalink": "http://scripting.com/2018/01/18/152557.html#a152931"
                                },
                                {
                                    "text": "The only platforms worth developing for are ones without a platform vendor. That is, open platforms based on open formats and protocols.",
                                    "created": "Thu, 18 Jan 2018 15:38:44 GMT",
                                    "permalink": "http://scripting.com/2018/01/18/152557.html#a153844"
                                },
                                {
                                    "text": "I was <a href=\"https://twitter.com/ericmbudd/status/954004936500551681\">asked</a> why Google Reader is on my list.",
                                    "created": "Thu, 18 Jan 2018 15:38:45 GMT",
                                    "subs": [
                                        {
                                            "text": "They didn't support all of RSS, so blogging became limited to what Google Reader understood. And then they just threw it all out, like a massive oil spill, and did nothing to clean it up. In the end it would have been better if it never existed.",
                                            "created": "Thu, 18 Jan 2018 15:38:56 GMT",
                                            "permalink": "http://scripting.com/2018/01/18/152557.html#a153856"
                                        }
                                    ],
                                    "permalink": "http://scripting.com/2018/01/18/152557.html#a153845"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/18/152557.html"
                        },
                        "id": "0000412"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/18.html#a141729",
                        "body": "Where you see Americans trashing each other online, use your imagination. How are trolls, some Russian, some from elsewhere, instigating to make it worse, increase the hurt, damage, deepen the division, make it more permanent?",
                        "pubDate": "Thu, 18 Jan 2018 14:17:29 GMT",
                        "permaLink": "http://scripting.com/2018/01/18.html#a141729",
                        "outline": {
                            "text": "Where you see Americans trashing each other online, use your imagination. How are trolls, some Russian, some from elsewhere, instigating to make it worse, increase the hurt, damage, deepen the division, make it more permanent?",
                            "created": "Thu, 18 Jan 2018 14:17:29 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/18.html#a141729"
                        },
                        "id": "0000411"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/18.html#a141603",
                        "body": "I support the NYT turning over its editorial page to Trump voters. But we keep hearing from them. How about Clinton voters next. And black voters. And people who didn't vote. And so on. Let's hear more from people outside the elite bubble.",
                        "pubDate": "Thu, 18 Jan 2018 14:16:03 GMT",
                        "permaLink": "http://scripting.com/2018/01/18.html#a141603",
                        "outline": {
                            "text": "I support the NYT <a href=\"https://www.poynter.org/news/new-york-times-morphs-welcome-wagon-trump-supporters\">turning over</a> its editorial page to Trump voters. But we keep hearing from them. How about Clinton voters next. And black voters. And people who didn't vote. And so on. Let's hear more from people outside the elite bubble.",
                            "created": "Thu, 18 Jan 2018 14:16:03 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/18.html#a141603"
                        },
                        "id": "0000410"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/18.html#a152401",
                        "body": "Update on the Feeds for Journalists project. \"The current list doesn't have much meta-news or news-about-news. It's mostly just plain news. I am totally in favor of adding Canadian feeds, but for news orgs that are producing news about Canada, and news from a Canadian ...",
                        "pubDate": "Thu, 18 Jan 2018 15:24:01 GMT",
                        "permaLink": "http://scripting.com/2018/01/18.html#a152401",
                        "outline": {
                            "text": "<a href=\"https://github.com/scripting/feedsForJournalists/issues/3#issuecomment-358664338\">Update</a> on the <a href=\"https://github.com/scripting/feedsForJournalists\">Feeds for Journalists</a> project. \"The current list doesn't have much meta-news or news-about-news. It's mostly just plain news. I am totally in favor of adding Canadian feeds, but for news orgs that are producing news about Canada, and news from a Canadian perspective.\"",
                            "created": "Thu, 18 Jan 2018 15:24:01 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/18.html#a152401"
                        },
                        "id": "0000409"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/18.html#a201844",
                        "body": "Much of what I read in #metoo writing paints men solely as women haters. That's all there is to say about us. Your father, brother, son, uncle, cousin, friends, we only exist to hate women. That of course is not true. I think what we all want, men and women, is to relax, to be ...",
                        "pubDate": "Thu, 18 Jan 2018 20:18:44 GMT",
                        "permaLink": "http://scripting.com/2018/01/18.html#a201844",
                        "outline": {
                            "text": "Much of what I read in #metoo writing paints men solely as women haters. That's all there is to say about us. Your father, brother, son, uncle, cousin, friends, we only exist to hate women. That of course is not true. I think what we all want, men and women, is to relax, to be loved, to feel safe, pleasure, acceptance, and secure for the moment. That's imho most of what we're looking for in love. No specific gender.",
                            "created": "Thu, 18 Jan 2018 20:18:44 GMT",
                            "type": "outline",
                            "urlvideo": "https://www.youtube.com/embed/t9a1JQi7G3k",
                            "permalink": "http://scripting.com/2018/01/18.html#a201844"
                        },
                        "id": "0000408"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/18.html#a200203",
                        "body": "Journalists still think they were/are bloggers. Reporters using blogging tools is not blogging. Bloggers are reporters' sources. They say blogging is over because their professional CMSes caught up? They're in a bubble. A profession that should be good at listening only actually ...",
                        "pubDate": "Thu, 18 Jan 2018 20:02:03 GMT",
                        "permaLink": "http://scripting.com/2018/01/18.html#a200203",
                        "outline": {
                            "text": "Journalists <a href=\"https://twitter.com/unclegrambo/status/954010704490713088\">still think</a> they were/are bloggers. Reporters using blogging tools is not blogging. Bloggers are reporters' sources. <a href=\"https://www.newyorker.com/culture/cultural-comment/the-end-of-the-awl-and-the-vanishing-of-freedom-and-fun-from-the-internet\">They say</a> blogging is over because their professional CMSes caught up? They're in a <a href=\"https://en.wikipedia.org/wiki/Reality_distortion_field\">bubble</a>. A profession that should be <i>good</i> at listening only actually listens to itself.",
                            "created": "Thu, 18 Jan 2018 20:02:03 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/18.html#a200203"
                        },
                        "id": "0000407"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/18.html#a183200",
                        "body": "There was an outage on scripting.com earlier today. I let a domain expire, thinking it wasn't in use. Haha. Okay. I renewed it. It seems to be resolving here, so the outage should be over. Sorry. I'll be more careful in the future. 🚀",
                        "pubDate": "Thu, 18 Jan 2018 18:32:00 GMT",
                        "permaLink": "http://scripting.com/2018/01/18.html#a183200",
                        "outline": {
                            "text": "There was an outage on <a href=\"http://scripting.com/\">scripting.com</a> earlier today. I let a domain expire, thinking it wasn't in use. Haha. Okay. I renewed it. It seems to be resolving here, so the outage should be over. Sorry. I'll be more careful in the future. <span class=\"spOldSchoolEmoji\">🚀</span>",
                            "created": "Thu, 18 Jan 2018 18:32:00 GMT",
                            "type": "outline",
                            "image": "http://scripting.com/images/2018/01/18/travis.png",
                            "permalink": "http://scripting.com/2018/01/18.html#a183200"
                        },
                        "id": "0000406"
                    },
                    {
                        "title": "Other cutters",
                        "link": "http://scripting.com/2018/01/19/170459.html",
                        "body": "I was just talking with a friend, two ideas --\n\nskicutter -- like Wirecutter but for ski areas. Where's the best skiing right now.\nweedcutter -- same thing for weed.\n\nFor extra credit, cross-tabulate. 💥",
                        "pubDate": "Fri, 19 Jan 2018 17:04:59 GMT",
                        "permaLink": "http://scripting.com/2018/01/19/170459.html",
                        "outline": {
                            "text": "Other cutters",
                            "created": "Fri, 19 Jan 2018 17:04:59 GMT",
                            "type": "outline",
                            "subs": [
                                {
                                    "text": "I was just talking with a friend, two ideas --",
                                    "created": "Fri, 19 Jan 2018 17:05:03 GMT",
                                    "flnumberedsubs": "true",
                                    "subs": [
                                        {
                                            "text": "skicutter -- like <a href=\"https://thewirecutter.com/\">Wirecutter</a> but for ski areas. Where's the best skiing right now.",
                                            "created": "Fri, 19 Jan 2018 17:05:18 GMT",
                                            "permalink": "http://scripting.com/2018/01/19/170459.html#a170518"
                                        },
                                        {
                                            "text": "weedcutter -- same thing for weed.",
                                            "created": "Fri, 19 Jan 2018 17:05:21 GMT",
                                            "permalink": "http://scripting.com/2018/01/19/170459.html#a170521"
                                        }
                                    ],
                                    "permalink": "http://scripting.com/2018/01/19/170459.html#a170503"
                                },
                                {
                                    "text": "For extra credit, cross-tabulate. <span class=\"spOldSchoolEmoji\">💥</span>",
                                    "created": "Fri, 19 Jan 2018 17:05:33 GMT",
                                    "permalink": "http://scripting.com/2018/01/19/170459.html#a170533"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/19/170459.html"
                        },
                        "id": "0000405"
                    },
                    {
                        "title": "IPC in Node",
                        "link": "http://scripting.com/2018/01/20/213336.html",
                        "body": "Here's a theoretical question with practical implications.\nIn Node.js, is there a way to do interprocess communication between Node apps? I could set it up so both apps have an HTTP server, and the apps could communicate using XML-RPC, so at that level I know it's possible, but ...",
                        "pubDate": "Sat, 20 Jan 2018 21:33:36 GMT",
                        "permaLink": "http://scripting.com/2018/01/20/213336.html",
                        "outline": {
                            "text": "IPC in Node",
                            "created": "Sat, 20 Jan 2018 21:33:36 GMT",
                            "type": "outline",
                            "subs": [
                                {
                                    "text": "Here's a theoretical <a href=\"https://github.com/scripting/Scripting-News/issues/60\">question</a> with practical implications.",
                                    "created": "Sat, 20 Jan 2018 21:33:45 GMT",
                                    "image": "http://scripting.com/images/2018/01/20/rose.png",
                                    "permalink": "http://scripting.com/2018/01/20/213336.html#a213345"
                                },
                                {
                                    "text": "In Node.js, is there a way to do interprocess communication between Node apps? I could set it up so both apps have an HTTP server, and the apps could communicate using <a href=\"http://xmlrpc.scripting.com/\">XML-RPC</a>, so at that level I know it's possible, but I'm wondering about a different, higher-performance, approach.",
                                    "created": "Sat, 20 Jan 2018 21:55:46 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/213336.html#a215546"
                                },
                                {
                                    "text": "Suppose I launch two apps from my app. I would do it exactly the way the <a href=\"https://github.com/foreverjs/forever\">forever</a> utility does it. Is there some way for an app to call back to <i>forever, </i>and is there a way for <i>forever</i> to call into the app?",
                                    "created": "Sat, 20 Jan 2018 21:35:53 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/213336.html#a213553"
                                },
                                {
                                    "text": "That way you could have all kinds of external interfaces abstracted.",
                                    "created": "Sat, 20 Jan 2018 21:36:34 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/213336.html#a213634"
                                },
                                {
                                    "text": "All of a sudden you could build a high-level OS for Node. A way for Node apps to share a data space.",
                                    "created": "Sat, 20 Jan 2018 21:36:52 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/213336.html#a213652"
                                },
                                {
                                    "text": "This is what we tried to do on the Mac with Frontier. We never convinced Apple to stay out of the way so we could do it. But in Node, with its open source culture, if there was a way for <i>forever</i> to do it, that means there's a way for you and I to too, because of course <i>forever</i> is open source. ;-)",
                                    "created": "Sat, 20 Jan 2018 21:37:32 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/213336.html#a213732"
                                },
                                {
                                    "text": "Badaboom. <span class=\"spOldSchoolEmoji\">💥</span>",
                                    "created": "Sat, 20 Jan 2018 21:38:22 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/213336.html#a213822"
                                },
                                {
                                    "text": "PS: Of course I may be missing something obvious, a way to do this that I spaced out about. That does happen from time to time.",
                                    "created": "Sat, 20 Jan 2018 21:41:06 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/213336.html#a214106"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/20/213336.html"
                        },
                        "id": "0000404"
                    },
                    {
                        "title": "Can we trust the people?",
                        "link": "http://scripting.com/2018/01/20/220532.html",
                        "body": "Can the people be trusted to rate news sources?\nWhat choice do we have? I know experts think they have the means that we don't, but they accept a set of premises about what news should be, that lead us to news that\n\nAccepts that the US had to go to war with Iraq, and doesn't ...",
                        "pubDate": "Sat, 20 Jan 2018 22:05:32 GMT",
                        "permaLink": "http://scripting.com/2018/01/20/220532.html",
                        "outline": {
                            "text": "Can we trust the people?",
                            "created": "Sat, 20 Jan 2018 22:05:32 GMT",
                            "type": "outline",
                            "subs": [
                                {
                                    "text": "Can the people be trusted to rate news sources?",
                                    "created": "Sat, 20 Jan 2018 22:01:44 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/220532.html#a220144"
                                },
                                {
                                    "text": "What choice do we have? I know experts think they have the means that we don't, but they accept a set of premises about what news should be, that lead us to news that",
                                    "created": "Sat, 20 Jan 2018 22:05:49 GMT",
                                    "flnumberedsubs": "true",
                                    "subs": [
                                        {
                                            "text": "Accepts that the US had to go to war with Iraq, and doesn't question the assumption that there were WMDs and probably nukes in their arsenal.",
                                            "created": "Sat, 20 Jan 2018 22:06:25 GMT",
                                            "permalink": "http://scripting.com/2018/01/20/220532.html#a220625"
                                        },
                                        {
                                            "text": "Shows endless clips of Trump rallies as if that was news.",
                                            "created": "Sat, 20 Jan 2018 22:06:32 GMT",
                                            "permalink": "http://scripting.com/2018/01/20/220532.html#a220632"
                                        },
                                        {
                                            "text": "Asks ridiculous uninformed questions about Hillary Clinton's emails.",
                                            "created": "Sat, 20 Jan 2018 22:06:39 GMT",
                                            "permalink": "http://scripting.com/2018/01/20/220532.html#a220639"
                                        },
                                        {
                                            "text": "States conventional wisdom among reporters as fact.",
                                            "created": "Sat, 20 Jan 2018 22:11:02 GMT",
                                            "permalink": "http://scripting.com/2018/01/20/220532.html#a221102"
                                        },
                                        {
                                            "text": "Assumes the Trump story is pretty much Watergate, when it's clearly not.",
                                            "created": "Sat, 20 Jan 2018 22:07:45 GMT",
                                            "permalink": "http://scripting.com/2018/01/20/220532.html#a220745"
                                        },
                                        {
                                            "text": "Where reporters are all trying to get a prize or a raise, trying to catch politicians in gotchas, and filtering out all the politicians who just want to get shit done.",
                                            "created": "Sat, 20 Jan 2018 22:19:56 GMT",
                                            "permalink": "http://scripting.com/2018/01/20/220532.html#a221956"
                                        },
                                        {
                                            "text": "Endlessly analyzes what today's events mean for the 2020 election, when the answer is not one thing.",
                                            "created": "Sat, 20 Jan 2018 22:08:07 GMT",
                                            "permalink": "http://scripting.com/2018/01/20/220532.html#a220807"
                                        }
                                    ],
                                    "permalink": "http://scripting.com/2018/01/20/220532.html#a220549"
                                },
                                {
                                    "text": "The news orgs that report this way would be rated highest by the experts, no doubt. But we need news to be better. It's the one thing I agree about with Trump supporters. The standard \"trusted\" news attaches to ideas that they won't let go of. And reports on non-news endlessly. Can we rate that kind of stuff way way down?",
                                    "created": "Sat, 20 Jan 2018 22:06:47 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/220532.html#a220647"
                                },
                                {
                                    "text": "We need a lot from news that they aren't giving us, because they do everything they can to <i>not listen to the users.</i> If Facebook really wants to do this, don't listen to the experts on this, they answer the wrong questions, imho. Find a way for the users to decide. I know they don't trust us, but we're all we got.",
                                    "created": "Sat, 20 Jan 2018 22:09:19 GMT",
                                    "permalink": "http://scripting.com/2018/01/20/220532.html#a220919"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/20/220532.html"
                        },
                        "id": "0000403"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/20.html#a004855",
                        "body": "So much of what's tweeted amounts to this --> Trump is a piece of shit. You can stop saying it. We got it.",
                        "pubDate": "Sun, 21 Jan 2018 00:48:55 GMT",
                        "permaLink": "http://scripting.com/2018/01/20.html#a004855",
                        "outline": {
                            "text": "So much of what's tweeted amounts to this --> Trump is a piece of shit. You can stop saying it. We got it.",
                            "created": "Sun, 21 Jan 2018 00:48:55 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/20.html#a004855"
                        },
                        "id": "0000402"
                    },
                    {
                        "title": "An XML vs JSON argument that's not bullshit",
                        "link": "http://scripting.com/2018/01/21/145156.html",
                        "body": "Most of the XML vs JSON stuff is bullshit, but this one is not. Having worked extensively with both formats, I have to say jerf has got it right. But there are two criteria that need to be added:\n\n4. If the format was designed before JSON existed, use XML.\n5. If there already is ...",
                        "pubDate": "Sun, 21 Jan 2018 14:51:56 GMT",
                        "permaLink": "http://scripting.com/2018/01/21/145156.html",
                        "outline": {
                            "text": "An XML vs JSON argument that's not bullshit",
                            "created": "Sun, 21 Jan 2018 14:51:56 GMT",
                            "type": "outline",
                            "subs": [
                                {
                                    "text": "Most of the XML vs JSON stuff is <a href=\"http://thesaurus.land/?word=bullshit\">bullshit</a>, but <a href=\"https://news.ycombinator.com/item?id=11446984\">this one</a> is not. Having worked extensively with both formats, I have to say <a href=\"https://news.ycombinator.com/user?id=jerf\">jerf</a> has got it right. But there are two criteria that need to be added:",
                                    "created": "Sun, 21 Jan 2018 14:49:49 GMT",
                                    "subs": [
                                        {
                                            "text": "4. If the format was designed before JSON existed, use XML.",
                                            "created": "Sun, 21 Jan 2018 14:50:30 GMT",
                                            "permalink": "http://scripting.com/2018/01/21/145156.html#a145030"
                                        },
                                        {
                                            "text": "5. If there already is an XML format that does what you're doing, it's better to just use it, instead of inventing a new \"better\" one in JSON, because you'll still have to support the XML way of doing it, and net-net that's more complicated, more work and more confusing.",
                                            "created": "Sun, 21 Jan 2018 14:50:59 GMT",
                                            "permalink": "http://scripting.com/2018/01/21/145156.html#a145059"
                                        }
                                    ],
                                    "permalink": "http://scripting.com/2018/01/21/145156.html#a144949"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/21/145156.html"
                        },
                        "id": "0000401"
                    },
                    {
                        "title": "Walter Cronkite, trust, greatness and happiness",
                        "link": "http://scripting.com/2018/01/21/173411.html",
                        "body": "Talking with a friend, he said back in the day we trusted the people who gave us the news. Walter Cronkite was his example. \"And that's the way it is.\" \nI thought for a moment, he certainly seemed trustworthy, but viewed in hindsight, they were telling us lies, some big, some ...",
                        "pubDate": "Sun, 21 Jan 2018 17:34:11 GMT",
                        "permaLink": "http://scripting.com/2018/01/21/173411.html",
                        "outline": {
                            "text": "Walter Cronkite, trust, greatness and happiness",
                            "created": "Sun, 21 Jan 2018 17:34:11 GMT",
                            "type": "outline",
                            "description": "They, the ones on TV, knew everything, were infinitely wealthy, had everything they wanted, were loved by everyone they met.",
                            "subs": [
                                {
                                    "text": "Talking with a friend, he said back in the day we trusted the people who gave us the news. Walter Cronkite was his example. \"And that's the way it is.\"",
                                    "created": "Sun, 21 Jan 2018 17:59:04 GMT",
                                    "permalink": "http://scripting.com/2018/01/21/173411.html#a175904"
                                },
                                {
                                    "text": "I thought for a moment, he certainly <i>seemed</i> trustworthy, but viewed in hindsight, they were telling us lies, some big, some very big.",
                                    "created": "Sun, 21 Jan 2018 17:59:39 GMT",
                                    "permalink": "http://scripting.com/2018/01/21/173411.html#a175939"
                                },
                                {
                                    "text": "TV made me want to become one of the people on TV. They were happy. We couldn't be happy until we were one of them. I remember as a kid firmly believing this was true. They knew everything, were infinitely wealthy, had everything they wanted, were loved by everyone they met.",
                                    "permalink": "http://scripting.com/2018/01/21/173411.html#aNaNNaNNaN"
                                },
                                {
                                    "text": "Walter Cronkite, who may not have been aware he was selling that lie, was selling it all the same. It wasn't until I was well into adulthood that I realized that it was a lie. I had given up a lot, aspiring to greatness. Only to learn there is no such thing.",
                                    "permalink": "http://scripting.com/2018/01/21/173411.html#aNaNNaNNaN"
                                },
                                {
                                    "text": "So yes, I think we did trust Walter Cronkite, but here's the thing, we shouldn't have.",
                                    "permalink": "http://scripting.com/2018/01/21/173411.html#aNaNNaNNaN"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/21/173411.html"
                        },
                        "id": "0000400"
                    },
                    {
                        "title": "How to create a trustworthy flow of news for 2018",
                        "link": "http://scripting.com/2018/01/21/222843.html",
                        "body": "Forget about doing it on Facebook, it's a lost cause.\nInstead of trying to eliminate bad sources, identify a small number of good sources.\nCreate a news product with just those feeds. \nYou're done.",
                        "pubDate": "Sun, 21 Jan 2018 22:28:43 GMT",
                        "permaLink": "http://scripting.com/2018/01/21/222843.html",
                        "outline": {
                            "text": "How to create a trustworthy flow of news for 2018",
                            "created": "Sun, 21 Jan 2018 22:28:43 GMT",
                            "type": "outline",
                            "subs": [
                                {
                                    "text": "Forget about doing it on Facebook, it's a lost cause.",
                                    "created": "Sun, 21 Jan 2018 22:28:52 GMT",
                                    "permalink": "http://scripting.com/2018/01/21/222843.html#a222852"
                                },
                                {
                                    "text": "Instead of trying to eliminate bad sources, identify a small number of good sources.",
                                    "created": "Sun, 21 Jan 2018 22:29:22 GMT",
                                    "permalink": "http://scripting.com/2018/01/21/222843.html#a222922"
                                },
                                {
                                    "text": "Create a news product with just those feeds.",
                                    "created": "Sun, 21 Jan 2018 22:29:25 GMT",
                                    "permalink": "http://scripting.com/2018/01/21/222843.html#a222925"
                                },
                                {
                                    "text": "You're done.",
                                    "created": "Sun, 21 Jan 2018 22:28:53 GMT",
                                    "permalink": "http://scripting.com/2018/01/21/222843.html#a222853"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/21/222843.html"
                        },
                        "id": "0000399"
                    },
                    {
                        "title": "I should probably patent this but I won't",
                        "link": "http://scripting.com/2018/01/22/180119.html",
                        "body": "In a text chat with a friend, I was going to say something sexy but thought better of it. I should confirm that she welcomes it. \nYes means yes after all.\nThen I thought wow that would be a pretty cool feature to add to chat.\n I prototyped it in less than a minute. Of course I ...",
                        "pubDate": "Mon, 22 Jan 2018 18:01:19 GMT",
                        "permaLink": "http://scripting.com/2018/01/22/180119.html",
                        "outline": {
                            "text": "I should probably patent this but I won't",
                            "created": "Mon, 22 Jan 2018 18:01:19 GMT",
                            "type": "outline",
                            "subs": [
                                {
                                    "text": "In a text chat with a friend, I was going to say something sexy but thought better of it. I should confirm that she welcomes it.",
                                    "created": "Mon, 22 Jan 2018 18:04:26 GMT",
                                    "permalink": "http://scripting.com/2018/01/22/180119.html#a180426"
                                },
                                {
                                    "text": "<i>Yes means yes</i> after all.",
                                    "created": "Mon, 22 Jan 2018 18:04:50 GMT",
                                    "permalink": "http://scripting.com/2018/01/22/180119.html#a180450"
                                },
                                {
                                    "text": "Then I thought wow that would be a pretty cool feature to add to chat.",
                                    "created": "Mon, 22 Jan 2018 18:05:16 GMT",
                                    "permalink": "http://scripting.com/2018/01/22/180119.html#a180516"
                                },
                                {
                                    "text": "I <a href=\"http://scripting.com/images/2018/01/22/confirmationDialog.png\">prototyped</a> it in less than a minute. Of course I told her about it, and that became the joke, not the sexy thing, which I have now forgotten. <span class=\"spOldSchoolEmoji\">💥</span>",
                                    "created": "Mon, 22 Jan 2018 17:55:51 GMT",
                                    "type": "outline",
                                    "permalink": "http://scripting.com/2018/01/22/180119.html#a175551"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/22/180119.html"
                        },
                        "id": "0000398"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/22.html#a150722",
                        "body": "To Axios' web devs: The &lt;link> elements in the main Axios RSS feed are not pointing to the corresponding article. Details here.",
                        "pubDate": "Mon, 22 Jan 2018 15:07:22 GMT",
                        "permaLink": "http://scripting.com/2018/01/22.html#a150722",
                        "outline": {
                            "text": "To Axios' web devs: The &lt;link> elements in the <a href=\"https://www.axios.com/feeds/feed.rss\">main Axios RSS feed</a> are not pointing to the corresponding article. <a href=\"https://github.com/scripting/Scripting-News/issues/61\">Details here</a>.",
                            "created": "Mon, 22 Jan 2018 15:07:22 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/22.html#a150722"
                        },
                        "id": "0000397"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/22.html#a160535",
                        "body": "The editor of the Columbia Journalism Review is astonished by \"the ability of this former reality TV star to be our assignment editor.\" This echoes my constant urging for journalism to rep our interests, not that of the most successful troll of all time.",
                        "pubDate": "Mon, 22 Jan 2018 16:05:35 GMT",
                        "permaLink": "http://scripting.com/2018/01/22.html#a160535",
                        "outline": {
                            "text": "The editor of the Columbia Journalism Review is <a href=\"https://www.cjr.org/covering_trump/trump-coverage-inauguration-press-media.php\">astonished</a> by \"the ability of this former reality TV star to be our assignment editor.\" This echoes my constant <a href=\"http://thesaurus.land/?word=urge\">urging</a> for journalism to rep our interests, not that of the <a href=\"http://this.how/trolls/\">most successful troll</a> of all time.",
                            "created": "Mon, 22 Jan 2018 16:05:35 GMT",
                            "type": "outline",
                            "image": "http://scripting.com/images/2018/01/22/drNick.png",
                            "permalink": "http://scripting.com/2018/01/22.html#a160535"
                        },
                        "id": "0000396"
                    },
                    {
                        "title": "MSNBC stories",
                        "link": "http://scripting.com/2018/01/23/151820.html",
                        "body": "I heard a familiar idea at the end of the Chris Hayes show on MSNBC last night. The organizer of the Women's March and a reporter for the New Yorker both say they were ignored by Democratic legislators fighting for Dreamers. It's true, they were ignored. I didn't notice it until ...",
                        "pubDate": "Tue, 23 Jan 2018 15:18:20 GMT",
                        "permaLink": "http://scripting.com/2018/01/23/151820.html",
                        "outline": {
                            "text": "MSNBC stories",
                            "created": "Tue, 23 Jan 2018 15:18:20 GMT",
                            "type": "outline",
                            "description": "Before you make it about the Repubs, make it about the dream.",
                            "subs": [
                                {
                                    "text": "I heard a familiar idea at the end of the Chris Hayes show on MSNBC last night. The organizer of the Women's March and a reporter for the New Yorker both say they were ignored by Democratic legislators fighting for Dreamers. It's true, they were ignored. I didn't notice it until they said so. Why didn't the Dems use the fact that there were over a million people marching in America and more in other countries? The New Yorker writer says it's because they are women, to which I say two things: 1. I doubt it. 2. If it is, why not broaden the movement to be more inclusive? Can't we just have People's Marches? Why are women's issues the only ones that matter?",
                                    "created": "Tue, 23 Jan 2018 15:18:31 GMT",
                                    "image": "http://scripting.com/images/2018/01/16/librarianActionFigure.png",
                                    "permalink": "http://scripting.com/2018/01/23/151820.html#a151831"
                                },
                                {
                                    "text": "Earlier on the Ari Melber show, another woman whose name I didn't catch, made a similar powerful statement. Why is everything politicos talk about procedural? The Dreamers make an incredibly compelling story. How about some ads, you know the president is still campaigning, that show how the Dreamers are Americans! The stupid fucking Repubs want to deport Americans. But this idea if it's mentioned at all is only mentioned in passing. There's a powerful emotional story here, yet it is largely invisible. It's up to the Democrats, the opposition, to speak up for them. Not just procedurally, but personally.",
                                    "created": "Tue, 23 Jan 2018 15:20:25 GMT",
                                    "permalink": "http://scripting.com/2018/01/23/151820.html#a152025"
                                },
                                {
                                    "text": "In other words before you make it about the Repubs, make it about the dream. I keep pointing back to the incredibly emotional Bernie Sanders <a href=\"https://twitter.com/davewiner/status/955826325255852032\">America ad</a> from 2016. An ad like that showing a day in the life of dreamers across America would, as <a href=\"https://twitter.com/GeorgeLakoff?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor\">Lakoff</a> says, frame the debate in terms that make sense, not the procedural ones that to most people, including me, don't.",
                                    "created": "Tue, 23 Jan 2018 15:33:18 GMT",
                                    "permalink": "http://scripting.com/2018/01/23/151820.html#a153318"
                                },
                                {
                                    "text": "But the big problem is even bigger. Only a certain kind of person is given a voice. I think that's why the marchers were ignored. They don't work for Harvard or write for the New Yorker (sorry, but you get a lot of attention for your ideas there, I don't feel sorry for you). Unless you're a billionaire, or on the payroll of billionaires, your ideas and dreams don't mean shit.",
                                    "created": "Tue, 23 Jan 2018 15:22:55 GMT",
                                    "permalink": "http://scripting.com/2018/01/23/151820.html#a152255"
                                },
                                {
                                    "text": "That's what Trump sold, and he's right. I feel it. I feel left out of the conversation. I read a <a href=\"http://nymag.com/daily/intelligencer/2018/01/democracy-survived-a-year-of-trump-but-the-fights-not-over.html\">piece</a> by an excellent writer in New York magazine, who says we have to do more, well the biggest thing <i>he</i> could do is figure out how to empower people who don't have columns in his publication,  people who want to do anything they can to help but feel sidelined.",
                                    "created": "Tue, 23 Jan 2018 15:24:17 GMT",
                                    "permalink": "http://scripting.com/2018/01/23/151820.html#a152417"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/23/151820.html"
                        },
                        "id": "0000395"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/23.html#a161737",
                        "body": "What's really killing us, not just our country, is this idea that we're all in it for ourselves. To feed our families. It's a dream, from evolution. It's no longer a survival trait. The only way we survive is if we see our fates as intertwined, co-dependent. That's reality.",
                        "pubDate": "Tue, 23 Jan 2018 16:17:37 GMT",
                        "permaLink": "http://scripting.com/2018/01/23.html#a161737",
                        "outline": {
                            "text": "What's really killing us, not just our country, is this idea that we're all in it for ourselves. To feed our families. It's a dream, from evolution. It's no longer a survival trait. The only way we survive is if we see our fates as intertwined, co-dependent. That's reality.",
                            "created": "Tue, 23 Jan 2018 16:17:37 GMT",
                            "type": "outline",
                            "urltweet": "https://twitter.com/davewiner/status/955835575239675905",
                            "permalink": "http://scripting.com/2018/01/23.html#a161737"
                        },
                        "id": "0000394"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/23.html#a160418",
                        "body": "I'm sure people don't get why I keep saying people should use the web. It's selfish, I admit it. I want to be able to make tools for you. And to connect your ideas with others. If you do it inside a silo, only the employees of the silo can help. I can't, unless you use the web.",
                        "pubDate": "Tue, 23 Jan 2018 16:04:18 GMT",
                        "permaLink": "http://scripting.com/2018/01/23.html#a160418",
                        "outline": {
                            "text": "I'm sure people don't get why I keep saying people should use the web. It's selfish, I admit it. I want to be able to make tools for you. And to connect your ideas with others. If you do it inside a silo, only the employees of the silo can help. I can't, unless you use the web.",
                            "created": "Tue, 23 Jan 2018 16:04:18 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/23.html#a160418"
                        },
                        "id": "0000393"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/23.html#a180449",
                        "body": "Another possible River5 problem. For the last two days the NYT Daily podcast hasn't shown up on Podcatch.com. I was willing to write it off as a one-time glitch yesterday, but when it happened again today, I'm starting to get concerned. Here's the feedvalidator link to the feed. ...",
                        "pubDate": "Tue, 23 Jan 2018 18:04:49 GMT",
                        "permaLink": "http://scripting.com/2018/01/23.html#a180449",
                        "outline": {
                            "text": "Another possible <a href=\"https://github.com/scripting/river5\">River5</a> problem. For the last two days the NYT Daily podcast hasn't shown up on <a href=\"http://podcatch.com/\">Podcatch.com</a>. I was willing to write it off as a one-time glitch yesterday, but when it happened again today, I'm starting to get concerned. Here's the <a href=\"http://www.feedvalidator.org/check.cgi?url=http%3A%2F%2Frss.art19.com%2Fthe-daily\">feedvalidator</a> link to the feed. As you can see it's saying it's not valid, but I also fed it through my own <a href=\"https://github.com/scripting/nodeFeedDebug\">feed debugger</a>, which exactly mimicks what River5 does with feeds, and it seems okay with it. I'm starting a <a href=\"https://github.com/scripting/Scripting-News/issues/62\">thread</a> for this.",
                            "created": "Tue, 23 Jan 2018 18:04:49 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/23.html#a180449"
                        },
                        "id": "0000392"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/23.html#a165528",
                        "body": "Five years. Between 1994 and 1999, there was a brief period when the web was truly open. There was no one who could veto you. No one who, if they took offense to what you said or did, could knock you off the net. There were people who tried. That made it dramatic. But there was ...",
                        "pubDate": "Tue, 23 Jan 2018 16:55:28 GMT",
                        "permaLink": "http://scripting.com/2018/01/23.html#a165528",
                        "outline": {
                            "text": "Five years. Between 1994 and 1999, there was a brief period when the web was truly open. There was no one who could veto you. No one who, if they took offense to what you said or did, could knock you off the net. There were people who tried. That made it dramatic. But there was <a href=\"https://duckduckgo.com/?q=site%3Ascripting.com+blue+sky&t=hg&ia=web\">blue sky</a> everywhere. Now the web is divided into silos controlled by big companies. A little bit of light shows through between the cracks. I keep hoping that one crack will open into a new world that's open where we can play where we have users to serve, and competitors to compete with. I go from slightly optimistic to get-a-clue-Dave-it-ain't-happening.",
                            "created": "Tue, 23 Jan 2018 16:55:28 GMT",
                            "type": "outline",
                            "image": "http://scripting.com/images/2018/01/01/bear.png",
                            "permalink": "http://scripting.com/2018/01/23.html#a165528"
                        },
                        "id": "0000391"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/24.html#a155344",
                        "body": "One more thing, I say this all the time, the same is true of software. If you forget you're using the software and your focus is on what's in your mind, the software is good. I saw this video by Ted Nelson where he says software is art. As with a movie, the goal is to get out of ...",
                        "pubDate": "Wed, 24 Jan 2018 15:53:44 GMT",
                        "permaLink": "http://scripting.com/2018/01/24.html#a155344",
                        "outline": {
                            "text": "One more thing, I say this all the time, the same is true of software. If you forget you're using the software and your focus is on what's in your mind, the software is good. I saw this <a href=\"https://spectrum.ieee.org/video/geek-life/profiles/ted-nelson-on-what-modern-programmers-can-learn-from-the-past\">video</a> by Ted Nelson where he says software is art. As with a movie, the goal is to get out of your way and let you forget it's software. Same with painting and music. You go into yourself. You create your own story from mine.",
                            "created": "Wed, 24 Jan 2018 15:53:44 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/24.html#a155344"
                        },
                        "id": "0000390"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/24.html#a154049",
                        "body": "I've now seen most of the Best Picture Oscar-nominated films. I haven't seen Get Out and Phantom Thread yet. The only one I didn't like is The Post. I just saw The Florida Project. Beautiful movie. The kind of movie that I admire, even adore, for having a small plot but still ...",
                        "pubDate": "Wed, 24 Jan 2018 15:40:49 GMT",
                        "permaLink": "http://scripting.com/2018/01/24.html#a154049",
                        "outline": {
                            "text": "I've now seen most of the Best Picture Oscar-nominated films. I haven't seen <a href=\"https://en.wikipedia.org/wiki/Get_Out\">Get Out</a> and <a href=\"https://en.wikipedia.org/wiki/Phantom_Thread\">Phantom Thread</a> yet. The only one I didn't like is <a href=\"https://en.wikipedia.org/wiki/The_Post_(film)\">The Post</a>. I just saw <a href=\"https://en.wikipedia.org/wiki/The_Florida_Project\">The Florida Project</a>. Beautiful movie. The kind of movie that I admire, even adore, for having a small plot but still holding my interest. The characters, the acting, the presentation are all so skillful and compelling that you fall into the movie and don't want to leave. Suspension of disbelief. Story-telling. I saw someone ask why you people like <a href=\"https://en.wikipedia.org/wiki/The_Shape_of_Water_(film)\">The Shape of Water</a> (they didn't, I did). I answer it with a question: Are you pulled into the story. If so, it's good. That's all there is to it. I can certainly see how one might not be pulled into the movie. That was the problem with The Post. It has all the best actors. It's shot perfectly. But none of it meant anything to me. I was waiting to be in the movie, and it never happened.",
                            "created": "Wed, 24 Jan 2018 15:40:49 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/24.html#a154049"
                        },
                        "id": "0000389"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/24.html#a154008",
                        "body": "Blogging is thinking aloud into an outliner that has a Publish button on it.",
                        "pubDate": "Wed, 24 Jan 2018 15:40:08 GMT",
                        "permaLink": "http://scripting.com/2018/01/24.html#a154008",
                        "outline": {
                            "text": "Blogging is thinking aloud into an outliner that has a <i>Publish</i> button on it.",
                            "created": "Wed, 24 Jan 2018 15:40:08 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/24.html#a154008"
                        },
                        "id": "0000388"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/24.html#a153744",
                        "body": "I wrote recently about being invited to a new media thing in Moscow in 2011. I was puzzled as to why I was invited, but I think I just figured out it must have been somehow connected to my one Davos visit. I hung out with a bunch of Russian reporters for one lunch. That might ...",
                        "pubDate": "Wed, 24 Jan 2018 15:37:44 GMT",
                        "permaLink": "http://scripting.com/2018/01/24.html#a153744",
                        "outline": {
                            "text": "I <a href=\"http://scripting.com/2017/11/09.html#a025848\">wrote</a> recently about being invited to a new media thing in Moscow in 2011. I was puzzled as to why I was invited, but I think I just figured out it must have been somehow connected to my one Davos visit. I hung out with a bunch of Russian reporters for one lunch. That might have been the connection.",
                            "created": "Wed, 24 Jan 2018 15:37:44 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/24.html#a153744"
                        },
                        "id": "0000387"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/24.html#a153626",
                        "body": "On the other hand I would love to go to a creative summit of unknown people who just wanted to jam. The people have to be great, but not famous, otherwise all you see is star-preening and star-fucking. As at Davos.",
                        "pubDate": "Wed, 24 Jan 2018 15:36:26 GMT",
                        "permaLink": "http://scripting.com/2018/01/24.html#a153626",
                        "outline": {
                            "text": "On the other hand I would love to go to a creative summit of unknown people who just wanted to jam. The people have to be great, but not famous, otherwise all you see is star-preening and star-fucking. As at Davos.",
                            "created": "Wed, 24 Jan 2018 15:36:26 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/24.html#a153626"
                        },
                        "id": "0000386"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/24.html#a153305",
                        "body": "I went to Davos one year, in 2000. Didn't get invited back, but that was no surprise because the invite itself was a fluke. I wondered if I would go now if I were invited, and unless there were something specific I wanted to do with all those super rich people and their ...",
                        "pubDate": "Wed, 24 Jan 2018 15:33:05 GMT",
                        "permaLink": "http://scripting.com/2018/01/24.html#a153305",
                        "outline": {
                            "text": "I went to Davos one year, in 2000. Didn't get invited back, but that was no surprise because the invite itself was a fluke. I wondered if I would go now if I were invited, and unless there were something specific I wanted to do with all those super rich people and their employees, and that seems pretty unlikely, I don't think I would go. It was worth going once. That's for sure. So I have a picture of all the things they talk about from there. And after a couple of days I unfollow all the people who live-tweet it. Enough with the star-fucking, ok.",
                            "created": "Wed, 24 Jan 2018 15:33:05 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/24.html#a153305"
                        },
                        "id": "0000385"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/24.html#a151042",
                        "body": "Another thing, if enough people use the open web, not very many, we'll be able to boot up more open stuff like blogging, podcasting and RSS. All that stopped because y'all are using silos now, and that cuts off innovation.",
                        "pubDate": "Wed, 24 Jan 2018 15:10:42 GMT",
                        "permaLink": "http://scripting.com/2018/01/24.html#a151042",
                        "outline": {
                            "text": "Another thing, if enough people use the open web, not very many, we'll be able to boot up more open stuff like blogging, podcasting and RSS. All that stopped because y'all are using silos now, and that cuts off innovation.",
                            "created": "Wed, 24 Jan 2018 15:10:42 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/24.html#a151042"
                        },
                        "id": "0000384"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/24.html#a150943",
                        "body": "Once a year I accidentally make a pot of excellent coffee. Today is that day for 2018.",
                        "pubDate": "Wed, 24 Jan 2018 15:09:43 GMT",
                        "permaLink": "http://scripting.com/2018/01/24.html#a150943",
                        "outline": {
                            "text": "Once a year I accidentally make a pot of excellent coffee. Today is that day for 2018.",
                            "created": "Wed, 24 Jan 2018 15:09:43 GMT",
                            "type": "tweet",
                            "tweetid": "956182298411626496",
                            "tweetusername": "davewiner",
                            "permalink": "http://scripting.com/2018/01/24.html#a150943"
                        },
                        "id": "0000383"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/24.html#a160622",
                        "body": "This is a good story to think about re #metoo and what's consensual and not. I have reservations about the piece, but am glad both people are anonymous. I applaud Vox for taking this approach.",
                        "pubDate": "Wed, 24 Jan 2018 16:06:22 GMT",
                        "permaLink": "http://scripting.com/2018/01/24.html#a160622",
                        "outline": {
                            "text": "This is a <a href=\"https://www.vox.com/first-person/2018/1/24/16925444/aziz-ansari-me-too-feminism-consent\">good story</a> to think about re #metoo and what's consensual and not. I have reservations about the piece, but am glad both people are anonymous. I applaud Vox for taking this approach.",
                            "created": "Wed, 24 Jan 2018 16:06:22 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/24.html#a160622"
                        },
                        "id": "0000382"
                    },
                    {
                        "title": "How to give yourself a Trumpectomy",
                        "link": "http://scripting.com/2018/01/25/142931.html",
                        "body": "There are several layers:   \n\nUnfollow Trump.\nBlock Trump. \nUnfollow anyone who quote-tweets a Trump tweet. \nUnfollow anyone who RTs a quote-tweet of Trump.\n\nThere might be more but this is what I thought of so far.\nI've done all of this, and continue to, and my Twitter timeline ...",
                        "pubDate": "Thu, 25 Jan 2018 14:29:31 GMT",
                        "permaLink": "http://scripting.com/2018/01/25/142931.html",
                        "outline": {
                            "text": "How to give yourself a Trumpectomy",
                            "created": "Thu, 25 Jan 2018 14:29:31 GMT",
                            "type": "outline",
                            "description": "I've done all of this, and continue to, and my Twitter timeline is remarkably Trump-free.",
                            "subs": [
                                {
                                    "text": "There are several layers:",
                                    "created": "Thu, 25 Jan 2018 14:29:39 GMT",
                                    "flnumberedsubs": "true",
                                    "subs": [
                                        {
                                            "text": "Unfollow Trump.",
                                            "created": "Thu, 25 Jan 2018 14:29:50 GMT",
                                            "permalink": "http://scripting.com/2018/01/25/142931.html#a142950"
                                        },
                                        {
                                            "text": "Block Trump.",
                                            "created": "Thu, 25 Jan 2018 14:30:24 GMT",
                                            "permalink": "http://scripting.com/2018/01/25/142931.html#a143024"
                                        },
                                        {
                                            "text": "Unfollow anyone who quote-tweets a Trump tweet.",
                                            "created": "Thu, 25 Jan 2018 14:29:57 GMT",
                                            "permalink": "http://scripting.com/2018/01/25/142931.html#a142957"
                                        },
                                        {
                                            "text": "Unfollow anyone who RTs a quote-tweet of Trump.",
                                            "created": "Thu, 25 Jan 2018 14:30:11 GMT",
                                            "permalink": "http://scripting.com/2018/01/25/142931.html#a143011"
                                        }
                                    ],
                                    "permalink": "http://scripting.com/2018/01/25/142931.html#a142939"
                                },
                                {
                                    "text": "There might be more but this is what I thought of so far.",
                                    "created": "Thu, 25 Jan 2018 14:30:54 GMT",
                                    "permalink": "http://scripting.com/2018/01/25/142931.html#a143054"
                                },
                                {
                                    "text": "I've done all of this, and continue to, and my Twitter timeline is remarkably Trump-free. But it's a chronic condition, you have to keep monitoring it for new outbreaks.",
                                    "created": "Thu, 25 Jan 2018 14:31:43 GMT",
                                    "permalink": "http://scripting.com/2018/01/25/142931.html#a143143"
                                },
                                {
                                    "text": "You might think that \"exposing\" his corruption or idiocy is some kind of public service, but it's not. John Dvorak, a classic Internet troll, <a href=\"https://www.youtube.com/watch?v=NMQv0j29WHA\">explains</a> how it works. That's Trump. He wants you to think he wants to be shown the light. That's why the Repubs say things like they want to protect the Dreamers, just so you'll argue with them and waste energy and time and attention on it. <i>Meanwhile they're giving themselves your money.</i> So if you help them distribute their BS, you're part of the problem. Don't be part of the problem. Give yourself a Trumpectomy today.",
                                    "created": "Thu, 25 Jan 2018 14:32:13 GMT",
                                    "urlvideo": "https://www.youtube.com/embed/NMQv0j29WHA",
                                    "permalink": "http://scripting.com/2018/01/25/142931.html#a143213"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/25/142931.html"
                        },
                        "id": "0000381"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/25.html#a163104",
                        "body": "The way I help fight fake news of all kinds is when I see it, I unfollow the person who brought it to me.",
                        "pubDate": "Thu, 25 Jan 2018 16:31:04 GMT",
                        "permaLink": "http://scripting.com/2018/01/25.html#a163104",
                        "outline": {
                            "text": "The way I help fight fake news of all kinds is when I see it, I unfollow the person who brought it to me.",
                            "created": "Thu, 25 Jan 2018 16:31:04 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/25.html#a163104"
                        },
                        "id": "0000380"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/25.html#a145324",
                        "body": "Vivian Schiller: \"No self-respecting secret society would call itself a secret society.\"",
                        "pubDate": "Thu, 25 Jan 2018 14:53:24 GMT",
                        "permaLink": "http://scripting.com/2018/01/25.html#a145324",
                        "outline": {
                            "text": "<a href=\"https://twitter.com/vivian/status/956537767483330560\">Vivian Schiller</a>: \"No self-respecting secret society would call itself a secret society.\"",
                            "created": "Thu, 25 Jan 2018 14:53:24 GMT",
                            "type": "outline",
                            "urltweet": "https://twitter.com/vivian/status/956537767483330560",
                            "permalink": "http://scripting.com/2018/01/25.html#a145324"
                        },
                        "id": "0000379"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/25.html#a174951",
                        "body": "A UX tip for Twitter. Add a spam button like Gmail's.",
                        "pubDate": "Thu, 25 Jan 2018 17:49:51 GMT",
                        "permaLink": "http://scripting.com/2018/01/25.html#a174951",
                        "outline": {
                            "text": "A <a href=\"https://twitter.com/davewiner/status/956584749916672002\">UX tip</a> for Twitter. Add a spam button like Gmail's.",
                            "created": "Thu, 25 Jan 2018 17:49:51 GMT",
                            "type": "outline",
                            "urltweet": "https://twitter.com/davewiner/status/956584749916672002",
                            "permalink": "http://scripting.com/2018/01/25.html#a174951"
                        },
                        "id": "0000378"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/25.html#a213549",
                        "body": "A remarkable historic episode of the Daily podcast on the end of the trial of Dr Lawrence Nassar. Each of the survivors spoke directly to Nassar, publicly, before he was sentenced.",
                        "pubDate": "Thu, 25 Jan 2018 21:35:49 GMT",
                        "permaLink": "http://scripting.com/2018/01/25.html#a213549",
                        "outline": {
                            "text": "A remarkable historic <a href=\"http://podcatch.com/archive/2018/01/25/61195.html\">episode</a> of the <i>Daily</i> podcast on the end of the trial of Dr Lawrence Nassar. Each of the survivors spoke directly to Nassar, publicly, before he was sentenced.",
                            "created": "Thu, 25 Jan 2018 21:35:49 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/25.html#a213549"
                        },
                        "id": "0000377"
                    },
                    {
                        "title": "Does Facebook really do open source?",
                        "link": "http://scripting.com/2018/01/26/181453.html",
                        "body": "I spied an exchange on Twitter between Bruce Sterling and an open source advocate. Sterling's account is private, so I had to check for permission. Not sure how much you'll be able to see of the exchange.  \nThe other guy said philathropists like George Soroos should get behind ...",
                        "pubDate": "Fri, 26 Jan 2018 18:14:53 GMT",
                        "permaLink": "http://scripting.com/2018/01/26/181453.html",
                        "outline": {
                            "text": "Does Facebook really do open source?",
                            "created": "Fri, 26 Jan 2018 18:14:53 GMT",
                            "type": "outline",
                            "description": "Yes, technically Facebook is doing open source, but not in the way most people think. It doesn't make Facebook open. Quite the opposite.",
                            "subs": [
                                {
                                    "text": "I spied an <a href=\"https://twitter.com/jackwilliambell/status/956934330290286593\">exchange</a> on Twitter between <a href=\"https://en.wikipedia.org/wiki/Bruce_Sterling\">Bruce Sterling</a> and an open source advocate. Sterling's account is private, so I had to check for permission. Not sure how much you'll be able to see of the exchange.",
                                    "created": "Fri, 26 Jan 2018 18:15:10 GMT",
                                    "image": "http://scripting.com/2016/10/19/fatcat.png",
                                    "permalink": "http://scripting.com/2018/01/26/181453.html#a181510"
                                },
                                {
                                    "text": "The <a href=\"https://twitter.com/jackwilliambell\">other guy</a> said philathropists like <a href=\"https://www.theguardian.com/business/2018/jan/25/george-soros-facebook-and-google-are-a-menace-to-society\">George Soroos</a> should get behind open source as a way of balancing the power of BigCo's like Google and Facebook.",
                                    "created": "Fri, 26 Jan 2018 18:17:45 GMT",
                                    "permalink": "http://scripting.com/2018/01/26/181453.html#a181745"
                                },
                                {
                                    "text": "Sterling pointed out that <a href=\"https://code.facebook.com/projects/\">Facebook does</a> a lot of open source. It's true they do. But it's different. Then I realized it's the same confusion news people created about blogging when they <a href=\"http://www.nytimes.com/interactive/blogs/directory.html\">claimed</a> to be bloggers a number of years ago. Yes, technically Facebook is doing open source, but not the way most people think. It doesn't make Facebook open. Quite the opposite, it's a huge death trap for the web. <a href=\"https://nypost.com/1999/01/23/microsoft-exec-on-the-hot-seat/\">Suffocates</a> it. A roach motel for the open web.",
                                    "created": "Fri, 26 Jan 2018 18:18:19 GMT",
                                    "permalink": "http://scripting.com/2018/01/26/181453.html#a181819"
                                },
                                {
                                    "text": "So Facebook is not open as in the <a href=\"https://duckduckgo.com/?q=site%3Ascripting.com+open+web&t=hg&ia=web\">open web</a>. And reporters aren't bloggers, they're still professionals with all the limits that come from working for other people. You're getting a corporate product in both cases, yet the implication is that you're getting something open and uncontrolled. But open source projects sponsored by big tech companies are as controlled as their products and user experience.",
                                    "created": "Fri, 26 Jan 2018 18:19:09 GMT",
                                    "permalink": "http://scripting.com/2018/01/26/181453.html#a181909"
                                },
                                {
                                    "text": "There ought to be a different name for what they do. Or we have to help people understand that there's a difference between platforms without platform vendors and what Facebook and Google do.",
                                    "created": "Fri, 26 Jan 2018 18:19:48 GMT",
                                    "permalink": "http://scripting.com/2018/01/26/181453.html#a181948"
                                }
                            ],
                            "permalink": "http://scripting.com/2018/01/26/181453.html"
                        },
                        "id": "0000376"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/26.html#a180708",
                        "body": "When I was in college a second cousin who was in his 40s would tease me about my supposed sexual conquests. I wasn't nearly as prolific as he imagined. And he was being funny, I knew that, but it was also mildly irritating. Male bonding. Now I find myself tempted to do it to my ...",
                        "pubDate": "Fri, 26 Jan 2018 18:07:08 GMT",
                        "permaLink": "http://scripting.com/2018/01/26.html#a180708",
                        "outline": {
                            "text": "When I was in college a second cousin who was in his 40s would tease me about my supposed sexual conquests. I wasn't nearly as prolific as he imagined. And he was being funny, I knew that, but it was also mildly irritating. Male bonding. Now I find myself tempted to do it to my younger compadres. But so far, mostly, I've resisted. <span class=\"spOldSchoolEmoji\">💥</span>",
                            "created": "Fri, 26 Jan 2018 18:07:08 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/26.html#a180708"
                        },
                        "id": "0000375"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/26.html#a185318",
                        "body": "Schumer may have made a tactical mistake. Now the Repubs can say that Dems blocked citizenship for Dreamers. Better to make a counter-proposal with no funding for the wall and without the other stuff.",
                        "pubDate": "Fri, 26 Jan 2018 18:53:18 GMT",
                        "permaLink": "http://scripting.com/2018/01/26.html#a185318",
                        "outline": {
                            "text": "<a href=\"https://www.politico.com/story/2018/01/26/trump-immigration-proposal-chuck-schumer-371139\">Schumer</a> may have made a tactical mistake. Now the Repubs can say that Dems blocked citizenship for Dreamers. Better to make a counter-proposal with no funding for the wall and without the other stuff.",
                            "created": "Fri, 26 Jan 2018 18:53:18 GMT",
                            "type": "outline",
                            "permalink": "http://scripting.com/2018/01/26.html#a185318"
                        },
                        "id": "0000374"
                    },
                    {
                        "title": "",
                        "link": "http://scripting.com/2018/01/26.html#a202707",
                        "body": "Thought of a BitCoin-like game, maybe like Fallout Shelter. You do things to mine new coins, and keep the ledgers balanced. The cute thing is that it's real. The profits would go to the developer of course.",
                        "pubDate": "Fri, 26 Jan 2018 20:27:07 GMT",
                        "permaLink": "http://scripting.com/2018/01/26.html#a202707",
                        "outline": {
                            "text": "Thought of a BitCoin-like game, maybe like <a href=\"https://www.gamespot.com/articles/fallout-shelter-passes-100-million-downloads-and-h/1100-6453354/\">Fallout Shelter</a>. You do things to mine new coins, and keep the ledgers balanced. The cute thing is that it's real. The profits would go to the developer of course.",
                            "created": "Fri, 26 Jan 2018 20:27:07 GMT",
                            "type": "outline",
                            "image": "http://scripting.com/images/2018/01/26/falloutShelterGuy.png",
                            "permalink": "http://scripting.com/2018/01/26.html#a202707"
                        },
                        "id": "0000373"
                    }
                ]
            },
            {
                "feedTitle": "Techdirt.",
                "feedUrl": "http://feeds.feedburner.com/techdirt/feed",
                "websiteUrl": "https://www.techdirt.com/",
                "feedDescription": "Easily digestible tech news...",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:59 GMT",
                "item": [
                    {
                        "title": "US Army Files Dumb Trademark Opposition Against The NHL's Las Vegas Golden Knights",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/3plUf7fQLIs/us-army-files-dumb-trademark-opposition-against-nhls-las-vegas-golden-knights.shtml",
                        "body": "This post will come as no surprise to those of us super-interesting people that for some reason have made trademark law and news a key fulcrom point in our lives, but the United States Army has filed an opposition to the trademark application for the Las Vegas Golden Knights. ...",
                        "pubDate": "Wed, 24 Jan 2018 03:11:07 GMT",
                        "permaLink": "",
                        "id": "0000372"
                    },
                    {
                        "title": "Psychiatrist Bitterly Drops Defamation Lawsuit Against Redditors",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/V-sUWjp-Jmk/psychiatrist-bitterly-drops-defamation-lawsuit-against-redditors.shtml",
                        "body": "Dr. Douglas Berger, an American psychiatrist offering services to ex-pats in Japan, recently sued a bunch of Redditors for telling other Redditors to steer clear of his services. Berger's lawsuit was exhaustive, covering several months of disparaging comments delivered by ...",
                        "pubDate": "Wed, 24 Jan 2018 11:30:59 GMT",
                        "permaLink": "",
                        "id": "0000371"
                    },
                    {
                        "title": "New Bill Would Prevent Comcast-Loyal States From Blocking Broadband Competition",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/2TCMIqW6E-A/new-bill-would-prevent-comcast-loyal-states-blocking-broadband-competition.shtml",
                        "body": "We've long noted how state legislatures are so corrupt, they often quite literally let entrenched telecom operators write horrible, protectionist laws that hamstring competition. That's why there's now 21 states where companies like AT&#038;T, Verizon and Comcast have ...",
                        "pubDate": "Wed, 24 Jan 2018 14:33:59 GMT",
                        "permaLink": "",
                        "id": "0000370"
                    },
                    {
                        "title": "Italian Government Criminalizes 'Fake News,' Provides Direct Reporting Line To State Police Force",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/Oa9jWA_KhoI/italian-government-criminalizes-fake-news-provides-direct-reporting-line-to-state-police-force.shtml",
                        "body": "No one knows how to handle \"fake news.\" Rather than step back and see what light-touch approaches might work, governments all over the world are rushing forward with bad ideas that harm speech and threaten journalism. No one seems to be immune to the \"do something\" infection and ...",
                        "pubDate": "Wed, 24 Jan 2018 17:40:41 GMT",
                        "permaLink": "",
                        "id": "0000369"
                    },
                    {
                        "title": "Daily Deal: Final Draft 10",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/_K_W7Z0AWC0/daily-deal-final-draft-10.shtml",
                        "body": "Final Draft 10 is the standard software for professional screenwriters and studios the world over. It automatically paginates your script to entertainment industry standards and gives you over 100 templates and formatting tools to turn your ideas into real scripts. You can ...",
                        "pubDate": "Wed, 24 Jan 2018 18:34:09 GMT",
                        "permaLink": "",
                        "id": "0000368"
                    },
                    {
                        "title": "AT&T's Bogus 'Internet Bill Of Rights' Aims To Undermine Net Neutrality, Foist Regulation Upon Silicon Valley Competitors",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/zph0JUzi9m8/ats-bogus-internet-bill-rights-aims-to-undermine-net-neutrality-foist-regulation-upon-silicon-valley-competitors.shtml",
                        "body": "As we've been warning for a while, the next phase in the war on net neutrality for giant ISPs is pushing a new \"net neutrality law\" in name only. ISPs are nervous that the FCC's net neutrality repeal won't survive a court challenge due to the numerous instances of fraud and ...",
                        "pubDate": "Wed, 24 Jan 2018 19:14:32 GMT",
                        "permaLink": "",
                        "id": "0000367"
                    },
                    {
                        "title": "Censorship By Weaponizing Free Speech: Rethinking How The Marketplace Of Ideas Works",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/3_ZpP5Gia4Y/censorship-weaponizing-free-speech-rethinking-how-marketplace-ideas-works.shtml",
                        "body": "It should be no surprise that I'm an unabashed supporter of free speech. Usually essays that start that way are then followed with a \"but...\" and that \"but...\" undermines everything in that opening sentence. This is not such an essay. However, I am going to talk about some ...",
                        "pubDate": "Wed, 24 Jan 2018 19:59:25 GMT",
                        "permaLink": "",
                        "id": "0000366"
                    },
                    {
                        "title": "Wherein We Ask The California Supreme Court To Lessen The Damage The Court Of Appeal Caused To Speech",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/coiC6Ah9G08/wherein-we-ask-california-supreme-court-to-lessen-damage-court-appeal-caused-to-speech.shtml",
                        "body": "A few weeks ago we posted an update on Montagna v. Nunis. This was a case where a plaintiff subpoenaed Yelp for the identity of a user. The trial court originally denied Yelp's attempt to quash the subpoena – and sanctioned it for trying – on the grounds that platforms had no ...",
                        "pubDate": "Wed, 24 Jan 2018 21:34:49 GMT",
                        "permaLink": "",
                        "id": "0000365"
                    },
                    {
                        "title": "Denuvo Sold To Irdeto, Which Boasts Of Acquiring 'The World Leader In Gaming Security'",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/scX2PN1yfCE/denuvo-sold-to-irdeto-which-boasts-acquiring-world-leader-gaming-security.shtml",
                        "body": "Any reading of our thorough coverage of Denuvo DRM could be best summarized as: a spasm of success in 2015 followed by one of the great precipitous falls into failure in the subsequent two years. While some of us are of the opinion that all DRM such as Denuvo are destined for ...",
                        "pubDate": "Wed, 24 Jan 2018 23:38:49 GMT",
                        "permaLink": "",
                        "id": "0000364"
                    },
                    {
                        "title": "Danish Police Charge Over 1,000 People With Sharing Underage Couple's Sexting Video And Images",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/KYUG8_06-CY/danish-police-charge-over-1000-people-with-sharing-underage-couples-sexting-video-images.shtml",
                        "body": "Techdirt posts about sexting have a depressingly similar story line: young people send explicit photos of themselves to their partners, and one or both of them end up charged with distributing or possessing child pornography. Even more ridiculously, the authorities typically ...",
                        "pubDate": "Thu, 25 Jan 2018 03:54:28 GMT",
                        "permaLink": "",
                        "id": "0000363"
                    },
                    {
                        "title": "Disrupting The Fourth Amendment: Half Of Law Enforcement E-Warrants Approved In 10 Minutes Or Less",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/-OHH8mHZ3PY/disrupting-fourth-amendment-half-law-enforcement-e-warrants-approved-10-minutes-less.shtml",
                        "body": "Law enforcement officers will often testify that seeking warrants is a time-consuming process that subjects officers' sworn statements to strict judicial scrutiny. The testimony implies the process is a hallowed tradition that upholds the sanctity of the Fourth Amendment, hence ...",
                        "pubDate": "Thu, 25 Jan 2018 11:21:00 GMT",
                        "permaLink": "",
                        "id": "0000362"
                    },
                    {
                        "title": "The GAO Says It Will Investigate Bogus Net Neutrality Comments, Eventually",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/me12rx-t9Iw/gao-says-it-will-investigate-bogus-net-neutrality-comments-eventually.shtml",
                        "body": "The General Accounting Office (GAO) says the agency will launch an investigation into the fraud that occurred during the FCC's rushed repeal of net neutrality rules. Consumers only had one real chance to weigh in during the public comment period of the agency's ...",
                        "pubDate": "Thu, 25 Jan 2018 14:19:00 GMT",
                        "permaLink": "",
                        "id": "0000361"
                    },
                    {
                        "title": "Rupert Murdoch Admits, Once Again, He Can't Make Money Online -- Begs Facebook To Just Give Him Money",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/yl4ZOTdLjrY/rupert-murdoch-admits-once-again-he-cant-make-money-online-begs-facebook-to-just-give-him-money.shtml",
                        "body": "There's no denying that Rupert Murdoch built up quite a media empire over the decades -- but that was almost all entirely focused on newspaper and pay TV. While he's spent the past few decades trying to do stuff on the internet, he has an impressively long list of failures over ...",
                        "pubDate": "Thu, 25 Jan 2018 17:32:19 GMT",
                        "permaLink": "",
                        "id": "0000360"
                    },
                    {
                        "title": "Daily Deal: LithiumCard Pro Retro Series Lightning Battery Chargers",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/a9MuUBO4jq8/daily-deal-lithiumcard-pro-retro-series-lightning-battery-chargers.shtml",
                        "body": "Hop on that '80s nostalgia train with these portable LithiumCard Pro Retro Series Lightning Battery Chargers! The $40 battery uses 3.0 amp HyperCharging Generation 2 technology to deliver an ultra fast charge via lightening cable for your Apple devices and you can charge a ...",
                        "pubDate": "Thu, 25 Jan 2018 18:31:19 GMT",
                        "permaLink": "",
                        "id": "0000359"
                    },
                    {
                        "title": "Spanish Government Uses Hate Speech Law To Arrest Critic Of The Spanish Government",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/JfizvPket_I/spanish-government-uses-hate-speech-law-to-arrest-critic-spanish-government.shtml",
                        "body": "Spain's government has gotten into the business of regulating speech with predictably awful results. An early adopter of Blues Lives Matter-esque policies, Spain went full police state, passing a law making it a crime to show \"disrespect\" to law enforcement officers. The ...",
                        "pubDate": "Thu, 25 Jan 2018 18:36:19 GMT",
                        "permaLink": "",
                        "id": "0000358"
                    },
                    {
                        "title": "TPP Is Back, Minus Copyright Provisions And Pharma Patent Extensions, In A Clear Snub To Trump And The US",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/u2RCiQtpMsM/tpp-is-back-minus-copyright-provisions-pharma-patent-extensions-clear-snub-to-trump-us.shtml",
                        "body": "As Techdirt noted back in November, the Trans Pacific Partnership (TPP) agreement was not killed by Donald Trump's decision to pull the US out of the deal. Instead, something rather interesting happened: one of the TPP's worst chapters, dealing with copyright, was \"suspended\" at ...",
                        "pubDate": "Thu, 25 Jan 2018 19:54:06 GMT",
                        "permaLink": "",
                        "id": "0000357"
                    },
                    {
                        "title": "Harris Stingray Nondisclosure Agreement Forbids Cops From Telling Legislators About Surveillance Tech",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/iP13RM1P0iU/harris-stingray-nondisclosure-agreement-forbids-cops-telling-legislators-about-surveillance-tech.shtml",
                        "body": "The FBI set the first (and second!) rules of Stingray Club: DO NOT TALK ABOUT STINGRAY CLUB. Law enforcement agencies seeking to acquire cell tower spoofing tech were forced to sign a nondisclosure agreement forbidding them from disclosing details on the devices to defendants, ...",
                        "pubDate": "Thu, 25 Jan 2018 21:40:52 GMT",
                        "permaLink": "",
                        "id": "0000356"
                    },
                    {
                        "title": "Vice Media Goes After Vice Industry Token, A Porn Crypto-Currency Company, For Trademark",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/bjmSzpZkjY0/vice-media-goes-after-vice-industry-token-porn-crypto-currency-company-trademark.shtml",
                        "body": "The last time we checked in with Vice Media it was firing off a cease and desist letter to a tiny little punk band called ViceVersa, demanding that it change its name because Vice Media has a trademark for the word \"vice\" for several markets. In case you thought that occurrence ...",
                        "pubDate": "Thu, 25 Jan 2018 23:36:00 GMT",
                        "permaLink": "",
                        "id": "0000355"
                    },
                    {
                        "title": "Genome Of A Man Born In 1784 Recreated From The DNA Of His Descendants",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/uHjhTWDZJWA/genome-man-born-1784-recreated-dna-his-descendants.shtml",
                        "body": "The privacy implications of collecting DNA are wide-ranging, not least because they don't relate solely to the person from whom the sample is taken. Our genome is a direct product of our parents' genetic material, so the DNA strings of siblings from the same mother and father ...",
                        "pubDate": "Fri, 26 Jan 2018 03:22:32 GMT",
                        "permaLink": "",
                        "id": "0000354"
                    },
                    {
                        "title": "Sarajevo's City Government Says No One Can Use The Name 'Sarajevo' Without Its Permission",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/dy_ODDuSYfg/sarajevos-city-government-says-no-one-can-use-name-sarajevo-without-permission.shtml",
                        "body": "The city of Sarajevo passed a law in 2000 forbidding anyone but the city of Sarajevo from using the name Sarajevo. Not much has been said about it because the Sarajevo city council hasn't done much about it. But recently owners of Facebook pages containing the word \"Sarajevo\" ...",
                        "pubDate": "Fri, 26 Jan 2018 11:29:00 GMT",
                        "permaLink": "",
                        "id": "0000353"
                    },
                    {
                        "title": "FCC Hopes Its Phony Dedication To Rural Broadband Will Make You Forget It Killed Net Neutrality",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/RMpl7I3C6PM/fcc-hopes-phony-dedication-to-rural-broadband-will-make-you-forget-it-killed-net-neutrality.shtml",
                        "body": "The FCC and its large ISP allies are trying to change the subject in the wake of their hugely unpopular attack on net neutrality. With net neutrality having such broad, bipartisan support, the FCC is trying to shift the conversation away from net neutrality (which remember, is ...",
                        "pubDate": "Fri, 26 Jan 2018 14:33:00 GMT",
                        "permaLink": "",
                        "id": "0000352"
                    },
                    {
                        "title": "FBI Director Chris Wray Says Secure Encryption Backdoors Are Possible; Sen. Ron Wyden Asks Him To Produce Receipts",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/PlBKayaKjDU/fbi-director-chris-wray-says-secure-encryption-backdoors-are-possible-sen-ron-wyden-asks-him-to-produce-receipts.shtml",
                        "body": "I cannot wait to see FBI Director Christopher Wray try to escape the petard-hoisting Sen. Ron Wyden has planned for him. Wray has spent most of his time as director complaining about device encryption. He continually points at the climbing number of locked phones the FBI can't ...",
                        "pubDate": "Fri, 26 Jan 2018 17:32:26 GMT",
                        "permaLink": "",
                        "id": "0000351"
                    },
                    {
                        "title": "Daily Deal: Paww WaveSound 3 Noise-Cancelling Bluetooth Headphones",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/5QvShPcLFjY/daily-deal-paww-wavesound-3-noise-cancelling-bluetooth-headphones.shtml",
                        "body": "The WaveSound 3 headphones strike an elegant chord of premium sound quality, active noise cancellation, and comfort. Combining a state-of-the-art CSR chipset with multiple microphones, the WaveSound 3's block out as much as 20dB of unwanted ambient noise, independent of ANC ...",
                        "pubDate": "Fri, 26 Jan 2018 18:39:26 GMT",
                        "permaLink": "",
                        "id": "0000350"
                    },
                    {
                        "title": "Senate IT Tells Staffers They're On Their Own When It Comes To Personal Devices And State-Sponsored Hackers",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/EruZvfD3ie8/senate-it-tells-staffers-theyre-their-own-when-it-comes-to-personal-devices-state-sponsored-hackers.shtml",
                        "body": "Notification of state-sponsored hacking attempts has revealed another weak spot in the US government's defenses. The security of the government's systems is an ongoing concern, but the Senate has revealed it's not doing much to ensure sensitive documents and communications don't ...",
                        "pubDate": "Fri, 26 Jan 2018 18:44:26 GMT",
                        "permaLink": "",
                        "id": "0000349"
                    },
                    {
                        "title": "Harvard Study Shows Community-Owned ISPs Offer Lower, More Transparent Prices",
                        "link": "http://feedproxy.google.com/~r/techdirt/feed/~3/aUn7JHF6H7o/harvard-study-shows-community-owned-isps-offer-lower-more-transparent-prices.shtml",
                        "body": "We've routinely noted how countless communities have been forced to explore building their own broadband networks thanks to limited competition in the market. As most of you have experienced first hand, this lack of competition routinely results in higher prices, slower speeds, ...",
                        "pubDate": "Fri, 26 Jan 2018 20:00:22 GMT",
                        "permaLink": "",
                        "id": "0000348"
                    }
                ]
            },
            {
                "feedTitle": "TechCrunch",
                "feedUrl": "http://techcrunch.com/feed/",
                "websiteUrl": "https://techcrunch.com/",
                "feedDescription": "Startup and Technology News",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:59 GMT",
                "item": [
                    {
                        "title": "Crunch Report | CNN shuts down Casey Neistat’s Beme",
                        "link": "https://techcrunch.com/2018/01/25/crunch-report-cnn-shuts-down-casey-neistats-beme/?ncid=rss",
                        "body": "Robinhood is going to let you buy and sell crypto soon, CNN shuts down Casey Neistat&#8217;s Beme and Sotheby&#8217;s acquires Thread Genius. All this on Crunch Report. Read More",
                        "pubDate": "Fri, 26 Jan 2018 04:00:58 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/25/crunch-report-cnn-shuts-down-casey-neistats-beme/#respond",
                        "enclosure": [
                            {
                                "url": "http://img.vidible.tv/prod/2018-01/24/5a691a51cc912f4adc846c3b/5a691a92695bf0579c99f4c5_o_U_v1.png?w=1279&h=727",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000347"
                    },
                    {
                        "title": "Heetch raises another $20 million to compete head-to-head with Uber in Europe",
                        "link": "https://techcrunch.com/2018/01/26/heetch-raises-another-20-million-to-compete-head-to-head-with-uber-in-europe/?ncid=rss",
                        "body": "&nbsp;French startup Heetch has an ambitious goal. The company wants to become the second ride-sharing service in France and in the other European countries where it operates. The startup just raised $20 million from Félix Capital, Via ID, Alven Capital, Idinvest Partners and ...",
                        "pubDate": "Fri, 26 Jan 2018 08:01:31 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/heetch-raises-another-20-million-to-compete-head-to-head-with-uber-in-europe/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/heetch-order.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000346"
                    },
                    {
                        "title": "Twitter accused of dodging Brexit botnet questions again",
                        "link": "https://techcrunch.com/2018/01/26/twitter-accused-of-dodging-brexit-botnet-questions-again/?ncid=rss",
                        "body": "&nbsp;Once again Twitter stands accused of dodging questions from a parliamentary committee that&#8217;s investigating Russian bot activity during the UK&#8217;s 2016 Brexit referendum.   Read More",
                        "pubDate": "Fri, 26 Jan 2018 12:42:02 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/twitter-accused-of-dodging-brexit-botnet-questions-again/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2017/12/gettyimages-876768474.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000345"
                    },
                    {
                        "title": "Equity podcast: Netflix soars, Twitter loses a big bird, Lyft isn’t woke and more ICOs",
                        "link": "https://techcrunch.com/2018/01/26/equity-podcast-netflix-soars-twitter-loses-a-big-bird-lyft-isnt-woke-and-more-icos/?ncid=rss",
                        "body": "&nbsp;Hello and welcome back to Equity, TechCrunch&#8217;s venture capital-focused podcast where we unpack the numbers behind the headlines. This week we once again had the full crew on set: Matthew Lynley, Katie Roof, and myself. And even better, we were joined by Sarah Tavel ...",
                        "pubDate": "Fri, 26 Jan 2018 14:10:22 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/equity-podcast-netflix-soars-twitter-loses-a-big-bird-lyft-isnt-woke-and-more-icos/#respond",
                        "id": "0000344"
                    },
                    {
                        "title": "Furniture maker Floyd raises $5.6m to expand product line, move into new Detroit HQ",
                        "link": "https://techcrunch.com/2018/01/26/furniture-maker-floyd-raises-5-6m-to-expand-product-line-move-into-new-detroit-hq/?ncid=rss",
                        "body": "&nbsp;Detroit-based Floyd has announced a $5.6 million Series A funding round that will let the company expand its operations and release new products. The company has been in Detroit for the last five years and they&#8217;re dedicated to creating better furniture and a better ...",
                        "pubDate": "Fri, 26 Jan 2018 14:14:45 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/furniture-maker-floyd-raises-5-6m-to-expand-product-line-move-into-new-detroit-hq/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/floyd.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000343"
                    },
                    {
                        "title": "Poptheatr is a bucket you put on your head so you can watch movies",
                        "link": "https://techcrunch.com/2018/01/26/poptheatr-is-a-bucket-you-put-on-your-head-so-you-can-watch-movies/?ncid=rss",
                        "body": "&nbsp;Are you afraid of human contact? Do you like movies? Do you like buckets? Sister, have I got a product for you.\nPoptheatr is a bucket that you put over your head. You put your telecommunications device on top of the bucket and then look up at movies, television, and ...",
                        "pubDate": "Fri, 26 Jan 2018 14:38:59 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/poptheatr-is-a-bucket-you-put-on-your-head-so-you-can-watch-movies/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/screen-shot-2018-01-26-at-9-29-19-am.png",
                                "type": "image/png",
                                "length": null
                            }
                        ],
                        "id": "0000342"
                    },
                    {
                        "title": "Google experiments in local news with an app called Bulletin",
                        "link": "https://techcrunch.com/2018/01/26/google-experiments-in-local-news-with-an-app-called-bulletin/?ncid=rss",
                        "body": "&nbsp;Google is testing a tool called Bulletin that would allow anyone to publish local news stories and events, according to a report from Slate, which Google later confirmed. The company described Bulletin as a way for others to communicate information of local interest, like ...",
                        "pubDate": "Fri, 26 Jan 2018 15:04:42 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/google-experiments-in-local-news-with-an-app-called-bulletin/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2016/08/sunset-view-of-the-oakland-bay-bridge-in-hd-by-spotmatik.png",
                                "type": "image/png",
                                "length": null
                            }
                        ],
                        "id": "0000341"
                    },
                    {
                        "title": "Ford files a patent for an autonomous police car",
                        "link": "https://techcrunch.com/2018/01/26/ford-files-a-patent-for-an-autonomous-police-car/?ncid=rss",
                        "body": "&nbsp;If the fourth season of Black Mirror didn&#8217;t thoroughly freak you out, buckle up! Ford has filed for a patent on an autonomous police car. While patent filings don&#8217;t always come to fruition, the mere fact that this idea is in development is mildly unnerving. The ...",
                        "pubDate": "Fri, 26 Jan 2018 15:30:01 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/ford-files-a-patent-for-an-autonomous-police-car/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/gettyimages-141303718.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000340"
                    },
                    {
                        "title": "Intel does its best to tamp down impact of Spectre and Meltdown in earnings call",
                        "link": "https://techcrunch.com/2018/01/26/intel-does-its-best-to-tamp-down-impact-of-spectre-and-meltdown-in-earnings-call/?ncid=rss",
                        "body": "&nbsp;Intel CEO Brian Krzanich was delighted to report that Intel had a record year in the company&#8217;s quarterly earnings call with analysts yesterday. Of course, he also had to acknowledge the Spectre and Meltdown chip vulnerabilities revealed earlier this month in perhaps ...",
                        "pubDate": "Fri, 26 Jan 2018 15:31:26 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/intel-does-its-best-to-tamp-down-impact-of-spectre-and-meltdown-in-earnings-call/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/gettyimages-906822194.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000339"
                    },
                    {
                        "title": "Microsoft’s Mixer follows Twitch with addition of direct tipping and game sales",
                        "link": "https://techcrunch.com/2018/01/26/microsofts-mixer-follows-twitch-with-addition-of-direct-tipping-and-game-sales/?ncid=rss",
                        "body": "&nbsp;Mixer, Microsoft&#8217;s answer to Twitch, will soon allow its streamers to start selling games and other downloadable content via its service, and will introduce tipping, the company announced this week in its year-end wrap-up. The game sales will be made possible through ...",
                        "pubDate": "Fri, 26 Jan 2018 15:57:05 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/microsofts-mixer-follows-twitch-with-addition-of-direct-tipping-and-game-sales/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/mixer.png",
                                "type": "image/png",
                                "length": null
                            }
                        ],
                        "id": "0000338"
                    },
                    {
                        "title": "LittleBits’ Droid Inventor kit is the first STEM toy that works",
                        "link": "https://techcrunch.com/2018/01/26/littlebits-droid-inventor-kit-is-the-first-stem-toy-that-works/?ncid=rss",
                        "body": "&nbsp;I&#8217;ve been watching STEAM and/or STEAM toys with great interest and great skepticism. While many parents and teachers report great success with programming toys like Cleverbot and whatever this thing is, I&#8217;ve found that kids (and parents) I know find many STEM ...",
                        "pubDate": "Fri, 26 Jan 2018 16:53:34 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/littlebits-droid-inventor-kit-is-the-first-stem-toy-that-works/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/img_0332.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000337"
                    },
                    {
                        "title": "Report: More startup lawyers are accepting cryptocurrencies as payment",
                        "link": "https://techcrunch.com/2018/01/26/report-more-startup-lawyers-are-accepting-cryptocurrencies-as-payment/?ncid=rss",
                        "body": "&nbsp;A growing number of law firms working with startups are beginning to accept their payment in cryptocurrencies. It&#8217;s an interesting shift, and one that&#8217;s very reminiscent of service providers who were paid in equity during the go-go dot.com days of the late ...",
                        "pubDate": "Fri, 26 Jan 2018 17:27:36 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/report-more-startup-lawyers-are-accepting-cryptocurrencies-as-payment/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2017/06/cryptocurrency2.png",
                                "type": "image/png",
                                "length": null
                            }
                        ],
                        "id": "0000336"
                    },
                    {
                        "title": "Hoodline is trying to fix local news deserts with a new automated news wire",
                        "link": "https://techcrunch.com/2018/01/26/hoodline-is-trying-to-fix-local-news-deserts-with-a-new-automated-news-wire/?ncid=rss",
                        "body": "&nbsp;Local news is kind of a mess. While the global platforms have been exploding, making it easier to follow events at a world wide level, local news sources have atrophied. Those two things are, obviously, intertwined. As the news moved online, the revenue sources that ...",
                        "pubDate": "Fri, 26 Jan 2018 17:44:08 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/hoodline-is-trying-to-fix-local-news-deserts-with-a-new-automated-news-wire/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/hoodline_blog_wireservice.png",
                                "type": "image/png",
                                "length": null
                            }
                        ],
                        "id": "0000335"
                    },
                    {
                        "title": "Original Content podcast: We revisit the crime scene of Netflix’s ‘Ozark’",
                        "link": "https://techcrunch.com/2018/01/26/original-content-ozark/?ncid=rss",
                        "body": "&nbsp;Ozark launched on Netflix back in July &#8212; before we&#8217;d even started Original Content, our podcast covering the latest streaming news, shows and movies. However, TechCrunch&#8217;s Jordan Crook was a big fan, so at her urging, we all checked out the series, which ...",
                        "pubDate": "Fri, 26 Jan 2018 18:22:16 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/original-content-ozark/#respond",
                        "id": "0000334"
                    },
                    {
                        "title": "Up close with Apple’s HomePod",
                        "link": "https://techcrunch.com/2018/01/26/up-close-with-apples-homepod/?ncid=rss",
                        "body": "&nbsp;The HomePod is much smaller than I remember. I hadn&#8217;t seen one since June, when Apple announced the speaker back at WWDC. Apple pushed back its projected December launch date because the company needed, &#8220;a little more time.&#8221; That time, it seems, has ...",
                        "pubDate": "Fri, 26 Jan 2018 18:26:29 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/up-close-with-apples-homepod/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/screen-shot-2018-01-26-at-10-41-00-am.png",
                                "type": "image/png",
                                "length": null
                            }
                        ],
                        "id": "0000333"
                    },
                    {
                        "title": "MoviePass pulls out of AMC’s top theaters as negotiations fail",
                        "link": "https://techcrunch.com/2018/01/26/moviepass-pulls-out-of-amcs-top-theaters-as-negotiations-fail/?ncid=rss",
                        "body": "&nbsp;MoviePass, the monthly subscription service for seeing movies in theaters, has pulled out of 10 high-traffic AMC theaters, as a negotiating tactic with the theater chain. AMC, so far, has shown no interest in working with MoviePass or sharing revenue with the service for ...",
                        "pubDate": "Fri, 26 Jan 2018 18:42:15 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/moviepass-pulls-out-of-amcs-top-theaters-as-negotiations-fail/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2017/11/gettyimages-482410396.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000332"
                    },
                    {
                        "title": "Strix Leviathan wants to build a better enterprise platform for crypto trading",
                        "link": "https://techcrunch.com/2018/01/26/strix-leviathan-wants-to-build-a-better-enterprise-platform-for-crypto-trading/?ncid=rss",
                        "body": "&nbsp;We are still in the early days of cryptocurrency &#8212; or at least that&#8217;s what all of the startups that are jumping into this space NOW hope. And when there&#8217;s a gold rush, there&#8217;s plenty of money to be made by selling shovels (or ASIC rigs) to miners. ...",
                        "pubDate": "Fri, 26 Jan 2018 18:42:44 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/strix-leviathan-wants-to-build-a-better-enterprise-platform-for-crypto-trading/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/gettyimages-761606093.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000331"
                    },
                    {
                        "title": "Facebook lets you tip game live streamers $3+",
                        "link": "https://techcrunch.com/2018/01/26/facebook-gamer-tipping/?ncid=rss",
                        "body": "&nbsp;Facebook Live is launching monetization for video gameplay streamers, allowing users to tip creators a minimum of $3 via the desktop site. Right now, the contributor of the tips doesn&#8217;t get any special call-out or privileges, though Facebook tells me it&#8217;s ...",
                        "pubDate": "Fri, 26 Jan 2018 19:01:16 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/facebook-gamer-tipping/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2018/01/facebook-game-streaming.png",
                                "type": "image/png",
                                "length": null
                            }
                        ],
                        "id": "0000330"
                    },
                    {
                        "title": "Twitter now lets advertisers sponsor publishers’ Moments",
                        "link": "https://techcrunch.com/2018/01/26/twitter-now-lets-advertisers-sponsor-publishers-moments/?ncid=rss",
                        "body": "&nbsp;Twitter has added a new advertising product to its lineup. The company announced today it&#8217;s offering brands the ability to sponsor Moments &#8211; the &#8220;Stories&#8221;-like feature that includes a series of tweets, often including images, GIFs and video &#8211; ...",
                        "pubDate": "Fri, 26 Jan 2018 19:44:14 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/twitter-now-lets-advertisers-sponsor-publishers-moments/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2017/05/gettyimages-502130278.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000329"
                    },
                    {
                        "title": "Madden NFL 18 esports are coming to Disney XD and ESPN",
                        "link": "https://techcrunch.com/2018/01/26/madden-nfl-18-esports-are-coming-to-disney-xd-and-espn/?ncid=rss",
                        "body": "&nbsp;With the Superbowl just a few weeks away, the world of football has some relatively unexpected news. Madden NFL, one of the most popular gaming titles in the world, is going even more mainstream. ESPN2 and Disney XD will broadcast Madden e-sports tournaments thanks to a ...",
                        "pubDate": "Fri, 26 Jan 2018 20:33:43 GMT",
                        "permaLink": "",
                        "comments": "https://techcrunch.com/2018/01/26/madden-nfl-18-esports-are-coming-to-disney-xd-and-espn/#respond",
                        "enclosure": [
                            {
                                "url": "http://tctechcrunch2011.files.wordpress.com/2016/09/50yards.jpg",
                                "type": "image/jpeg",
                                "length": null
                            }
                        ],
                        "id": "0000328"
                    }
                ]
            },
            {
                "feedTitle": "Editor's Blog",
                "feedUrl": "http://feeds.feedburner.com/talking-points-memo",
                "websiteUrl": "http://talkingpointsmemo.com/edblog",
                "feedDescription": "",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:58 GMT",
                "item": [
                    {
                        "title": "Stop Whining, Move Forward",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/cqtRnabN4S4/stop-whining-move-forward",
                        "body": "It&#8217;s amazing how quickly conventional wisdom can congeal. It&#8217;s even more amazing when it plays to Democrats&#8217; habit of garment-rending and self-flagellation. This morning I read this in the lede of The Washington Post&#8216;s Daily 202.\r\nSeven takeaways from the ...",
                        "pubDate": "Tue, 23 Jan 2018 14:13:47 GMT",
                        "permaLink": "",
                        "id": "0000327"
                    },
                    {
                        "title": "Meet Brandon Griesemer",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/FTWNFRtfiOI/meet-brandon-griesemer",
                        "body": "Meet Brandon Griesemer, the 19 year old Michigan man who I referred to in the post below. He lives in suburban Detroit and made a series of calls to CNN headquarters in Atlanta threatening mass murder as payback for &#8220;fake news.&#8221; &#8220;Fake news. I&#8217;m coming to ...",
                        "pubDate": "Tue, 23 Jan 2018 14:31:55 GMT",
                        "permaLink": "",
                        "id": "0000326"
                    },
                    {
                        "title": "Keeping It Real",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/_FfQ0u8HwOI/keeping-it-real-3",
                        "body": "As I argued below, being in the minority is difficult. You have no power. Fights take a long time. The worst thing Democrats can do is fall into the old pattern of garment-rending and self-flagellation. But let&#8217;s look at a poll result just released about who gets the ...",
                        "pubDate": "Tue, 23 Jan 2018 17:52:19 GMT",
                        "permaLink": "",
                        "id": "0000325"
                    },
                    {
                        "title": "A Favor",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/YLe9tDap2QU/a-favor-3",
                        "body": "I have a quick favor to ask. It will take a minute or two tops. It&#8217;s very important for the site. For the next twenty-four hours we&#8217;re running our annual reader survey. If you could take a moment to fill it out I would appreciate it greatly. Again, it&#8217;s just a ...",
                        "pubDate": "Tue, 23 Jan 2018 18:00:16 GMT",
                        "permaLink": "",
                        "id": "0000324"
                    },
                    {
                        "title": "Follow the Money",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/D6sbtSLISW4/making-sense-of-the-nra-going-mum-on-russian-money",
                        "body": "Read More&nbsp;&rarr;",
                        "pubDate": "Tue, 23 Jan 2018 18:53:03 GMT",
                        "permaLink": "",
                        "id": "0000323"
                    },
                    {
                        "title": "Mid-Afternoon Roundup On The Russia Probe",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/iU8IJ_SyOmk/trump-russia-probe-new-developments-summary",
                        "body": "The New York Times just reported that former FBI Director James Comey sat for an interview with special counsel Robert Mueller last year, focused on memos Comey wrote about his interactions with President Donald Trump.And that&#8217;s not all. We&#8217;ve seen a number of new ...",
                        "pubDate": "Tue, 23 Jan 2018 19:51:22 GMT",
                        "permaLink": "",
                        "id": "0000322"
                    },
                    {
                        "title": "This Is A Big, Big Deal",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/qAT8KLyCU-w/this-is-a-big-big-deal-2",
                        "body": "There&#8217;s something very notable, potentially very important in the new Washington Post article reporting Robert Mueller&#8217;s push to interview President Trump. Read More&nbsp;&rarr;",
                        "pubDate": "Tue, 23 Jan 2018 21:13:54 GMT",
                        "permaLink": "",
                        "id": "0000321"
                    },
                    {
                        "title": "Today’s Mueller Revelations Were The Biggest in Months",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/lRIatcductc/todays-mueller-revelations-were-the-biggest-in-months",
                        "body": "Over the course of today there was a rush of nugget-sized revelations about the Trump/Russia investigation. Overnight there was news that FBI Director Christopher Wray had threatened to resign. Then we learned that Jeff Sessions had sat for an extended interview with Robert ...",
                        "pubDate": "Wed, 24 Jan 2018 01:59:35 GMT",
                        "permaLink": "",
                        "id": "0000320"
                    },
                    {
                        "title": "Where The Burden Falls",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/wq_FekLL1Bs/manchin-azar-reproductive-rights-hhs-good-women",
                        "body": "From Alice Ollstein&#8217;s feature this morning, this little nugget. Sen. Joe Manchin (D-WV) defended his support for Alex Azar to head up HHS (his Senate confirmation is expected today) in light of Azar&#8217;s comments on reproductive rights issues: “Trust me, I have enough ...",
                        "pubDate": "Wed, 24 Jan 2018 14:13:30 GMT",
                        "permaLink": "",
                        "id": "0000319"
                    },
                    {
                        "title": "You Need To Watch This",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/xvskA1Kj0PQ/you-need-to-watch-this",
                        "body": "I&#8217;ve been watching rightwing media since the late 80s. I&#8217;ve been doing it professionally for two decades. Very little surprises me. But last night on a tip I checked out a series of segments on Fox claiming new evidence of a anti-Trump &#8220;secret society&#8221; at ...",
                        "pubDate": "Wed, 24 Jan 2018 15:11:52 GMT",
                        "permaLink": "",
                        "id": "0000318"
                    },
                    {
                        "title": "Guilt By Inference",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/A3t4mqx2Z2s/guilt-by-inference",
                        "body": "I remain skeptical that the Mueller probe will end with hard evidence that Donald Trump committed crimes on the &#8216;collusion&#8217; part of the investigation. (The obstruction front is a very different matter.) &#8216;Skepticism&#8217; may even overstate my knowledge. I ...",
                        "pubDate": "Wed, 24 Jan 2018 19:18:25 GMT",
                        "permaLink": "",
                        "id": "0000317"
                    },
                    {
                        "title": "The Evolving Definition",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/wYc5IRgRtxo/the-evolving-definition",
                        "body": "The Times&#8217; Maggie Haberman pressed Sarah Sanders today on how the President is defining &#8220;collusion&#8221; these days.\r\n\r\nInteresting new standard for &#8220;collusion&#8221; Sanders sets forth here. Seems to be. Collusion would mean that Trump won *because of Russian ...",
                        "pubDate": "Wed, 24 Jan 2018 20:47:11 GMT",
                        "permaLink": "",
                        "id": "0000316"
                    },
                    {
                        "title": "\"A Defense Lawyer's Worst Nightmare\"",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/T5Jv1jeD3pc/a-defense-lawyers-worst-nightmare",
                        "body": "From an ex-DOJ prosecutor whose insights I often seek, responding to a question I asked about this Prime Nugget item from yesterday.\r\nYour layman&#8217;s sense is right: &#8220;this tells me that Mueller&#8217;s interest may not suggest he thinks these other events are ...",
                        "pubDate": "Thu, 25 Jan 2018 00:13:43 GMT",
                        "permaLink": "",
                        "id": "0000315"
                    },
                    {
                        "title": "We're Not Focused On the Biggest Part of Trump's Immigration Agenda",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/VvsHFngJcOI/were-not-focused-on-the-biggest-part-of-trumps-immigration-agenda",
                        "body": "Yesterday, on behalf of the President, Sarah Sanders released a statement outlining the &#8220;four pillars&#8221; of his immigration plan. Read More&nbsp;&rarr;",
                        "pubDate": "Thu, 25 Jan 2018 16:05:22 GMT",
                        "permaLink": "",
                        "id": "0000314"
                    },
                    {
                        "title": "About That Photo",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/Ra1tvalrOUc/about-that-photo",
                        "body": "Read More&nbsp;&rarr;",
                        "pubDate": "Thu, 25 Jan 2018 20:10:48 GMT",
                        "permaLink": "",
                        "id": "0000313"
                    },
                    {
                        "title": "Where'd You Get the Money",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/sjCTS08FQ7I/whered-you-get-the-money",
                        "body": "The NRA has responded five days late to the Russian money story with a non-denial denial from an &#8220;outside lawyer.&#8221;",
                        "pubDate": "Thu, 25 Jan 2018 23:39:22 GMT",
                        "permaLink": "",
                        "id": "0000312"
                    },
                    {
                        "title": "Boom",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/wdiwByZ57FA/boom-11",
                        "body": "And we have more. Trump tried to fire Mueller in June. But he backed down when his White House Counsel threatened to quit. Sometimes it just seems like this won&#8217;t go four years. More to come shortly &#8230;",
                        "pubDate": "Fri, 26 Jan 2018 01:41:47 GMT",
                        "permaLink": "",
                        "id": "0000311"
                    },
                    {
                        "title": "Putting The Mueller Firing Story in Context",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/uAe2YmMdmnc/putting-the-mueller-firing-story-in-context",
                        "body": "Read More&nbsp;&rarr;",
                        "pubDate": "Fri, 26 Jan 2018 16:06:45 GMT",
                        "permaLink": "",
                        "id": "0000310"
                    },
                    {
                        "title": "Key Point",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/N9-l-IaqQS0/key-point-2",
                        "body": "Here&#8217;s one structural/political point to keep in mind about President Trump&#8217;s proposed &#8220;four pillars&#8221; immigration deal. We know from hard experience that almost no piece of immigration legislation on the 2006/2013 model can make its way through the House. ...",
                        "pubDate": "Fri, 26 Jan 2018 16:37:16 GMT",
                        "permaLink": "",
                        "id": "0000309"
                    },
                    {
                        "title": "This Sounds Just Right To Me",
                        "link": "http://feedproxy.google.com/~r/Talking-Points-Memo/~3/jb-SSMTgDZA/this-sounds-just-right-to-me",
                        "body": "Former federal prosecutor shares a key point about June and July of last year when Trump&#8217;s staff was trying their best to keep Trump from destroying himself and the limits on Ty Cobb&#8217;s cooperation strategy. Give this a read. Very important perspective.\r\nI wanted to ...",
                        "pubDate": "Fri, 26 Jan 2018 19:50:08 GMT",
                        "permaLink": "",
                        "id": "0000308"
                    }
                ]
            },
            {
                "feedTitle": "Political Wire",
                "feedUrl": "http://feeds.feedburner.com/PoliticalWire",
                "websiteUrl": "http://politicalwire.com/",
                "feedDescription": "All the political news in one place",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:58 GMT",
                "item": [
                    {
                        "title": "Quote of the Day",
                        "link": "http://politicalwire.com/2018/01/26/quote-of-the-day-1837/",
                        "body": "&#8220;I have literally been on Air Force One once and there were several people in the room when I was there. He says that I’ve been talking a lot with the president in the Oval about my political future. I’ve never talked once to the president about my future and I am never ...",
                        "pubDate": "Fri, 26 Jan 2018 12:06:19 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/quote-of-the-day-1837/#respond",
                        "id": "0000307"
                    },
                    {
                        "title": "Trump Denies Ordering Firing of Mueller",
                        "link": "http://politicalwire.com/2018/01/26/trump-denies-ordering-firing-mueller/",
                        "body": "President Trump dismissed reports that he ordered special counsel Robert Mueller fired last summer, according to USA Today saying: &#8220;Fake news. Fake news. Typical New York Times. Fake stories.&#8221;",
                        "pubDate": "Fri, 26 Jan 2018 12:56:49 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/trump-denies-ordering-firing-mueller/#respond",
                        "id": "0000306"
                    },
                    {
                        "title": "White House Immigration Proposal Is Already Dead",
                        "link": "http://politicalwire.com/2018/01/26/white-house-immigration-proposal-already-dead/",
                        "body": "Jonathan Swan: &#8220;The White House framework on immigration reform that leaked yesterday — then was released early — is a non-starter those on the left, having spoken to progressive immigration leaders in close touch with top Democrats.&#8221;",
                        "pubDate": "Fri, 26 Jan 2018 12:59:52 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/white-house-immigration-proposal-already-dead/#respond",
                        "id": "0000305"
                    },
                    {
                        "title": "Mueller Knows Trump Was Trying to Obstruct Probe",
                        "link": "http://politicalwire.com/2018/01/26/mueller-now-knows-trump-trying-obstruct-probe/",
                        "body": "Renato Mariotti: &#8220;As we learned Thursday in the New York Times, there was indeed a need to protect Mueller back in June, when Trump ordered the firing of special counsel due to &#8216;conflicts of interest&#8217; that were not actually conflicts and appear to be thinly ...",
                        "pubDate": "Fri, 26 Jan 2018 13:04:35 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/mueller-now-knows-trump-trying-obstruct-probe/#respond",
                        "id": "0000304"
                    },
                    {
                        "title": "Will Trump Try Again to Fire Mueller?",
                        "link": "http://politicalwire.com/2018/01/26/officials-worried-trump-will-try-fire-mueller/",
                        "body": "On June 12, 2017, Newsmax CEO told PBS that he thought President Trump was &#8220;considering perhaps terminating the special counsel. I think he&#8217;s weighing that option.&#8221; On the same day, political analyst April Ryan told CNN there was &#8220;mass hysteria&#8221; in ...",
                        "pubDate": "Fri, 26 Jan 2018 13:43:19 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/officials-worried-trump-will-try-fire-mueller/#respond",
                        "id": "0000303"
                    },
                    {
                        "title": "Mia Love Vulnerable In Re-Election Race",
                        "link": "http://politicalwire.com/2018/01/26/mia-love-vulnerable-re-election-race/",
                        "body": "A new Salt Lake Tribune-Hinckley Institute of Politics poll shows Rep. Mia Love (R) with a small 5-point edge over Salt Lake County Mayor Ben McAdams (D), 47% to 42%, &#8220;a slightly narrower lead than she had three months ago and just outside of the margin of error of plus or ...",
                        "pubDate": "Fri, 26 Jan 2018 13:51:45 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/mia-love-vulnerable-re-election-race/#respond",
                        "id": "0000302"
                    },
                    {
                        "title": "Bonus Quote of the Day",
                        "link": "http://politicalwire.com/2018/01/26/bonus-quote-day-287/",
                        "body": "“I will say this with great conviction, that had the opposing party won, in my opinion, because they would have added tremendous regulation, I believe the markets would’ve been down anywhere from 25 to 50 percent.”\n&#8212; President Trump, quoted by Axios, during a meeting at ...",
                        "pubDate": "Fri, 26 Jan 2018 14:33:51 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/bonus-quote-day-287/#respond",
                        "id": "0000301"
                    },
                    {
                        "title": "Why Republicans Ultimately Get the Blame for Shutdown",
                        "link": "http://politicalwire.com/2018/01/26/republicans-ultimately-get-blame-shutdown/",
                        "body": "Harry Enten: &#8220;When you combine the people who blamed Trump and with those who blamed congressional Republicans, you end up with 49 percent of voters saying Republican politicians were at fault vs. 32 blaming Democrats.&#8221;\n&#8220;Now, is there value in knowing how blame ...",
                        "pubDate": "Fri, 26 Jan 2018 14:43:08 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/republicans-ultimately-get-blame-shutdown/#respond",
                        "id": "0000300"
                    },
                    {
                        "title": "The Battle Now Is Over Legal Immigration",
                        "link": "http://politicalwire.com/2018/01/26/battle-now-legal-immigration/",
                        "body": "NBC News: “After years of high-profile debates over a border wall and a path to citizenship, the biggest obstacle to a bipartisan deal on DACA is quickly becoming legal immigration, an issue where President Donald Trump has presided over a monumental shift in the GOP&#8217;s ...",
                        "pubDate": "Fri, 26 Jan 2018 15:02:43 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/battle-now-legal-immigration/#respond",
                        "id": "0000299"
                    },
                    {
                        "title": "Baker Has Trounced Democrats In Fundraising",
                        "link": "http://politicalwire.com/2018/01/26/baker-trounced-democrats-fundraising/",
                        "body": "Boston Globe: &#8220;The major problem for the Democratic candidates is the perception that Baker, with his consistently high standing in the surveys and a never-before-seen fund-raising juggernaut, is hugely favored for reelection. Between Baker and his No. 2, Karyn Polito, the ...",
                        "pubDate": "Fri, 26 Jan 2018 15:27:20 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/baker-trounced-democrats-fundraising/#respond",
                        "id": "0000298"
                    },
                    {
                        "title": "Schumer Rejects Trump’s Immigration Proposal",
                        "link": "http://politicalwire.com/2018/01/26/schumer-rejects-trumps-immigration-proposal/",
                        "body": "Senate Minority Leader Chuck Schumer (D-NY) &#8220;opposes the immigration framework released by the White House — a potentially fatal blow for the prospective legislation in the closely divided Senate,&#8221; Politico reports.\nSaid Schumer: &#8220;This plan flies in the face of ...",
                        "pubDate": "Fri, 26 Jan 2018 17:28:05 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/schumer-rejects-trumps-immigration-proposal/#respond",
                        "id": "0000297"
                    },
                    {
                        "title": "Clinton Shielded Adviser Accused of Harassment",
                        "link": "http://politicalwire.com/2018/01/26/clinton-shielded-adviser-accused-harassment/",
                        "body": "&#8220;A senior adviser to Hillary Clinton’s 2008 presidential campaign who was accused of repeatedly sexually harassing a young subordinate was kept on the campaign at Mrs. Clinton’s request,&#8221; the New York Times reports.\n&#8220;Mrs. Clinton’s campaign manager at the time ...",
                        "pubDate": "Fri, 26 Jan 2018 17:44:14 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/clinton-shielded-adviser-accused-harassment/#respond",
                        "id": "0000296"
                    },
                    {
                        "title": "Bush Says Trump May Drag Down GOP In Midterms",
                        "link": "http://politicalwire.com/2018/01/26/bush-says-trump-may-drag-gop-midterms/",
                        "body": "Former Florida Gov. Jeb Bush (R) &#8220;warns that Republicans are in for a beating in the fall elections if congressional races focus on the rhetoric and character of President Trump,&#8221; USA Today reports.\n&#8220;Bush lambasted Trump&#8217;s erratic leadership ...",
                        "pubDate": "Fri, 26 Jan 2018 17:47:36 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/bush-says-trump-may-drag-gop-midterms/#respond",
                        "id": "0000295"
                    },
                    {
                        "title": "Will Congress Move to Protect Mueller?",
                        "link": "http://politicalwire.com/2018/01/26/will-congress-move-protect-mueller/",
                        "body": "Washington Post: &#8220;There are two bills in Congress, both of which have some Republican support, that would protect Mueller from being fired by Trump. But neither bill has been seriously considered by leadership.&#8221;\n&#8220;Up until this point, Republicans had given Trump ...",
                        "pubDate": "Fri, 26 Jan 2018 17:59:16 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/will-congress-move-protect-mueller/#respond",
                        "id": "0000294"
                    },
                    {
                        "title": "A Russia Investigation Timeline",
                        "link": "http://politicalwire.com/2018/01/26/russia-investigation-timeline/",
                        "body": "NBC News a useful interactive timeline to help better understand the key details and players involved in special counsel Robert Mueller&#8217;s Russia investigation.",
                        "pubDate": "Fri, 26 Jan 2018 18:09:44 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/russia-investigation-timeline/#respond",
                        "id": "0000293"
                    },
                    {
                        "title": "The Unbearable Awkwardness of Being Melania",
                        "link": "http://politicalwire.com/2018/01/26/unbearable-awkwardness-melania/",
                        "body": "Renee Graham: &#8220;Melania has seemed more reluctant than most women thrust into this odd, undefined space. There’s no indication that she’s a calming influence on her erratic husband or his chaotic presidency, which seems the most basic thing the nation could want from this ...",
                        "pubDate": "Fri, 26 Jan 2018 18:12:49 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/unbearable-awkwardness-melania/#respond",
                        "id": "0000292"
                    },
                    {
                        "title": "A Quarter of the Way to Taking the House?",
                        "link": "http://politicalwire.com/2018/01/26/quarter-way-taking-house/",
                        "body": "This piece is only available to Political Wire members.\nJoin today for exclusive analysis, new features and no advertising.\nSign in to your account or join today!\n Monthly &#8211; $5 per month  or  Annual &#8211; $50 per year \n&nbsp;",
                        "pubDate": "Fri, 26 Jan 2018 19:29:29 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/quarter-way-taking-house/#respond",
                        "id": "0000291"
                    },
                    {
                        "title": "Kasich Plans Trip to New Hampshire",
                        "link": "http://politicalwire.com/2018/01/26/kasich-plans-trip-new-hampshire/",
                        "body": "Gov. John Kasich will return to New Hampshire in April, the Concord Monitor reports.\nThe news &#8220;will once again fuel rumors that he’s considering another White House bid in 2020.&#8221;",
                        "pubDate": "Fri, 26 Jan 2018 19:45:18 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/kasich-plans-trip-new-hampshire/#respond",
                        "id": "0000290"
                    },
                    {
                        "title": "Trump Doesn’t Really Understand His Job",
                        "link": "http://politicalwire.com/2018/01/26/trump-doesnt-really-understand-job/",
                        "body": "Matthew Yglesias: &#8220;President Trump’s first non-Fox television interview in a long time, conducted with CNBC’s Joe Kernen from Davos, Switzerland, is in many respects weirdly devoid of substance. And much of the substance that’s there consists of misstatements of ...",
                        "pubDate": "Fri, 26 Jan 2018 20:06:58 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/trump-doesnt-really-understand-job/#respond",
                        "id": "0000289"
                    },
                    {
                        "title": "RNC Finance Chairman Faces Allegations of Misconduct",
                        "link": "http://politicalwire.com/2018/01/26/rnc-finance-chairman-faces-allegations-misconduct/",
                        "body": "&#8220;Casino mogul Steve Wynn has no immediate plans to relinquish his role as finance chairman of the Republican National Committee in the wake of reports detailing decades of alleged sexual misconduct,&#8221; the Washington Post reports.\n&#8220;A report by the Wall Street ...",
                        "pubDate": "Fri, 26 Jan 2018 20:16:24 GMT",
                        "permaLink": "",
                        "comments": "http://politicalwire.com/2018/01/26/rnc-finance-chairman-faces-allegations-misconduct/#respond",
                        "id": "0000288"
                    }
                ]
            },
            {
                "feedTitle": "US news | The Guardian",
                "feedUrl": "http://www.guardian.co.uk/world/usa/rss",
                "websiteUrl": "https://www.theguardian.com/us-news",
                "feedDescription": "Latest US news, breaking news and current affairs coverage from theguardian.com",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:58 GMT",
                "item": [
                    {
                        "title": "What is Daca and who are the Dreamers?",
                        "link": "https://www.theguardian.com/us-news/2017/sep/04/donald-trump-what-is-daca-dreamers",
                        "body": "Here is everything you need to know about the program that gives temporary protection to undocumented migrants who arrived in the US as childrenTrump says he’s ‘fairly close’ to deal with Democrats to protect ‘Dreamers’The Trump administration announced last week that it planned ...",
                        "pubDate": "Thu, 14 Sep 2017 15:30:29 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2017/sep/04/donald-trump-what-is-daca-dreamers",
                        "id": "0000287"
                    },
                    {
                        "title": "Donald Trump's first year: in his own words - video",
                        "link": "https://www.theguardian.com/us-news/video/2018/jan/18/donald-trumps-first-year-in-his-own-words-video",
                        "body": "Donald Trump's first year as US president has seen a daily battle with the media, a federal investigation into his campaign team and a series of domestic and diplomatic bust-ups. In his own inimitable way he describes the events as he sees them Continue reading...",
                        "pubDate": "Thu, 18 Jan 2018 15:19:18 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/video/2018/jan/18/donald-trumps-first-year-in-his-own-words-video",
                        "id": "0000286"
                    },
                    {
                        "title": "Theresa May risks unrest by paving way for Donald Trump visit",
                        "link": "https://www.theguardian.com/us-news/2018/jan/25/trump-and-may-discuss-uk-visit-later-this-year-davos",
                        "body": "Prime minister orders officials to make plans for visit by the US president later this year after pair meet at DavosTheresa May has taken the gamble of inviting Donald Trump to make his controversial visit to Britain later this year after a meeting between the prime minister and ...",
                        "pubDate": "Thu, 25 Jan 2018 19:37:16 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/25/trump-and-may-discuss-uk-visit-later-this-year-davos",
                        "id": "0000285"
                    },
                    {
                        "title": "Senate committee to release transcripts of Donald Trump Jr interview",
                        "link": "https://www.theguardian.com/us-news/2018/jan/25/senate-committee-to-release-transcripts-of-donald-trump-jr-interview",
                        "body": "Senator Grassley says committee will not interview Jared KushnerCommittee is concluding investigation into Trump Tower meetingThe Senate judiciary committee plans to release transcripts from closed-door interviews with Donald Trump Jr and others, as it wraps up its investigation ...",
                        "pubDate": "Thu, 25 Jan 2018 18:36:26 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/25/senate-committee-to-release-transcripts-of-donald-trump-jr-interview",
                        "id": "0000284"
                    },
                    {
                        "title": "Donald Trump prepared to apologise for UK far-right video retweets",
                        "link": "https://www.theguardian.com/us-news/2018/jan/26/donald-trump-prepared-to-apologise-for-retweeting-britain-first",
                        "body": "US president tells ITV he did not know of Britain First and only wanted to oppose Islamic terrorDonald Trump has said he is prepared to apologise for retweeting inflammatory videos by the far-right group Britain First, as he seeks to prepare the ground for a visit to the UK this ...",
                        "pubDate": "Fri, 26 Jan 2018 10:40:02 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/26/donald-trump-prepared-to-apologise-for-retweeting-britain-first",
                        "id": "0000283"
                    },
                    {
                        "title": "Donald Trump denies report he tried to fire Robert Mueller in June",
                        "link": "https://www.theguardian.com/us-news/2018/jan/25/trump-mueller-firing-june-white-house-counsel-quit",
                        "body": "President was dissuaded from firing Robert Mueller when counsel threatened to quit rather than carry out order, New York Times reportsDonald Trump has denied a report he ordered the firing of special counsel Robert Mueller last June, but was persuaded against it after the White ...",
                        "pubDate": "Fri, 26 Jan 2018 09:46:36 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/25/trump-mueller-firing-june-white-house-counsel-quit",
                        "id": "0000282"
                    },
                    {
                        "title": "White House asks for Van Gogh loan – but Guggenheim offers gold toilet instead",
                        "link": "https://www.theguardian.com/us-news/2018/jan/25/white-house-gold-toilet-loan-guggenheim-museum",
                        "body": "The Guggenheim Museum proposed lending Maurizio Cattelan’s America after turning down a request for Landscape With Snow The Guggenheim Museum has reportedly turned down a White House request to borrow a Van Gogh painting, and has instead offered the Trump administration the use ...",
                        "pubDate": "Fri, 26 Jan 2018 07:46:50 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/25/white-house-gold-toilet-loan-guggenheim-museum",
                        "id": "0000281"
                    },
                    {
                        "title": "Judge tells Larry Nassar 'I just signed your death warrant' – video",
                        "link": "https://www.theguardian.com/sport/video/2018/jan/25/judge-tells-larry-nassar-i-just-signed-your-death-warrant-video",
                        "body": "Judge Rosemarie Aquilina tells the disgraced Michigan sports doctor Larry Nassar that she does not think that he should ever leave jail and does not believe he can be rehabilitated. 'It is my honour and privilege to sentence you,' she tells the defendant. Aquilina&nbsp;sentenced ...",
                        "pubDate": "Thu, 25 Jan 2018 00:17:12 GMT",
                        "permaLink": "https://www.theguardian.com/sport/video/2018/jan/25/judge-tells-larry-nassar-i-just-signed-your-death-warrant-video",
                        "id": "0000280"
                    },
                    {
                        "title": "Trump to May: 'We love your country' – video",
                        "link": "https://www.theguardian.com/us-news/video/2018/jan/25/donald-trump-theresa-may-we-love-your-country-video",
                        "body": "Donald Trump and Theresa May hold a bilateral talk at Davos 2018. Trump used the opportunity to confirm his 'great relationship' with the prime minister and announce his love for the UKDavos 2018: Donald Trump and Theresa May meet - live updates Continue reading...",
                        "pubDate": "Thu, 25 Jan 2018 15:34:00 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/video/2018/jan/25/donald-trump-theresa-may-we-love-your-country-video",
                        "id": "0000279"
                    },
                    {
                        "title": "'I would certainly apologise': Trump on Britain First retweets – video",
                        "link": "https://www.theguardian.com/us-news/video/2018/jan/26/i-would-certainly-apologise-trump-britain-first-far-right-retweets-video",
                        "body": "Donald Trump says he is prepared to apologise for&nbsp;retweeting inflammatory videos&nbsp;by a British far-right group. The US president tells ITV’s Good Morning Britain&nbsp;he knew nothing about Britain First when he shared the posts. Trump was interviewed by Piers Morgan in ...",
                        "pubDate": "Fri, 26 Jan 2018 08:46:09 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/video/2018/jan/26/i-would-certainly-apologise-trump-britain-first-far-right-retweets-video",
                        "id": "0000278"
                    },
                    {
                        "title": "As State of the Union nears, is America great again for the working class?",
                        "link": "https://www.theguardian.com/us-news/2018/jan/25/trump-state-of-union-workers-rights-working-class-record",
                        "body": "Donald Trump has painted himself a champion of workers, and will probably do so again next week. But the record tells a different storyFew parts of the American body politic have been analysed as thoroughly as the love affair between a certain section of America’s working class ...",
                        "pubDate": "Thu, 25 Jan 2018 11:00:14 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/25/trump-state-of-union-workers-rights-working-class-record",
                        "id": "0000277"
                    },
                    {
                        "title": "As a black, gay woman I have to be selective in my outrage. So should you | Ashley ‘Dotty’ Charles",
                        "link": "https://www.theguardian.com/commentisfree/2018/jan/25/black-gay-woman-selective-outrage-hashtag-protest-social-media",
                        "body": "Before social media, protest was provocative and empowering. Outrage used to mean something – now it’s just another hashtag• Ashley ‘Dotty’ Charles is a BBC Radio 1Xtra presenterEveryone is offended by everything. It’s exhausting. Keeping up with all the noninclusive, ...",
                        "pubDate": "Thu, 25 Jan 2018 14:21:15 GMT",
                        "permaLink": "https://www.theguardian.com/commentisfree/2018/jan/25/black-gay-woman-selective-outrage-hashtag-protest-social-media",
                        "id": "0000276"
                    },
                    {
                        "title": "The right's not-so-secret effort to discredit Trump-Russia inquiry",
                        "link": "https://www.theguardian.com/us-news/2018/jan/25/trump-russia-inquiry-secret-society-discredit",
                        "body": "The president’s supporters in Congress and the media have been trumpeting the idea of a conspiracy within the FBIAttacks by Donald Trump on US intelligence agencies were taken up and amplified by the US right at full volume this week, as special counsel Robert Mueller’s ...",
                        "pubDate": "Thu, 25 Jan 2018 19:19:09 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/25/trump-russia-inquiry-secret-society-discredit",
                        "id": "0000275"
                    },
                    {
                        "title": "How the far right has perfected the art of deniable racism | Gary Younge",
                        "link": "https://www.theguardian.com/commentisfree/2018/jan/26/far-right-racism-electoral-successes-europe-us-bigotry",
                        "body": "Electoral successes in Europe and the US are the result of a process whereby bigotry is made palatableIn July 2016 the bigoted troll Milo Yiannopoulos, a British darling of the American far right, was banned from Twitter after encouraging a torrent of racist abuse at Leslie ...",
                        "pubDate": "Fri, 26 Jan 2018 06:00:38 GMT",
                        "permaLink": "https://www.theguardian.com/commentisfree/2018/jan/26/far-right-racism-electoral-successes-europe-us-bigotry",
                        "id": "0000274"
                    },
                    {
                        "title": "Why was this man arrested for giving water to migrants crossing the border?",
                        "link": "https://www.theguardian.com/global/2018/jan/26/scott-warren-no-more-death-arrested-migrants-water",
                        "body": "Scott Warren was arrested after he helped migrants – but he’s a humanitarian aid worker trying to save lives in a place where so many find death Cabeza Prieta national wildlife refuge, which includes 56 miles of Sonoran Desert along the US-Mexico border, is a stunningly ...",
                        "pubDate": "Fri, 26 Jan 2018 11:00:44 GMT",
                        "permaLink": "https://www.theguardian.com/global/2018/jan/26/scott-warren-no-more-death-arrested-migrants-water",
                        "id": "0000273"
                    },
                    {
                        "title": "Here's how to turn back the Doomsday clock | David Shariatmadari",
                        "link": "https://www.theguardian.com/commentisfree/2018/jan/26/donald-trump-kim-jong-un-nuclear-war-doomsday-clock",
                        "body": "The world needs to stop worrying and learn to live with North Korea’s nukes if a devastating war is to be avoidedIt’s two minutes to midnight. That’s the view of the Bulletin of Atomic Scientists, which has published its wake-up call to humanity every year since 1947. Midnight, ...",
                        "pubDate": "Fri, 26 Jan 2018 11:48:11 GMT",
                        "permaLink": "https://www.theguardian.com/commentisfree/2018/jan/26/donald-trump-kim-jong-un-nuclear-war-doomsday-clock",
                        "id": "0000272"
                    },
                    {
                        "title": "Larry Nassar shouldn't be the only one going to jail | Michael Dolce",
                        "link": "https://www.theguardian.com/commentisfree/2018/jan/26/larry-nassar-jail",
                        "body": "Survivors reported Nassar’s abuse to coaches, trainers, parents, therapists, a training facility owner, and even law enforcement officials – but all in vainIt is tragically ironic that in the same month we applauded the courageous young survivors confronting Larry Nassar in ...",
                        "pubDate": "Fri, 26 Jan 2018 15:17:21 GMT",
                        "permaLink": "https://www.theguardian.com/commentisfree/2018/jan/26/larry-nassar-jail",
                        "id": "0000271"
                    },
                    {
                        "title": "The Guardian view on Theresa May’s government: divided they drift | Editorial",
                        "link": "https://www.theguardian.com/commentisfree/2018/jan/26/the-guardian-view-on-theresa-mays-government-divided-they-drift",
                        "body": "The crowds turned out for Donald Trump in Davos, but not for Theresa May. The reason is simple. Her government embodies a national decision that Britain does not wish to be taken seriously in the worldSolipsistic, dangerous and unpredictable as ever, Donald Trump commanded ...",
                        "pubDate": "Fri, 26 Jan 2018 16:52:53 GMT",
                        "permaLink": "https://www.theguardian.com/commentisfree/2018/jan/26/the-guardian-view-on-theresa-mays-government-divided-they-drift",
                        "id": "0000270"
                    },
                    {
                        "title": "Venice attracts 30m tourists a year. Why do so few visit its football club?",
                        "link": "https://www.theguardian.com/football/the-gentleman-ultra/2018/jan/26/venice-tourists-watch-football-venezia",
                        "body": "Venice has sold a myth of itself to tourists for centuries, so why has no one ever never succeeded in promoting its football club?By Patrick Graham for The Gentleman UltraThe travel writer Thomas Watkins wrote: “There is no country so much frequented yet so little known by ...",
                        "pubDate": "Fri, 26 Jan 2018 10:16:32 GMT",
                        "permaLink": "https://www.theguardian.com/football/the-gentleman-ultra/2018/jan/26/venice-tourists-watch-football-venezia",
                        "id": "0000269"
                    },
                    {
                        "title": "Sports quiz of the week: Alexis Sánchez, Kyle Edmund and Javier Mascherano",
                        "link": "https://www.theguardian.com/sport/2018/jan/26/sports-quiz-week-alexis-sanchez-kyle-edmund-javier-mascherano",
                        "body": "Who offered congratulations? Who won? And who knows nothing?• Tennis quiz: identify the players at the Australian Open• Football quiz: January transfer windowComplete the sequence: Alexis Sánchez, Memphis Depay, Ángel Di María, Antonio Valencia and ...Paul ScholesEric ...",
                        "pubDate": "Fri, 26 Jan 2018 12:32:37 GMT",
                        "permaLink": "https://www.theguardian.com/sport/2018/jan/26/sports-quiz-week-alexis-sanchez-kyle-edmund-javier-mascherano",
                        "id": "0000268"
                    },
                    {
                        "title": "The resurrected XFL is the perfect exponent of Trump's America",
                        "link": "https://www.theguardian.com/sport/2018/jan/26/xfl-trump-vince-mcmahon-football",
                        "body": "While the original XFL sought to profit on players’ freedom of expression, Vince McMahon’s reboot is based on limiting it. The message is clear: shut up and Make Football Great AgainVince McMahon screamed.It was 2 February 2001 and the man who made professional wrestling a ...",
                        "pubDate": "Fri, 26 Jan 2018 13:12:37 GMT",
                        "permaLink": "https://www.theguardian.com/sport/2018/jan/26/xfl-trump-vince-mcmahon-football",
                        "id": "0000267"
                    },
                    {
                        "title": "Entire USA Gymnastics board to resign in wake of Larry Nassar scandal",
                        "link": "https://www.theguardian.com/sport/2018/jan/26/mark-hollis-msu-ad-resigns-larry-nassar",
                        "body": "Remaining directors of USA Gymnastics resign amid scandal Michigan State athletic director stepped down Friday morningThe remaining directors of USA Gymnastics will resign following revelations that the former national team doctor sexually abused female athletes, complying with ...",
                        "pubDate": "Fri, 26 Jan 2018 16:44:03 GMT",
                        "permaLink": "https://www.theguardian.com/sport/2018/jan/26/mark-hollis-msu-ad-resigns-larry-nassar",
                        "id": "0000266"
                    },
                    {
                        "title": "Why is the US stock market so high – and is it justified? | Robert Shiller",
                        "link": "https://www.theguardian.com/business/2018/jan/23/us-stock-market-trump-tax-cuts-capital",
                        "body": "It’s not the Trump effect or tax cuts. Americans seem to have a strong desire to own capitalThe level of stock markets differs widely across countries. And right now, the United States is leading the world. What everyone wants to know is why – and whether its stock market’s ...",
                        "pubDate": "Tue, 23 Jan 2018 11:24:14 GMT",
                        "permaLink": "https://www.theguardian.com/business/2018/jan/23/us-stock-market-trump-tax-cuts-capital",
                        "id": "0000265"
                    },
                    {
                        "title": "Rupert Murdoch’s Sky bid is not in public interest, says regulator",
                        "link": "https://www.theguardian.com/business/2018/jan/23/rupert-murdoch-sky-bid-blocked-21st-century-fox-cma",
                        "body": "CMA rules against 21st Century Fox’s bid to take control of 61% of Sky it does not already ownRupert Murdoch’s £11.7bn bid to take full control of Sky could be blocked after the UK competition regulator said the deal would give his family to much control over UK news media. The ...",
                        "pubDate": "Tue, 23 Jan 2018 14:57:59 GMT",
                        "permaLink": "https://www.theguardian.com/business/2018/jan/23/rupert-murdoch-sky-bid-blocked-21st-century-fox-cma",
                        "id": "0000264"
                    },
                    {
                        "title": "Protectionism is not the answer, Angela Merkel warns US",
                        "link": "https://www.theguardian.com/business/2018/jan/24/wto-head-rejects-talk-global-trade-war-us-more-tariffs",
                        "body": "German chancellor uses Davos speech to protest against sanctions on imports from ChinaGermany’s chancellor Angela Merkel has sent out a strong warning to Donald Trump about the dangers of protectionism as the United States said it was preparing to impose fresh sanctions on ...",
                        "pubDate": "Wed, 24 Jan 2018 17:21:21 GMT",
                        "permaLink": "https://www.theguardian.com/business/2018/jan/24/wto-head-rejects-talk-global-trade-war-us-more-tariffs",
                        "id": "0000263"
                    },
                    {
                        "title": "Donald Trump woos business but attacks media at Davos",
                        "link": "https://www.theguardian.com/business/2018/jan/26/donald-trump-booed-in-davos-as-he-woos-businesses",
                        "body": "President declares US open for business as he accuses media for being nasty, mean and fake Donald Trump has taken his battle with the media on to the global stage by using a speech in Davos declaring the US open for business to accuse his press and TV critics of being mean, ...",
                        "pubDate": "Fri, 26 Jan 2018 16:32:13 GMT",
                        "permaLink": "https://www.theguardian.com/business/2018/jan/26/donald-trump-booed-in-davos-as-he-woos-businesses",
                        "id": "0000262"
                    },
                    {
                        "title": "IMF chief warns Trump's tax cuts could destabilise global economy",
                        "link": "https://www.theguardian.com/business/2018/jan/26/imf-chief-warns-trumps-tax-cuts-could-destabilise-global-economy",
                        "body": "Reforms may threaten recovery and lead to bigger US budget deficit, says Christine Lagarde Donald Trump’s huge tax cuts are a threat to the stability of the global economy, the managing director of the International Monetary Fund has warned.Christine Lagarde singled out Trump’s ...",
                        "pubDate": "Fri, 26 Jan 2018 18:48:39 GMT",
                        "permaLink": "https://www.theguardian.com/business/2018/jan/26/imf-chief-warns-trumps-tax-cuts-could-destabilise-global-economy",
                        "id": "0000261"
                    },
                    {
                        "title": "Oprah Winfrey ruled out running for president before Golden Globes speech",
                        "link": "https://www.theguardian.com/tv-and-radio/2018/jan/25/oprah-winfrey-president-dna-instyle",
                        "body": "‘It’s not something that interests me. I don’t have the DNA for it’Winfrey touted as possible Democratic contender in 2020Oprah Winfrey said she is not interested in running for president, in comments published this week that were made before the current wave of speculation that ...",
                        "pubDate": "Thu, 25 Jan 2018 16:54:59 GMT",
                        "permaLink": "https://www.theguardian.com/tv-and-radio/2018/jan/25/oprah-winfrey-president-dna-instyle",
                        "id": "0000260"
                    },
                    {
                        "title": "Museum of Natural History urged to cut ties with 'anti-science propagandist' Rebekah Mercer",
                        "link": "https://www.theguardian.com/us-news/2018/jan/25/american-museum-of-natural-history-rebekah-mercer",
                        "body": "More than 100 scientists have urged the museum to sever its ties with Mercer, one of Donald Trump’s top donorsThe American Museum of Natural History is under pressure to sever its ties to Rebekah Mercer, one of Donald Trump’s top donors who has used her family’s fortune to fund ...",
                        "pubDate": "Fri, 26 Jan 2018 00:00:31 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/25/american-museum-of-natural-history-rebekah-mercer",
                        "id": "0000259"
                    },
                    {
                        "title": "Trump proposes path to citizenship for 1.8m undocumented youths",
                        "link": "https://www.theguardian.com/us-news/2018/jan/25/donald-trump-dreamers-undocumented-citizenship",
                        "body": "President offers plan in exchange for border wall investment and crackdown on other undocumented people, among other measuresDonald Trump proposed legislation that would provide a pathway to citizenship for roughly 1.8 million young, undocumented immigrants in exchange for a ...",
                        "pubDate": "Fri, 26 Jan 2018 01:37:00 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/25/donald-trump-dreamers-undocumented-citizenship",
                        "id": "0000258"
                    },
                    {
                        "title": "Bombardier wins fight against huge US tariffs on aircraft imports",
                        "link": "https://www.theguardian.com/business/2018/jan/26/bombardier-us-tariffs-imports-uk-trump-jobs-belfast",
                        "body": "Defeat for Trump administration could save thousands of jobs at company’s Belfast operationThousands of jobs in Northern Ireland appear to have been saved after plane maker Bombardier won a legal battle in the US that has overturned damaging import tariffs on its C-Series ...",
                        "pubDate": "Fri, 26 Jan 2018 20:40:04 GMT",
                        "permaLink": "https://www.theguardian.com/business/2018/jan/26/bombardier-us-tariffs-imports-uk-trump-jobs-belfast",
                        "id": "0000257"
                    },
                    {
                        "title": "Trump-Russia investigation: the key questions answered",
                        "link": "https://www.theguardian.com/us-news/ng-interactive/2017/dec/08/donald-trump-russia-investigation-key-questions-latest-news-collusion-timeline",
                        "body": "Everything you need to know about the inquiry into Russian hacking, alleged collusion and Donald Trump, plus the latest news Continue reading...",
                        "pubDate": "Fri, 08 Dec 2017 12:00:02 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/ng-interactive/2017/dec/08/donald-trump-russia-investigation-key-questions-latest-news-collusion-timeline",
                        "id": "0000256"
                    },
                    {
                        "title": "'Still fighting': Africatown, site of last US slave shipment, sues over pollution",
                        "link": "https://www.theguardian.com/us-news/2018/jan/26/africatown-site-of-last-us-slave-ship-arrival-sues-over-factorys-pollution",
                        "body": "In 1860 the last, illegal, shipment of slaves to the US landed in this part of Alabama. Now hundreds of the largely African American residents are suing an industrial plant claiming it released toxic chemicals linked to cancerFrom the front seat of his truck, Joe Womack points ...",
                        "pubDate": "Fri, 26 Jan 2018 11:00:44 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/26/africatown-site-of-last-us-slave-ship-arrival-sues-over-factorys-pollution",
                        "id": "0000255"
                    },
                    {
                        "title": "Trump is booed at Davos as he takes swipe at media – video",
                        "link": "https://www.theguardian.com/us-news/video/2018/jan/26/trump-booed-at-davos-as-he-takes-swipe-at-media-video",
                        "body": "The US president uses an address at Davos to attack the 'mean and vicious' media, prompting boos and hisses from the audience. The founder of the World Economic Forum, Klaus Schwab, was also booed when he introduced Trump&nbsp;Davos 2018 – live&nbsp;Donald Trump prepared to ...",
                        "pubDate": "Fri, 26 Jan 2018 15:25:39 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/video/2018/jan/26/trump-booed-at-davos-as-he-takes-swipe-at-media-video",
                        "id": "0000254"
                    },
                    {
                        "title": "Fox News host Sean Hannity makes U-turn in Trump-Mueller report – video",
                        "link": "https://www.theguardian.com/us-news/video/2018/jan/26/fox-news-presenter-makes-u-turn-over-trump-wanting-to-fire-robert-mueller-video",
                        "body": "Sean Hannity performs an abrupt volte-face live on air after scoffing at a report that the US president tried to fire the special counsel last June. 'The New York Times is trying to distract you,' the Fox News presenter tells viewers only to backtrack later in the show and ...",
                        "pubDate": "Fri, 26 Jan 2018 15:46:06 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/video/2018/jan/26/fox-news-presenter-makes-u-turn-over-trump-wanting-to-fire-robert-mueller-video",
                        "id": "0000253"
                    },
                    {
                        "title": "Kentucky Senator pitches bill to put armed patrols in schools after shooting",
                        "link": "https://www.theguardian.com/us-news/2018/jan/26/kentucky-senator-pitches-bill-to-put-armed-patrols-in-schools-after-shooting",
                        "body": "New legislation would let local districts hire armed marshals, who would not need to be police officers, to patrol public schoolsHours after authorities say a 15-year-old student shot and killed two classmates at a western Kentucky high school, a Republican senator in the ...",
                        "pubDate": "Fri, 26 Jan 2018 16:37:14 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/26/kentucky-senator-pitches-bill-to-put-armed-patrols-in-schools-after-shooting",
                        "id": "0000252"
                    },
                    {
                        "title": "FBI agent fatally shot a kidnap victim during raid in Houston, authorities say",
                        "link": "https://www.theguardian.com/us-news/2018/jan/26/fbi-agent-fatally-shot-kidnap-victim-houston",
                        "body": "‘The system failed. Whether it was accidental or not, the man is not going home to his family,’ the Conroe police chief saidAn FBI agent fatally shot the victim of a kidnapping during a raid early Thursday at a Houston home, authorities said.FBI spokeswoman Christina Garza said ...",
                        "pubDate": "Fri, 26 Jan 2018 16:50:18 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/26/fbi-agent-fatally-shot-kidnap-victim-houston",
                        "enclosure": [
                            {
                                "url": "https://i.guim.co.uk/img/media/7267f670f36321288c95604e35668f51f1e7696b/0_30_2868_1722/master/2868.jpg?w=140&q=55&auto=format&usm=12&fit=max&s=99896babf6e8ac19b61349f94d70d958",
                                "type": "image/jpeg",
                                "length": null,
                                "width": "140"
                            }
                        ],
                        "id": "0000251"
                    },
                    {
                        "title": "UN ambassador Nikki Haley says rumors of affair with Trump are 'disgusting'",
                        "link": "https://www.theguardian.com/us-news/2018/jan/26/un-ambassador-nikki-haley-says-rumors-of-affair-with-trump-are-disgusting",
                        "body": "Haley denied speculation that spread online that she had an affair with the president, saying ‘It is absolutely not true’One of the few high-ranking women in Donald Trump’s administration has denied rumors of a sexual affair with the president.Nikki Haley, the US ambassador to ...",
                        "pubDate": "Fri, 26 Jan 2018 16:58:28 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/26/un-ambassador-nikki-haley-says-rumors-of-affair-with-trump-are-disgusting",
                        "id": "0000250"
                    },
                    {
                        "title": "Trump administration set to admit far fewer refugees than plan allows for",
                        "link": "https://www.theguardian.com/us-news/2018/jan/26/trump-administration-refugees-resettlement",
                        "body": "International Rescue Committee says 21,292 refugees will be admitted to the US this year, far below administration’s 45,000 limitThe Trump administration is on pace to resettle fewer than half of its own reduced target for refugees, according to an analysis by the International ...",
                        "pubDate": "Fri, 26 Jan 2018 17:51:37 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/26/trump-administration-refugees-resettlement",
                        "id": "0000249"
                    },
                    {
                        "title": "Trump still doesn't understand why 'you're fired' can't work like it did on TV | Richard Wolffe",
                        "link": "https://www.theguardian.com/us-news/2018/jan/26/trump-still-doesnt-get-the-difference-between-reality-and-reality-tv",
                        "body": "News that Trump ordered the firing of Mueller shows he thinks he can utter his catchphrase and people just disappearDonald Trump has a problem with reality. To be specific, he has a problem distinguishing reality television from reality. With each passing news cycle, it’s ...",
                        "pubDate": "Fri, 26 Jan 2018 19:13:04 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/26/trump-still-doesnt-get-the-difference-between-reality-and-reality-tv",
                        "id": "0000248"
                    },
                    {
                        "title": "Casino mogul Steve Wynn accused of pattern of sexual harassment - report",
                        "link": "https://www.theguardian.com/us-news/2018/jan/26/steve-wynn-founder-of-wynn-resorts-accused-of-sexual-misconduct",
                        "body": "Wynn Resorts denied multiple allegations of sexual harassment and assault by its founder as its shares tumbled 9%Wynn Resorts has denied multiple allegations of sexual harassment and assault by its founder, Steve Wynn, which were detailed in a Wall Street Journal report that ...",
                        "pubDate": "Fri, 26 Jan 2018 19:39:24 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/26/steve-wynn-founder-of-wynn-resorts-accused-of-sexual-misconduct",
                        "id": "0000247"
                    },
                    {
                        "title": "Woman accuses film star Steven Seagal of rape in 1993",
                        "link": "https://www.theguardian.com/us-news/2018/jan/26/woman-accuses-film-star-steven-seagal-of-in-1993",
                        "body": "The woman called the alleged assault ‘very predatory, very aggressive and traumatizing’ during an interview A once-aspiring actress has alleged Steven Seagal raped her at a wrap party for the film On Deadly Ground, claiming he undressed her and assaulted her on his bed while she ...",
                        "pubDate": "Fri, 26 Jan 2018 19:50:44 GMT",
                        "permaLink": "https://www.theguardian.com/us-news/2018/jan/26/woman-accuses-film-star-steven-seagal-of-in-1993",
                        "id": "0000246"
                    }
                ]
            },
            {
                "feedTitle": "Quartz",
                "feedUrl": "http://qz.com/feed/",
                "websiteUrl": "https://qz.com/",
                "feedDescription": "Quartz is a digitally native news outlet for the new global economy.",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:57 GMT",
                "item": [
                    {
                        "title": "For the first time, scientists found the flu can cause heart attacks",
                        "link": "https://qz.com/1189383/the-flu-can-cause-heart-attacks-scientists-found/",
                        "body": "This year&#8217;s flu season is undoubtedly bad. Vaccines are less effective than usual, and the strains seem particularly virulent.\nNow, scientists have found evidence that the virus may be even more dangerous than we previously thought. On Jan. 24, researchers from Canada ...",
                        "pubDate": "Thu, 25 Jan 2018 19:11:15 GMT",
                        "permaLink": "",
                        "id": "0000245"
                    },
                    {
                        "title": "Sonos takes aim at Apple by selling two of its speakers for the price of one HomePod",
                        "link": "https://qz.com/1189541/sonos-get-two-of-our-one-speakers-for-the-price-of-one-apple-homepod-aapl/",
                        "body": "What&#8217;s better than a brand new, $349 Apple HomePod speaker? According to Sonos, two of its One speakers.\nSonos announced today, Jan. 25, that it&#8217;s selling two of its newest smart speakers for the price of a single HomePod, which is scheduled to be released Feb. 9. ...",
                        "pubDate": "Thu, 25 Jan 2018 19:25:32 GMT",
                        "permaLink": "",
                        "id": "0000244"
                    },
                    {
                        "title": "Robert Shiller wrote the book on bubbles and still thinks that’s what bitcoin is",
                        "link": "https://qz.com/1189516/robert-shiller-said-at-the-wef-in-davos-that-bitcoin-is-characterized-by-speculation/",
                        "body": "When a moderator at the World Economic Forum (WEF) asked Robert Shiller if he&#8217;s pro-bitcoin, the Yale economics professor asked if he could be half-pro-bitcoin. The Nobel Prize winner said he admires the technology, but thinks it has gone viral for reasons other than some ...",
                        "pubDate": "Thu, 25 Jan 2018 19:54:12 GMT",
                        "permaLink": "",
                        "id": "0000243"
                    },
                    {
                        "title": "Nine American cities are revolutionizing how data can improve lives",
                        "link": "https://qz.com/1189263/michael-bloombergs-foundation-says-these-nine-american-cities-are-revolutionizing-how-data-can-improve-lives/",
                        "body": "Michael Bloomberg, business magnate and former mayor of New York, is bringing together two of his greatest obsessions: cities and data.\nBloomberg Philanthropies, his charitable foundation, named the inaugural group of nine US cities to achieve What Works Cities certification, ...",
                        "pubDate": "Thu, 25 Jan 2018 20:34:12 GMT",
                        "permaLink": "",
                        "id": "0000242"
                    }
                ]
            }
        ]
    },
    "metadata": {
        "name": "myOpmlFeeds.opml",
        "docs": "http://scripting.com/stories/2010/12/06/innovationRiverOfNewsInJso.html",
        "secs": 0.002,
        "ctBuilds": 1,
        "ctDuplicatesSkipped": 4,
        "whenGMT": "Fri, 26 Jan 2018 20:55:00 GMT",
        "whenLocal": "2018-1-26 13:55:00",
        "aggregator": "River5 v0.6.2"
    }
})