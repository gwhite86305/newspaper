onGetRiverStream ({
    "updatedFeeds": {
        "updatedFeed": [
            {
                "feedTitle": "American Civil Liberties Union",
                "feedUrl": "http://www.aclu.org/blog/feed/",
                "websiteUrl": "https://www.aclu.org/",
                "feedDescription": "ACLU.org",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:57 GMT",
                "item": [
                    {
                        "title": "On the 45th Anniversary of Roe v. Wade, Court Battles for Abortion Access Persist",
                        "link": "https://www.aclu.org/blog/reproductive-freedom/abortion/45th-anniversary-roe-v-wade-court-battles-abortion-access-persist",
                        "body": "You’d be forgiven for forgetting Roe is the law of the land. In 2017 alone, 19 states adopted 63 new abortion restrictions.\nOn January 22, 1973, the U.S. Supreme Court issued its landmark decision Roe v. Wade, recognizing that the Constitution protects a woman’s right to an ...",
                        "pubDate": "Mon, 22 Jan 2018 15:45:00 GMT",
                        "permaLink": "",
                        "id": "0000180"
                    },
                    {
                        "title": "Worried About Getting Sued for Reporting Sexual Abuse? Here Are Some Tips.",
                        "link": "https://www.aclu.org/blog/womens-rights/worried-about-getting-sued-reporting-sexual-abuse-here-are-some-tips",
                        "body": "#MeToo has spawned a surge in defamation claims designed to silence victims. But there are ways to minimize legal risks. \nThe #MeToo movement has drawn an outpouring of testimony by the victims of sexual harassment and sexual abuse. In response, there has been a surge in ...",
                        "pubDate": "Mon, 22 Jan 2018 21:00:00 GMT",
                        "permaLink": "",
                        "id": "0000179"
                    },
                    {
                        "title": "Tenants Can Get Evicted for Calling the Police Across New York and Much of the Country",
                        "link": "https://www.aclu.org/blog/womens-rights/violence-against-women/tenants-can-get-evicted-calling-police-across-new-york-and",
                        "body": "So-called “nuisance ordinances” create a perverse incentive not to report crime and endanger domestic violence victims. \nThe second time that Laurie Grape called the police during an attack by her then-boyfriend, they told her that a third call would get her evicted. Under a ...",
                        "pubDate": "Tue, 23 Jan 2018 16:00:00 GMT",
                        "permaLink": "",
                        "id": "0000178"
                    },
                    {
                        "title": "Dallas County Violates People’s Rights by Keeping Them in Jail for Being Poor",
                        "link": "https://www.aclu.org/blog/mass-incarceration/smart-justice/dallas-county-violates-peoples-rights-keeping-them-jail-being",
                        "body": "Dallas County’s use of money bail is perpetuating illegal wealth-based incarceration. \nShannon Daves is a 47-year-old transgender woman who has been homeless in Dallas County, Texas, since last August. On January 17, she was arrested for an alleged misdemeanor and taken to the ...",
                        "pubDate": "Tue, 23 Jan 2018 22:15:00 GMT",
                        "permaLink": "",
                        "id": "0000177"
                    },
                    {
                        "title": "Is a Nondisclosure Agreement Silencing You From Sharing Your ‘Me Too’ Story? 4 Reasons It Might Be Illegal",
                        "link": "https://www.aclu.org/blog/womens-rights/womens-rights-workplace/nondisclosure-agreement-silencing-you-sharing-your-me-too",
                        "body": "If you have been sexually harassed, you shouldn’t be silenced by a coercive nondisclosure agreement. \nThe #MeToo movement has freed women, many of whom have kept silent about sexual harassment or assault, to tell their stories. Finally, survivors’ voices are being heard. But ...",
                        "pubDate": "Wed, 24 Jan 2018 14:45:00 GMT",
                        "permaLink": "",
                        "id": "0000176"
                    },
                    {
                        "title": "Customs and Border Protection Violated Court Orders During the First Muslim Ban Implementation",
                        "link": "https://www.aclu.org/blog/immigrants-rights/ice-and-border-patrol-abuses/customs-and-border-protection-violated-court",
                        "body": "Even as court orders rolled in, Customs and Border Protection continued to uphold Trump&#039;s Muslim ban.\nIt’s been nearly a year since the Trump Administration issued its first Muslim ban, unleashing chaos at airports across the country. A new report provides some details ...",
                        "pubDate": "Wed, 24 Jan 2018 20:45:00 GMT",
                        "permaLink": "",
                        "id": "0000175"
                    },
                    {
                        "title": "Wyomingites Say No to For-Profit Prison Looking to Settle in the State.",
                        "link": "https://www.aclu.org/blog/mass-incarceration/privatization-criminal-justice/wyomingites-say-no-profit-prison-looking",
                        "body": "Seeing a track record of neglect and abuse, Wyoming is mobilizing against private prisons coming to the state. \nIt should have been an ordinary December day for Juan Moreno*. He kissed his wife and kids goodbye in Casper, Wyoming, and left for work. But as Moreno turned the ...",
                        "pubDate": "Wed, 24 Jan 2018 21:15:00 GMT",
                        "permaLink": "",
                        "id": "0000174"
                    },
                    {
                        "title": "The NYPD’s ‘Cult of Compliance’",
                        "link": "https://www.aclu.org/blog/criminal-law-reform/reforming-police-practices/nypds-cult-compliance",
                        "body": "The NYPD reveals itself as an unelected branch of New York City government that elected leaders will not defy.\nIn New York City, bills are passed by city council members and signed by the mayor. But when the legislation is about policing, there is another, de-facto branch of ...",
                        "pubDate": "Thu, 25 Jan 2018 15:00:00 GMT",
                        "permaLink": "",
                        "id": "0000173"
                    },
                    {
                        "title": "Pennsylvania Imposes Permanent Solitary on Prisoners Facing Death",
                        "link": "https://www.aclu.org/blog/prisoners-rights/solitary-confinement/pennsylvania-imposes-permanent-solitary-prisoners-facing",
                        "body": "Prisoners in Pennsylvania can spend decades in solitary confinement, harming their physical and mental health.  \nImagine an ordinary parking space. Now add walls and a ceiling made of thick concrete, closed off by a solid steel door. The lights are always on, so it’s never dark. ...",
                        "pubDate": "Thu, 25 Jan 2018 22:00:00 GMT",
                        "permaLink": "",
                        "id": "0000172"
                    },
                    {
                        "title": "Is Sexual Harassment a Civil Rights Violation? It Should Be.",
                        "link": "https://www.aclu.org/blog/womens-rights/sexual-harassment-civil-rights-violation-it-should-be",
                        "body": "Civil rights remedies can be a tool for accountability and redress.\nIf the recent wave of sexual harassment and sexual assault revelations has taught us anything, it’s that we have not done enough to end sexual harassment and gender-based violence.\nIt has been over 30 years ...",
                        "pubDate": "Fri, 26 Jan 2018 18:15:00 GMT",
                        "permaLink": "",
                        "id": "0000171"
                    }
                ]
            },
            {
                "feedTitle": "Movies : NPR",
                "feedUrl": "http://www.npr.org/rss/rss.php?id=1045",
                "websiteUrl": "https://www.npr.org/templates/story/story.php?storyId=1045",
                "feedDescription": "NPR Movies podcast, movie reviews, and commentary on new and classic films. Interviews with filmmakers, actors, and actresses.",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:55 GMT",
                "item": [
                    {
                        "title": "WATCH: 2018 Oscar Nominations",
                        "link": "https://www.npr.org/sections/monkeysee/2018/01/23/578602536/watch-live-2018-oscar-nominations?utm_medium=RSS&utm_campaign=movies",
                        "body": "This year's Academy Award nominations were announced Tuesday morning via live stream.(Image credit: Christopher Polk/Getty Images)",
                        "pubDate": "Tue, 23 Jan 2018 13:00:45 GMT",
                        "permaLink": "https://www.npr.org/sections/monkeysee/2018/01/23/578602536/watch-live-2018-oscar-nominations?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000125"
                    },
                    {
                        "title": "'Shape Of Water,' 'Dunkirk' And 'Three Billboards' Lead Oscar Nominations",
                        "link": "https://www.npr.org/sections/monkeysee/2018/01/23/579102656/oscars-2018-the-complete-list-of-nominees?utm_medium=RSS&utm_campaign=movies",
                        "body": "This year's Academy Award nominations include a lot of old favorites, but also some new voices — including the first women ever nominated for cinematography.(Image credit: Christopher Polk/Getty Images)",
                        "pubDate": "Tue, 23 Jan 2018 13:36:00 GMT",
                        "permaLink": "https://www.npr.org/sections/monkeysee/2018/01/23/579102656/oscars-2018-the-complete-list-of-nominees?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000124"
                    },
                    {
                        "title": "Oscar-Nominated 'Phantom Thread' Focuses On Fashion's 'Most Obsessive'",
                        "link": "https://www.npr.org/2018/01/23/579940053/oscar-nominated-phantom-thread-focuses-on-fashion-s-most-obsessive?utm_medium=RSS&utm_campaign=movies",
                        "body": "Paul Thomas Anderson's new film stars Daniel Day-Lewis as a renowned fashion designer. Phantom Thread landed six Oscar nominations, but Anderson says making sewing look dramatic wasn't easy.(Image credit: Laurie Sparham /Focus Features)",
                        "pubDate": "Tue, 23 Jan 2018 18:55:00 GMT",
                        "permaLink": "https://www.npr.org/2018/01/23/579940053/oscar-nominated-phantom-thread-focuses-on-fashion-s-most-obsessive?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000123"
                    },
                    {
                        "title": "Oscars Debut New Rules To Avoid Another Envelope Mix-Up",
                        "link": "https://www.npr.org/sections/thetwo-way/2018/01/23/579973772/oscar-awards-debut-new-rules-to-avoid-another-envelope-mix-up?utm_medium=RSS&utm_campaign=movies",
                        "body": "Last year's historic Best Picture debacle prompted the accounting firm responsible for the security of envelopes to come up with new fail-safes. Most important: Stop tweeting and put away that phone.(Image credit: Matt Sayles/Invision/AP)",
                        "pubDate": "Tue, 23 Jan 2018 19:07:00 GMT",
                        "permaLink": "https://www.npr.org/sections/thetwo-way/2018/01/23/579973772/oscar-awards-debut-new-rules-to-avoid-another-envelope-mix-up?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000122"
                    },
                    {
                        "title": "Breaking Down The 2018 Oscar Nominations",
                        "link": "https://www.npr.org/2018/01/23/580076612/breaking-down-the-2018-oscar-nominations?utm_medium=RSS&utm_campaign=movies",
                        "body": "The Oscar Nominations have arrived. NPR's Linda Holmes and Bob Mondello discuss the whys and wherefores behind both surprises and snubs from this year's contenders.",
                        "pubDate": "Tue, 23 Jan 2018 21:18:00 GMT",
                        "permaLink": "https://www.npr.org/2018/01/23/580076612/breaking-down-the-2018-oscar-nominations?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000121"
                    },
                    {
                        "title": "Pop Culture Happy Hour: 'Phantom Thread'",
                        "link": "https://www.npr.org/sections/monkeysee/2018/01/24/580103245/pop-culture-happy-hour-phantom-thread?utm_medium=RSS&utm_campaign=movies",
                        "body": "Paul Thomas Anderson's story of a relationship between a mercurial dress designer in 1950s London and the young woman he begins seeing collected six Oscar nominations on Tuesday.(Image credit: Laurie Sparham /Focus Features)",
                        "pubDate": "Wed, 24 Jan 2018 18:21:00 GMT",
                        "permaLink": "https://www.npr.org/sections/monkeysee/2018/01/24/580103245/pop-culture-happy-hour-phantom-thread?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000120"
                    },
                    {
                        "title": "Violent Protests In India Follow Release Of Controversial Bollywood Epic",
                        "link": "https://www.npr.org/sections/thetwo-way/2018/01/25/580569671/violent-protests-in-india-follow-release-of-controversial-bollywood-epic?utm_medium=RSS&utm_campaign=movies",
                        "body": "Hindu nationalists ransacked shops and torched vehicles over the release of the film Padmaavat, which they say depicts a romantic encounter between a Hindu queen and a Muslim emperor.(Image credit: Dominique Faget/AFP/Getty Images)",
                        "pubDate": "Thu, 25 Jan 2018 10:04:00 GMT",
                        "permaLink": "https://www.npr.org/sections/thetwo-way/2018/01/25/580569671/violent-protests-in-india-follow-release-of-controversial-bollywood-epic?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000119"
                    },
                    {
                        "title": "A Superhero Movie Got A Screenplay Nomination: Glitch Or Game-Changer?",
                        "link": "https://www.npr.org/sections/monkeysee/2018/01/25/580219918/a-superhero-movie-got-a-screenplay-nomination-glitch-or-game-changer?utm_medium=RSS&utm_campaign=movies",
                        "body": "Some 40 years after Superman: The Movie launched the superhero film genre, a superhero movie earned an Oscar nomination outside of the tech categories. (And it wasn't Wonder Woman.)(Image credit: Ben Rothstein/Twentieth Century Fox)",
                        "pubDate": "Thu, 25 Jan 2018 11:00:00 GMT",
                        "permaLink": "https://www.npr.org/sections/monkeysee/2018/01/25/580219918/a-superhero-movie-got-a-screenplay-nomination-glitch-or-game-changer?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000118"
                    },
                    {
                        "title": "An Enchanting Love Triangle Unfolds In 'Lover For A Day'",
                        "link": "https://www.npr.org/2018/01/25/580631637/an-enchanting-parisian-love-triangle-unfolds-in-lover-for-a-day?utm_medium=RSS&utm_campaign=movies",
                        "body": "French filmmaker Philippe Garrel's new film follows a 50-something philosophy professor whose romantic relationship with a 23-year-old student is complicated when his grown daughter moves in.",
                        "pubDate": "Thu, 25 Jan 2018 18:41:41 GMT",
                        "permaLink": "https://www.npr.org/2018/01/25/580631637/an-enchanting-parisian-love-triangle-unfolds-in-lover-for-a-day?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000117"
                    },
                    {
                        "title": "A Lavish Bollywood Musical Is Fueling A Culture War In India",
                        "link": "https://www.npr.org/2018/01/25/580746654/a-lavish-bollywood-musical-is-fueling-a-culture-war-in-india?utm_medium=RSS&utm_campaign=movies",
                        "body": "From the moment Padmaavat went into production, it's been plagued by violent protests over its depiction of Queen Padmavati, a legendary Hindu royal.(Image credit: Manjunath Kiran/AFP/Getty Images)",
                        "pubDate": "Thu, 25 Jan 2018 21:21:00 GMT",
                        "permaLink": "https://www.npr.org/2018/01/25/580746654/a-lavish-bollywood-musical-is-fueling-a-culture-war-in-india?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000116"
                    },
                    {
                        "title": "In Gritty 'A Ciambra,' A Romani Teen Must Choose: His Friend, Or His Community?",
                        "link": "https://www.npr.org/2018/01/25/579711983/in-gritty-a-ciambra-a-romani-teen-must-choose-his-friend-or-his-community?utm_medium=RSS&utm_campaign=movies",
                        "body": "Writer-director Jonas Carpignano captures the street life of a Romani-Italian community in a way that's both uncompromisingly realistic and lyrical.(Image credit: IFC Films )",
                        "pubDate": "Thu, 25 Jan 2018 22:00:16 GMT",
                        "permaLink": "https://www.npr.org/2018/01/25/579711983/in-gritty-a-ciambra-a-romani-teen-must-choose-his-friend-or-his-community?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000115"
                    },
                    {
                        "title": "On The Spectrum, With A Spec Script: 'Please Stand By'",
                        "link": "https://www.npr.org/2018/01/25/579755614/on-the-spectrum-with-a-spec-script-please-stand-by?utm_medium=RSS&utm_campaign=movies",
                        "body": "Dakota Fanning plays a young woman with autism determined to submit her Star Trek script to a screenwriting competition in this disappointingly soft coming-of-age tale.",
                        "pubDate": "Thu, 25 Jan 2018 22:00:16 GMT",
                        "permaLink": "https://www.npr.org/2018/01/25/579755614/on-the-spectrum-with-a-spec-script-please-stand-by?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000114"
                    },
                    {
                        "title": "In The Brutal Animated Film 'Have A Nice Day,' Nobody Does",
                        "link": "https://www.npr.org/2018/01/25/579763911/in-the-brutal-animated-film-have-a-nice-day-nobody-does?utm_medium=RSS&utm_campaign=movies",
                        "body": "In this indie animated film from China, a bag of money keeps changing hands in a ruthless criminal underworld depicted with a blunt and deliberate crudeness.(Image credit: Strand Releasing)",
                        "pubDate": "Thu, 25 Jan 2018 22:00:46 GMT",
                        "permaLink": "https://www.npr.org/2018/01/25/579763911/in-the-brutal-animated-film-have-a-nice-day-nobody-does?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000113"
                    },
                    {
                        "title": "Casey Affleck Won't Be Presenting Statuettes At The Oscars",
                        "link": "https://www.npr.org/sections/thetwo-way/2018/01/25/580815826/casey-affleck-wont-be-presenting-statuettes-at-the-oscars?utm_medium=RSS&utm_campaign=movies",
                        "body": "The acclaimed actor has been dogged by two sexual harassment lawsuits filed against him by two separate women in 2010. The Academy says it appreciates his decision not to attend the ceremonies. (Image credit: Jordan Strauss/Jordan Strauss/Invision/AP)",
                        "pubDate": "Thu, 25 Jan 2018 22:12:31 GMT",
                        "permaLink": "https://www.npr.org/sections/thetwo-way/2018/01/25/580815826/casey-affleck-wont-be-presenting-statuettes-at-the-oscars?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000112"
                    },
                    {
                        "title": "'The Maze Runner: The Death Cure': Nice Guy Finishes, At Last",
                        "link": "https://www.npr.org/2018/01/26/579265353/the-maze-runner-the-death-cure-nice-guy-finishes-at-last?utm_medium=RSS&utm_campaign=movies",
                        "body": "The longest, and last, of the Maze Runner films puts its bland teen heroes through the usual paces, but there are enough strong character actors around to keep things interesting.(Image credit: Twentieth Century Fox)",
                        "pubDate": "Fri, 26 Jan 2018 20:00:46 GMT",
                        "permaLink": "https://www.npr.org/2018/01/26/579265353/the-maze-runner-the-death-cure-nice-guy-finishes-at-last?utm_medium=RSS&utm_campaign=movies",
                        "id": "0000111"
                    }
                ]
            },
            {
                "feedTitle": "code4lib",
                "feedUrl": "http://code4lib.org/node/feed",
                "websiteUrl": "https://code4lib.org/",
                "feedDescription": "code4lib is an IRC channel, a mailing list, and a conference for folks interested in the convergence of computers and library/information science. The channel grew out of the email list as a more informal and interactive way to discuss code, projects, ideas, and other random topics. Planet code4lib aggregates the journaling of some of the people in the room, and our most recent addition is a conference where we can all get some face time and share our latest projects.",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:55 GMT",
                "item": [
                    {
                        "title": "Code4Lib Journal #32",
                        "link": "https://code4lib.org/content/code4lib-journal-32",
                        "body": "Topic:&nbsp;journal\nIssue #32 of the Code4Lib Journal is now available.\n\n Editorial  Introduction: People, by Meghan Finch\nAn Open-Source Strategy for Documenting Events: The Case Study of the 42nd Canadian Federal Election on Twitter by Nick Ruest and Ian Milligan\nHow to Party ...",
                        "pubDate": "Tue, 26 Apr 2016 14:48:13 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/content/code4lib-journal-32#comments",
                        "id": "0000030"
                    },
                    {
                        "title": "Code4Lib Journal #33",
                        "link": "https://code4lib.org/content/code4lib-journal-33",
                        "body": "Topic:&nbsp;journalIssue #33 of the Code4Lib Journal is now available:\nEditorial Introduction – Summer Reading List, by Ron Peterson\nEmflix – Gone Baby Gone, by Netanel Ganin\nIntroduction to Text Mining with R for Information Professionals, by Monica Maceli\nData for Decision ...",
                        "pubDate": "Tue, 19 Jul 2016 15:32:07 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/content/code4lib-journal-33#comments",
                        "id": "0000029"
                    },
                    {
                        "title": "Code4Lib 2017",
                        "link": "https://code4lib.org/conference/2017",
                        "body": "The 2017 conference will be held from March 6 through March 9 in the at the Luskin Conference Center at UCLA. With a very large and modern conference center at our disposal, Code4Lib 2017 will be able to accommodate the growing number of attendees while also retaining that ...",
                        "pubDate": "Thu, 13 Oct 2016 19:09:15 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/conference/2017#comments",
                        "id": "0000028"
                    },
                    {
                        "title": "C4L17: Call for Presentation/Panel proposals",
                        "link": "https://code4lib.org/content/c4l17-call-presentationpanel-proposals",
                        "body": "Topic:&nbsp;conferencesCode4Lib 2017 is a loosely-structured conference that provides people working at the intersection of libraries/archives/museums/cultural heritage and technology with a chance to share ideas, be inspired, and forge collaborations. For more information about ...",
                        "pubDate": "Fri, 14 Oct 2016 16:05:05 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/content/c4l17-call-presentationpanel-proposals#comments",
                        "id": "0000027"
                    },
                    {
                        "title": "Code4Lib Journal #34",
                        "link": "https://code4lib.org/content/code4lib-journal-34",
                        "body": "Topic:&nbsp;journalCode4Lib Journal #34 is now available.\nEditorial: Some Numbers, by Andrew Darby\nDigital Archaeology and/or Forensics: Working with Floppy Disks from the 1980s, by John Durno\nNeed Help with Your Code? Piloting a Programming and Software Development Consultation ...",
                        "pubDate": "Sat, 29 Oct 2016 01:08:17 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/content/code4lib-journal-34#comments",
                        "id": "0000026"
                    },
                    {
                        "title": "Code4Lib Journal Issue 38 Call for Papers",
                        "link": "https://code4lib.org/content/code4lib-journal-issue-38-call-papers",
                        "body": "Topic:&nbsp;journalThe Code4Lib Journal (C4LJ) exists to foster community and share information among those interested in the intersection of libraries, technology, and the future.\nWe are now accepting proposals for publication in our 38th issue. Don't miss out on this ...",
                        "pubDate": "Mon, 12 Jun 2017 15:17:09 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/content/code4lib-journal-issue-38-call-papers#comments",
                        "id": "0000025"
                    },
                    {
                        "title": "Issue 37 of the Code4Lib Journal",
                        "link": "https://code4lib.org/content/issue-37-code4lib-journal",
                        "body": "Issue 37 of the Code4Lib Journal is now available.\nTable of Contents:\nEditorial: Welcome New Editors, What We Know About Who We Are, and Submission Pro Tip!\nby Sara Amato\nA Practical Starter Guide on Developing Accessible Websites\nby Cynthia Ng and Michael Schofield\nRecount: ...",
                        "pubDate": "Wed, 19 Jul 2017 01:16:16 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/content/issue-37-code4lib-journal#comments",
                        "id": "0000024"
                    },
                    {
                        "title": "Code4Lib 2018",
                        "link": "https://code4lib.org/conference/2018",
                        "body": "Topic:&nbsp;conferencesCode4Lib 2018 will be held from February 13-16, 2018, in Washington, D.C.\nMore information is available on the conference website at: http://2018.code4lib.org/",
                        "pubDate": "Tue, 08 Aug 2017 22:07:24 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/conference/2018#comments",
                        "id": "0000023"
                    },
                    {
                        "title": "Issue 38 of the Code4Lib Journal",
                        "link": "https://code4lib.org/content/issue-38-code4lib-journal",
                        "body": "Issue 38 is now available!\nTable of Contents:\nEditorial: The Economics of Not Being an Organization\nby Carol Bean\nUsability Analysis of the Big Ten Academic Alliance Geoportal: Findings and Recommendations for Improvement of the User Experience\nby Mara Blake, Karen Majewicz, ...",
                        "pubDate": "Wed, 18 Oct 2017 15:23:46 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/content/issue-38-code4lib-journal#comments",
                        "id": "0000022"
                    },
                    {
                        "title": "Seeking for a Local D.C. Host",
                        "link": "https://code4lib.org/content/seeking-local-dc-host",
                        "body": "Looking for any D.C. locals that could host a guest for the 2018 C4L. I'll be traveling from Philadelphia by car and am hoping to bike or metro to the Omni Shoreham Hotel. I have a potential registration that could be transferred to me if I can find boarding. Our travel budget ...",
                        "pubDate": "Mon, 22 Jan 2018 21:02:16 GMT",
                        "permaLink": "",
                        "comments": "https://code4lib.org/content/seeking-local-dc-host#comments",
                        "id": "0000021"
                    }
                ]
            },
            {
                "feedTitle": "bart.gov",
                "feedUrl": "http://www.bart.gov/news/rss/rss.xml",
                "websiteUrl": "http://www.bart.gov/news/rss",
                "feedDescription": "",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:54 GMT",
                "item": [
                    {
                        "title": "Longer loops help shorter straphangers and standees in short-term space solution",
                        "link": "http://www.bart.gov/news/articles/2018/news20180111?rss",
                        "body": "A car with modified seating, a short-term fix to increase capacity until new trains are in service...",
                        "pubDate": "Fri, 26 Jan 2018 20:54:54 GMT",
                        "permaLink": "http://www.bart.gov/news/articles/2018/news20180111",
                        "id": "0000020"
                    },
                    {
                        "title": "Teens can now get a big discount on BART with a Youth Clipper card",
                        "link": "http://www.bart.gov/news/articles/2017/news20171218?rss",
                        "body": "&nbsp;By JIAHAO HUANGContributing Writer&nbsp;If you&rsquo;re 13-18 and have a school break coming...",
                        "pubDate": "Fri, 26 Jan 2018 20:54:54 GMT",
                        "permaLink": "http://www.bart.gov/news/articles/2017/news20171218",
                        "id": "0000019"
                    },
                    {
                        "title": "Get Clipper and Save! New fares effective Jan. 1, 2018",
                        "link": "http://www.bart.gov/news/articles/2017/news20171201?rss",
                        "body": "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;On January 1, 2018, a number of...",
                        "pubDate": "Fri, 26 Jan 2018 20:54:54 GMT",
                        "permaLink": "http://www.bart.gov/news/articles/2017/news20171201",
                        "id": "0000018"
                    },
                    {
                        "title": "BART’s newest bike station is a first for Contra Costa County",
                        "link": "http://www.bart.gov/news/articles/2018/news20180117?rss",
                        "body": "BART is taking a major step forward in promoting bicycle access in Contra Costa County by opening...",
                        "pubDate": "Fri, 26 Jan 2018 20:54:54 GMT",
                        "permaLink": "http://www.bart.gov/news/articles/2018/news20180117",
                        "id": "0000017"
                    },
                    {
                        "title": "Monthly Reserved parking waitlist for Antioch stations UPDATE",
                        "link": "http://www.bart.gov/news/articles/2018/news20180104?rss",
                        "body": "UPDATE:Select-a-spot (our parking permit vendor) was unable to open up the list yesterday (1/16)...",
                        "pubDate": "Fri, 26 Jan 2018 20:54:54 GMT",
                        "permaLink": "http://www.bart.gov/news/articles/2018/news20180104",
                        "id": "0000016"
                    },
                    {
                        "title": "BART&amp;#039;s new train cars now in service",
                        "link": "http://www.bart.gov/news/articles/2018/news20180119?rss",
                        "body": "BART&rsquo;s new Fleet of the Future train is here. The very first customers to ride the new train...",
                        "pubDate": "Fri, 26 Jan 2018 20:54:54 GMT",
                        "permaLink": "http://www.bart.gov/news/articles/2018/news20180119",
                        "id": "0000015"
                    }
                ]
            },
            {
                "feedTitle": "Amazon Elastic Compute Cloud (N. Virginia) Service Status",
                "feedUrl": "http://status.aws.amazon.com/rss/ec2-us-east-1.rss",
                "websiteUrl": "http://status.aws.amazon.com/",
                "feedDescription": "",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:53 GMT",
                "item": [
                    {
                        "title": "Service is operating normally: [RESOLVED] Network Connectivity",
                        "link": "http://status.aws.amazon.com/",
                        "body": "Between 6:47 AM and 7:10 AM PDT we experienced increased launch failures for EC2 Instances, degraded EBS volume performance and connectivity issues for some instances in a single Availability Zone in the US-EAST-1 Region. The issue has been resolved and the service is operating ...",
                        "pubDate": "Thu, 27 Jul 2017 14:28:00 GMT",
                        "permaLink": "http://status.aws.amazon.com/#ec2-us-east-1_1501165680",
                        "id": "0000014"
                    },
                    {
                        "title": "Informational message: Network Connectivity",
                        "link": "http://status.aws.amazon.com/",
                        "body": "We are investigating network connectivity issues for some instances in a single Availability Zone in the US-EAST-1 Region.",
                        "pubDate": "Wed, 02 Aug 2017 23:36:24 GMT",
                        "permaLink": "http://status.aws.amazon.com/#ec2-us-east-1_1501716984",
                        "id": "0000013"
                    },
                    {
                        "title": "Informational message:  Increased Launch Error Rates",
                        "link": "http://status.aws.amazon.com/",
                        "body": "We are investigating increased error rates for new EBS-backed instance launches and EBS-related APIs in the US-EAST-1 Region.",
                        "pubDate": "Mon, 09 Oct 2017 05:48:00 GMT",
                        "permaLink": "http://status.aws.amazon.com/#ec2-us-east-1_1507528080",
                        "id": "0000009"
                    },
                    {
                        "title": "Service is operating normally: [RESOLVED]  Increased Launch Error Rates",
                        "link": "http://status.aws.amazon.com/",
                        "body": "Between 9:45 PM and 11:37 PM PDT we experienced increased error rates for EBS-backed instance launches and EBS-related APIs in the US-EAST-1 Region. Existing instances were unaffected. The issue has been resolved and the service is operating normally.",
                        "pubDate": "Mon, 09 Oct 2017 06:51:51 GMT",
                        "permaLink": "http://status.aws.amazon.com/#ec2-us-east-1_1507531911",
                        "id": "0000007"
                    },
                    {
                        "title": "Service is operating normally: [RESOLVED] Increased Launch Error Rates",
                        "link": "http://status.aws.amazon.com/",
                        "body": "We are investigating increased error rates for new launches in a single Availability Zone in the US-EAST-1 Region.",
                        "pubDate": "Tue, 14 Nov 2017 14:48:00 GMT",
                        "permaLink": "http://status.aws.amazon.com/#ec2-us-east-1_1510670880",
                        "id": "0000001"
                    }
                ]
            }
        ]
    },
    "metadata": {
        "name": "myTxtFeeds.txt",
        "docs": "http://scripting.com/stories/2010/12/06/innovationRiverOfNewsInJso.html",
        "secs": 0.003,
        "ctBuilds": 1,
        "ctDuplicatesSkipped": 10,
        "whenGMT": "Fri, 26 Jan 2018 20:55:00 GMT",
        "whenLocal": "2018-1-26 13:55:00",
        "aggregator": "River5 v0.6.2"
    }
})