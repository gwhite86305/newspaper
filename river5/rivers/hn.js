onGetRiverStream ({
    "updatedFeeds": {
        "updatedFeed": [
            {
                "feedTitle": "Hacker News Firehose",
                "feedUrl": "http://hn.geekity.com/newstories.xml",
                "websiteUrl": "https://news.ycombinator.com/newest",
                "feedDescription": "All links posted to Hacker News.",
                "whenLastUpdate": "Fri, 26 Jan 2018 20:54:55 GMT",
                "item": [
                    {
                        "title": "Show HN: Deploying Xamarin Android App to X86 Emulator",
                        "link": "http://blog.techdominator.com/article/deploying-xamarin-android-project-x86-emulator.html",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:04:05 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242410",
                        "id": "0000080"
                    },
                    {
                        "title": "Revenge of the CLI: A transformation through voice and AI",
                        "link": "http://www.bricklin.com/vuiandcli.htm",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:04:06 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242412",
                        "id": "0000079"
                    },
                    {
                        "title": "Where is Hubble now?",
                        "link": "http://www.satview.org/?sat_id=20580U",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:10:00 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242455",
                        "id": "0000078"
                    },
                    {
                        "title": "Intel’s Glass House Is Definitely More Than Half Full",
                        "link": "https://www.nextplatform.com/2018/01/26/intels-glass-house-definitely-half-full/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:12:14 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242472",
                        "id": "0000077"
                    },
                    {
                        "title": "Cannabidiol May Help Reduce Seizures for Those with Treatment Resistant Epilepsy",
                        "link": "http://neurosciencenews.com/cannabidiol-epilepsy-8374/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:12:46 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242475",
                        "id": "0000076"
                    },
                    {
                        "title": "NonPetya ransomware forced Maersk to reinstall 4000 servers, 45000 PCs",
                        "link": "http://www.zdnet.com/article/maersk-forced-to-reinstall-4000-servers-45000-pcs-due-to-notpetya-attack/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:13:08 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242477",
                        "id": "0000075"
                    },
                    {
                        "title": "The Cyber Incident Tsunami – Time to Get Ready – Online Trust Alliance",
                        "link": "https://otalliance.org/blog/cyber-incident-tsunami-time-get-ready",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:13:47 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242485",
                        "id": "0000074"
                    },
                    {
                        "title": "U.S. ITC rejects Boeing injury claims on Bombardier",
                        "link": "https://www.reuters.com/article/us-usa-itc-ruling/u-s-itc-rejects-boeing-injury-claims-on-bombardier-idUSKBN1FF2MB?utm_source=twitter&utm_medium=Social",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:14:13 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242490",
                        "id": "0000073"
                    },
                    {
                        "title": "Things I think I know about Cryptography",
                        "link": "https://dev.to/dwd/things-i-think-i-know-about-cryptography-ifj",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:14:26 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242493",
                        "id": "0000072"
                    },
                    {
                        "title": "Meltdown and Spectre Patching Has Been a Total Train Wreck",
                        "link": "https://www.wired.com/story/meltdown-spectre-patching-total-train-wreck/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:14:42 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242495",
                        "id": "0000071"
                    },
                    {
                        "title": "Intel CEO: New chips will have built-in protections against Meltdown, Spectre",
                        "link": "https://www.techrepublic.com/article/intel-ceo-new-chips-will-have-built-in-protections-against-meltdown-spectre/?ftag=COS-05-10aaa0g&utm_campaign=trueAnthem:+Trending+Content&utm_content=5a6b89f419694a00077cae3c&utm_medium=trueAnthem&utm_source=twitter",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:15:13 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242498",
                        "id": "0000070"
                    },
                    {
                        "title": "iPhone SE 2 Rumored for Late Spring Launch with Wireless Charging Support",
                        "link": "https://hothardware.com/news/iphone-se-2-rumored-for-late-spring-launch-with-wireless-charging-support",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:16:03 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242502",
                        "id": "0000069"
                    },
                    {
                        "title": "U.S. trade body backs Canada's Bombardier over Boeing in tariff spat",
                        "link": "https://www.reuters.com/article/us-usa-itc-ruling/u-s-trade-body-backs-canadas-bombardier-over-boeing-in-tariff-spat-idUSKBN1FF2MB?feedType=RSS&feedName=topNews&utm_source=twitter&utm_medium=Social&__twitter_impression=true",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:16:17 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242506",
                        "id": "0000068"
                    },
                    {
                        "title": "Mosquitoes can learn the association between mechanical shock and smells",
                        "link": "http://www.cell.com/current-biology/fulltext/S0960-9822(17)31617-2",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:19:32 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242534",
                        "id": "0000067"
                    },
                    {
                        "title": "Learn SELinux by Doing – SELinux Game",
                        "link": "http://selinuxgame.org/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:19:48 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242536",
                        "id": "0000066"
                    },
                    {
                        "title": "Secure Messaging App Comparison: iMessage, WhatsApp and Signal",
                        "link": "https://www.rokacom.com/best-secure-messaging-app-whats-right-choice/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:20:10 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242540",
                        "id": "0000065"
                    },
                    {
                        "title": "Algorithms Explained: Diffie-Hellman",
                        "link": "https://hackernoon.com/algorithms-explained-diffie-hellman-1034210d5100",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:20:11 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242541",
                        "id": "0000064"
                    },
                    {
                        "title": "Mueller team interviewed Facebook staff in Russia probe: Wired",
                        "link": "https://www.reuters.com/article/us-usa-trump-russia-facebook/mueller-team-interviewed-facebook-staff-in-russia-probe-wired-idUSKBN1FF2OR?feedType=RSS&feedName=topNews&utm_source=twitter&utm_medium=Social",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:20:52 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242545",
                        "id": "0000063"
                    },
                    {
                        "title": "Air Force One Needs New Refrigerators. They Cost $24M – Defense One",
                        "link": "http://www.defenseone.com/business/2018/01/air-force-one-needs-new-refrigerators-they-cost-24-million/145457/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:22:45 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242565",
                        "id": "0000062"
                    },
                    {
                        "title": "Blooms: Phi-Based Strobe Animated Sculptures",
                        "link": "http://www.instructables.com/id/Blooming-Zoetrope-Sculptures/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:24:03 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242573",
                        "id": "0000061"
                    },
                    {
                        "title": "Setting Your 2018 Goals by Nelson Dellis [video]",
                        "link": "https://www.youtube.com/watch?v=Dk6TyZysA2E",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:26:35 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242587",
                        "id": "0000060"
                    },
                    {
                        "title": "The female price of male pleasure",
                        "link": "https://theweek.com/articles/749978/female-price-male-pleasure",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:27:16 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242593",
                        "id": "0000059"
                    },
                    {
                        "title": "Microsoft's MVP threatens users with persecution for disabling Windows Update",
                        "link": "https://alamar.livejournal.com/393949.html",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:28:05 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242603",
                        "id": "0000058"
                    },
                    {
                        "title": "Ask HN: Which VPS/dedicated cloud providers let you bring your own IP blocks?",
                        "link": "https://news.ycombinator.com/item?id=16242620",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:29:58 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242620",
                        "id": "0000057"
                    },
                    {
                        "title": "Would a Billionaire Take the Time to Cash a 13 Cent Check?",
                        "link": "https://www.celebritynetworth.com/articles/entertainment-articles/billionaire-cash-13-cent-check-someone-actually-conducted-experiment/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:30:10 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242621",
                        "id": "0000056"
                    },
                    {
                        "title": "Stacktrace improvements in .NET Core 2.1",
                        "link": "https://www.ageofascent.com/2018/01/26/stack-trace-for-exceptions-in-dotnet-core-2.1/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:30:44 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242624",
                        "id": "0000055"
                    },
                    {
                        "title": "Blockchain's Broken Promises",
                        "link": "https://www.project-syndicate.org/commentary/why-bitcoin-is-a-bubble-by-nouriel-roubini-2018-01",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:31:36 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242630",
                        "id": "0000054"
                    },
                    {
                        "title": "MiniLatex Demo App",
                        "link": "https://jxxcarlson.github.io/app/minilatex/src/index.html",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:32:30 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242633",
                        "id": "0000053"
                    },
                    {
                        "title": "HomePod vs. Amazon Echo vs. Google Home Max vs. Sonos One: Speaker Showdown",
                        "link": "https://www.imore.com/homepod-vs-amazon-echo-vs-google-home-max-vs-sonos-one-speaker-showdown",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:32:36 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242634",
                        "id": "0000052"
                    },
                    {
                        "title": "Interesting 2017 JavaScript feature proposals that weren't adopted",
                        "link": "https://blog.logrocket.com/interesting-ecmascript-2017-proposals-163b787cf27c",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:34:40 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242648",
                        "id": "0000051"
                    },
                    {
                        "title": "Issue 82 – GraphQL-Weekly",
                        "link": "https://graphqlweekly.com/issues/82/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:35:02 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242651",
                        "id": "0000050"
                    },
                    {
                        "title": "Young College Graduates Aren't Moving Like They Used To",
                        "link": "https://www.forbes.com/sites/prestoncooper2/2018/01/26/young-college-graduates-arent-moving-like-they-used-to/?utm_source=TWITTER&utm_medium=social&utm_content=1305978719&utm_campaign=sprinklrForbesMainTwitter#4edd59ee685f",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:35:15 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242652",
                        "id": "0000049"
                    },
                    {
                        "title": "Subgraph: This Security-Focused Distro Is Malware’s Worst Nightmare",
                        "link": "https://www.linux.com/learn/intro-to-linux/2018/1/subgraph-security-focused-distro-malwares-worst-nightmare",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:35:47 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242655",
                        "id": "0000048"
                    },
                    {
                        "title": "Ask HN: How do you “own your private keys” for cryptocurrencies held  Exchanges",
                        "link": "https://news.ycombinator.com/item?id=16242665",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:36:51 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242665",
                        "id": "0000047"
                    },
                    {
                        "title": "Selling Laziness",
                        "link": "http://www.usrsb.in/selling-laziness.html",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:37:04 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242667",
                        "id": "0000046"
                    },
                    {
                        "title": "$400M Goes Missing from Japanese Crypto Exchange Coincheck",
                        "link": "https://gizmodo.com/400-million-goes-missing-from-japanese-crypto-exchange-1822454084?utm_campaign=socialflow_gizmodo_twitter&utm_source=gizmodo_twitter&utm_medium=socialflow",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:39:27 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242685",
                        "id": "0000045"
                    },
                    {
                        "title": "How I sold my stock options",
                        "link": "https://hackernoon.com/how-i-sold-my-stock-options-93448398500d",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:40:37 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242692",
                        "id": "0000044"
                    },
                    {
                        "title": "Bombardier wins trade dispute in US",
                        "link": "http://www.bbc.com/news/uk-northern-ireland-42825916",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:42:37 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242709",
                        "id": "0000043"
                    },
                    {
                        "title": "A Brief History of BitCon: the many cases of fraud in the cyrpto market",
                        "link": "https://hackernoon.com/a-brief-history-of-bitcon-cf358da30bf0",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:44:29 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242725",
                        "id": "0000042"
                    },
                    {
                        "title": "Ask HN: Support/service desk tools and tips?",
                        "link": "https://news.ycombinator.com/item?id=16242749",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:46:26 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242749",
                        "id": "0000041"
                    },
                    {
                        "title": "SoFi Buys Teams from Mortgage Startup Clara to Boost Offerings",
                        "link": "https://www.bloomberg.com/news/articles/2018-01-26/sofi-buys-teams-from-mortgage-startup-clara-to-boost-offerings",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:47:13 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242760",
                        "id": "0000040"
                    },
                    {
                        "title": "Facebook’s trust survey isn’t too short – but it is written badly – The Verge",
                        "link": "https://www.theverge.com/2018/1/26/16933458/facebook-news-trust-survey-problems-editing",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:47:39 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242764",
                        "id": "0000039"
                    },
                    {
                        "title": "Web Pack Academy",
                        "link": "https://medium.freecodecamp.org/introducing-webpack-academy-bad0a4e23deb",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:48:27 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242773",
                        "id": "0000038"
                    },
                    {
                        "title": "Vernacular Economics: How Building Codes and Taxes Shape Regional Architecture",
                        "link": "https://99percentinvisible.org/article/vernacular-economics-building-codes-taxes-shape-regional-architecture/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:49:04 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242780",
                        "id": "0000037"
                    },
                    {
                        "title": "The analyst community assumes that tech is the ultimate cybersecurity weapon",
                        "link": "https://medium.com/swlh/theres-more-to-cyber-security-than-expensive-new-technology-analysts-know-that-but-they-focus-on-1004e1d108f6",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:50:09 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242792",
                        "id": "0000036"
                    },
                    {
                        "title": "Many Intellectuals Can't Stand Jordan Peterson. Why?",
                        "link": "https://fee.org/articles/many-intellectuals-cant-stand-jordan-peterson-why/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:50:36 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242796",
                        "id": "0000035"
                    },
                    {
                        "title": "How Far Are We from a Fully Autonomous Driving World?",
                        "link": "https://hackernoon.com/how-far-are-we-from-a-fully-autonomous-driving-world-89fde97b5352",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:51:13 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242799",
                        "id": "0000034"
                    },
                    {
                        "title": "Using Helicopters for Nashville Mass Transit Alternative",
                        "link": "https://www.linkedin.com/pulse/using-helicopters-nashville-mass-transit-alternative-john-maddox/?trackingId=sH%2B6xzLkG%2F2%2F%2FuJUPXEfWA%3D%3D",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:52:33 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242808",
                        "id": "0000033"
                    },
                    {
                        "title": "Effective Testing with RSpec 3, Testing in Isolation: Unit Specs",
                        "link": "http://blog.travisspangle.com/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:52:49 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242811",
                        "id": "0000032"
                    },
                    {
                        "title": "If It’s Important, Learn It Repeatedly",
                        "link": "http://www.raptitude.com/2018/01/if-its-important-learn-it-repeatedly/",
                        "body": "",
                        "pubDate": "Fri, 26 Jan 2018 20:54:03 GMT",
                        "permaLink": "",
                        "comments": "https://news.ycombinator.com/item?id=16242826",
                        "id": "0000031"
                    }
                ]
            }
        ]
    },
    "metadata": {
        "name": "hn.opml",
        "docs": "http://scripting.com/stories/2010/12/06/innovationRiverOfNewsInJso.html",
        "secs": 0,
        "ctBuilds": 1,
        "ctDuplicatesSkipped": 0,
        "whenGMT": "Fri, 26 Jan 2018 20:55:00 GMT",
        "whenLocal": "2018-1-26 13:55:00",
        "aggregator": "River5 v0.6.2"
    }
})