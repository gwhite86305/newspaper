const express = require('express');
const worthynews = require('../services/worthynews');

const router = new express.Router();

/**
 * Returns a list of the most popular article id's
 */
router.get('/', async (req, res, next) => {
  const options = {
  };

  try {
    const result = await worthynews.getMostPopularArticles(options);
    res.status(result.status || 200).send(result.data);
  } catch (err) {
    return res.status(500).send({
      status: 500,
      error: 'Server Error'
    });
  }
});

/**
 * Adds an articleId and records the hit
 */
router.post('/:articleId', async (req, res, next) => {
  const options = {
    articleId: req.params.articleId
  };

  try {
    const result = await worthynews.trackArticleHit(options);
    res.status(result.status || 200).send(result.data);
  } catch (err) {
    return res.status(500).send({
      status: 500,
      error: 'Server Error'
    });
  }
});

module.exports = router;
