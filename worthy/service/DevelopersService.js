'use strict';


/**
 * searches inventory up to the last 10 most popular news articles for that day
 * You can retrieve up to 10 of today's most popular news worthy items 
 *
 * returns List
 **/
exports.searchInventory = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "keywords" : "Trump discusses immigration",
  "releaseDate" : "2016-08-29T09:12:33.001Z",
  "articleId" : "d290f1ee-6c54-4b01-90e6-d701748f0851",
  "name" : "Widget Adapter",
  "location" : "Anywhere",
  "source" : {
    "phone" : "408-867-5309",
    "name" : "The Washington Post",
    "homePage" : "https://www.washingtonpost.com"
  }
}, {
  "keywords" : "Trump discusses immigration",
  "releaseDate" : "2016-08-29T09:12:33.001Z",
  "articleId" : "d290f1ee-6c54-4b01-90e6-d701748f0851",
  "name" : "Widget Adapter",
  "location" : "Anywhere",
  "source" : {
    "phone" : "408-867-5309",
    "name" : "The Washington Post",
    "homePage" : "https://www.washingtonpost.com"
  }
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * adds a Worthy news article to the inventory based on the articleID
 * Adds a new article hit to the inventory
 *
 * no response value expected for this operation
 **/
exports.trackArticleHit = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

